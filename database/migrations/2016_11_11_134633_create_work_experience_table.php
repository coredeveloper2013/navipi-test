<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkExperienceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('work_experiences', function (Blueprint $table) {
            $table->increments('id');			
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('title',255);
            $table->string('company',255);
            $table->string('department',255);
            $table->string('city',255);
            $table->integer('country')->unsigned()->index();
            $table->integer('from_month')->unsigned()->index();
            $table->integer('from_year')->unsigned()->index();
            $table->integer('to_month')->unsigned()->index();
            $table->integer('to_year')->unsigned()->index();
            $table->enum('status',['Y','N'])->default('Y')->comment = 'Y = Active, N = Inactive';			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('work_experiences');
    }
}
