-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2017 at 04:21 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `navipi`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Y','N','E') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'E' COMMENT 'Y = Active, N = Block, E = Pending Activation',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', '$2y$10$3W5Mq9tTnDmG0/n.kvCDoun9TIjWFAFqHwGxNWgspKxis4olhyuGu', 'Y', 'WLkmN1XbT52AlNYFUcJFxyIF1FJngpMAqpOkus3KrDl3v2R1zM3ueam71QmB', NULL, '2017-01-18 17:20:26');

-- --------------------------------------------------------

--
-- Table structure for table `all_tropics`
--

CREATE TABLE `all_tropics` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `all_tropics`
--

INSERT INTO `all_tropics` (`id`, `title`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 'Street football', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(2, 'Racquet or racket', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(3, 'Interdependent team sports', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(4, 'Water sports', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(5, 'Underwater target shooting', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(6, 'Goal ', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(7, 'Sports that involve teams.', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(8, 'Bat-and-ball', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(9, 'Astronomy', 0, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(10, 'Australian rules football', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(11, 'Auto audiophilia', 2, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(12, 'Auto racing', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(13, 'Backpacking', 0, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(14, 'Badminton', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(15, 'BASE jumping', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(16, 'Baseball', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(17, 'Basketball', 1, '2016-11-14 18:30:00', '2016-11-14 18:30:00'),
(18, 'Baton twirling', 1, '2016-11-14 18:30:00', '2016-11-28 20:53:17'),
(20, 'Computer Science', 6, '2016-11-28 13:47:21', '2016-11-29 10:00:40'),
(23, 'Badminton', 1, '2016-11-28 15:14:55', '2016-11-29 10:00:06'),
(24, 'IT ', 0, '2016-11-28 15:16:29', '2016-12-11 21:02:31'),
(25, 'Japanese ', 2, '2016-11-28 17:39:19', '2016-11-28 17:39:19'),
(27, 'Finance ', 6, '2016-11-29 09:33:15', '2016-11-29 09:33:15'),
(28, 'Doctor ', 7, '2016-11-29 09:34:20', '2016-11-29 09:34:20'),
(29, 'Auditor ', 7, '2016-11-29 09:35:19', '2016-11-29 09:35:19'),
(30, 'Avatar ', 8, '2016-11-30 07:58:19', '2016-11-30 07:58:19'),
(31, 'VietinBank ', 9, '2016-11-30 10:15:03', '2016-11-30 10:15:03'),
(32, 'Vietnam ', 10, '2016-11-30 10:15:32', '2016-11-30 10:15:32'),
(43, 'kích hoạt thẻ RSA doanh nghiệp', 0, '2017-01-02 22:46:18', '2017-01-02 22:46:18'),
(44, 'Xử lý lỗi điện, giao dịch eFAST', 0, '2017-01-02 22:47:09', '2017-01-02 22:47:09'),
(45, 'chương trình thi đua khách hàng doanh nghiệp', 0, '2017-01-02 22:47:38', '2017-01-02 22:47:38'),
(46, 'chương trình tín dụng khách hàng doanh nghiệp', 0, '2017-01-02 22:47:53', '2017-01-02 22:47:53'),
(47, 'cho vay mua ô tô doanh nghiệp', 0, '2017-01-02 22:48:10', '2017-01-02 22:48:10'),
(48, 'Tiền gửi khách hàng doanh nghiệp', 0, '2017-01-07 21:52:03', '2017-01-07 21:52:03'),
(49, 'Chương trình tín dụng - Khách hàng doanh nghiệp', 0, '2017-01-07 22:28:22', '2017-01-07 22:28:52'),
(50, 'Chương trình thi đua - Khách hàng doanh nghiệp', 0, '2017-01-07 22:29:06', '2017-01-07 22:29:06'),
(51, 'Dịch vụ Ngân hàng điện tử - VietinBank eFAST', 0, '2017-01-07 22:36:54', '2017-01-07 22:36:54');

-- --------------------------------------------------------

--
-- Table structure for table `ask_questions`
--

CREATE TABLE `ask_questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `post_type` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1 = public , 2 = private ',
  `tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question_attached` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `send_as_anonymous` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Y = Active, N = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Công ty Cổ phần Tư vấn thiết kế và Phát triển đô thị (CDDC.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(2, 'Công ty Cổ phần Ánh Dương Việt Nam (Vinasun corp.)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(3, 'Công ty cổ phần Bamboo Capital (BCG)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(4, 'Công ty Cổ phần Bao bì Biên Hòa (SOVI)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(5, 'Công ty Cổ phần Bao bì dầu thực vật (V.Pack )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(6, 'Công ty Cổ phần Bao bì Nhựa Tân Tiến (TAPACK )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(7, 'Công ty Cổ phần Basa (BASACO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(8, 'Công ty Cổ phần Bất động sản Du lịch Ninh Vân Bay (Ninh Van Bay)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(9, 'Công ty cổ phần Bê tông Becamex (BECAMEX ACC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(10, 'Công ty Cổ phần Beton 6 (BT6 Corp)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(11, 'Công ty Cổ phần Bibica (BIBICA)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(12, 'Công ty Cổ phần Bông Bạch Tuyết (COBOVINA)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(13, 'Công ty Cổ phần Bóng đèn Điện Quang (DQ JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(14, 'Công ty Cổ phần Bóng đèn Phích nước Rạng Đông (RALACO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(15, 'Công ty Cổ phần Bột giặt Lix (Lixco)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(16, 'Công ty cổ phần Cảng Cát Lái (Cat Lai Port JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(17, 'Công ty Cổ phần Cảng Đồng Nai (Dong Nai Port)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(18, 'Công ty Cổ phần Cao su Bến Thành (BERUB JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(19, 'Công ty Cổ phần Cao su Đà Nẵng (DRC )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(20, 'Công ty Cổ phần Cao su Đồng Phú (DORUCO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(21, 'Công ty Cổ phần Cao su Hòa Bình (HRC )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(22, 'Công ty cổ phần Cao su Phước Hòa (PHURUCO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(23, 'Công ty Cổ phần Cao Su Sao Vàng (SRC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(24, 'Công ty Cổ phần Cao su Tây Ninh (TANIRUCO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(25, 'Công ty Cổ phần Cao su Thống Nhất (TRC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(26, 'Công ty Cổ phần Cấp nước Chợ Lớn (CHOLON WASUCO JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(27, 'Công ty Cổ phần Cấp nước Thủ Đức (Thu Duc Wasuco.J.S.C)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(28, 'Công ty Cổ phần Cáp Sài Gòn (SCC )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(29, 'Công ty Cổ phần Cáp treo Núi Bà Tây Ninh (CARTOUR )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(30, 'Công ty Cổ phần Cát Lợi (CLC. )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(31, 'Công ty Cổ phần Cavico Việt Nam Khai thác Mỏ và Xây dựng (CVCM.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(32, 'Công ty Cổ phần Chế biến Gỗ Đức Thành (DTWOODVN)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(33, 'Công ty Cổ phần Chế biến Gỗ Thuận An (T.A.C )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(34, 'Công ty Cổ phần Chế biến Hàng xuất khẩu Long An (LAFOOCO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(35, 'Công ty Cổ phần Chế biến Thực phẩm Kinh Đô Miền Bắc (NORTH.KINHDO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(36, 'Công ty Cổ phần Chế biến và Xuất nhập khẩu Thuỷ sản Cà Mau (CAMIMEX CORPORATION)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(37, 'Công ty Cổ phần Chiếu xạ An Phú (API.Co.)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(38, 'Công ty cổ phần Chứng khoán Ngân hàng Đầu tư và Phát triển Việt Nam (BSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(39, 'Công ty Cổ phần Chứng khoán Nông nghiệp và Phát triển Nông thôn (Agriseco)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(40, 'Công ty cổ phần Chứng khoán Sài Gòn (SSI)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(41, 'Công ty Cổ phần Chứng khoán Thành phố Hồ Chí Minh (HSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(42, 'Công ty Cổ phần Chứng khoán Thiên Việt (TVSC.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(43, 'Công ty Cổ phần Chương Dương (Chương Dương ACIC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(44, 'Công ty cổ phần CNG Việt Nam (CNG VietNam)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(45, 'Công ty Cổ phần Cơ điện lạnh (REE Corp )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(46, 'Công ty Cổ phần Cơ điện và Xây dựng Việt Nam (MECO JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(47, 'Công ty Cổ phần COMA18 (Coma18.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(48, 'Công ty Cổ phần Công nghệ Mạng và Truyền thông (INFONET.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(49, 'Công ty Cổ phần Công nghệ Sài Gòn Viễn Đông (Savitech JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(50, 'Công ty Cổ phần Công nghệ Tiên Phong (ITD Corporation)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(51, 'Công ty Cổ phần Công nghệ Viễn thông Sài Gòn (SAIGONTEL)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(52, 'Công ty Cổ phần Công nghiệp Cao su Miền Nam (CASUMINA)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(53, 'Công ty Cổ phần Công nghiệp Gốm sứ Taicera (TAICERA )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(54, 'Công ty Cổ phần Công nghiệp Khoáng sản Bình Thuận (BINH THUAN MINERAL)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(55, 'Công ty Cổ phần Công viên nước Đầm Sen (DASECO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(56, 'Công ty Cổ phần Đá Núi Nhỏ (NNC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(57, 'Công ty Cổ phần Đại lý Giao nhận Vận tải Xếp dỡ Tân Cảng (TAN CANG LOGISTICS)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(58, 'Công ty Cổ phần Đại lý Vận tải SAFI (SAFI)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(59, 'Công ty Cổ phần Đại Thiên Lộc (DTLS)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(60, 'Công ty Cổ phần Dầu Thực vật Tường An (Dầu Tường An )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(61, 'Công ty Cổ phần Đầu tư - Kinh doanh nhà (INTRESCO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(62, 'Công ty Cổ phần Đầu tư Alphanam (Alphanam JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(63, 'Công ty cổ phần Đầu tư AMD Group (AMD GROUP.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(64, 'Công ty cổ phần Đầu tư Căn nhà Mơ ước (Dream House Corp)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(65, 'Công ty Cổ phần Đầu tư Cao Su Quảng Nam (Quang Nam Rubber Investment JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(66, 'Công ty Cổ phần Đầu tư Cầu đường CII (CII Bridge & Road)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(67, 'Công ty Cổ phần Đầu tư Địa ốc Khang An (KHANG AN JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(68, 'Công ty Cổ phần Đầu tư Dịch vụ Hoàng Huy (HHS)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(69, 'Công ty Cổ phần Đầu tư Điện Tây Nguyên (TIC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(70, 'Công ty Cổ phần Đầu tư Du lịch và Phát triển Thủy sản (TRISEDCO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(71, 'Công ty cổ phần Đầu tư F.I.T (FIT Investment JSC.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(72, 'Công ty cổ phần Đầu tư Hạ tầng Kỹ thuật T.P Hồ Chí Minh (CII )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(73, 'Công ty Cổ phần Đầu tư Hạ tầng và Đô thị Dầu khí PVC (PVC - PETROLAND JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(74, 'Công ty Cổ phần Đầu tư Kinh doanh nhà Khang Điền (KhadiHouse.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(75, 'Công ty Cổ phần Đầu tư LDG (Dragonland JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(76, 'Công ty Cổ phần Đầu tư Năm Bảy Bảy (NBB Corp)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(77, 'Công ty cổ phần Đầu tư Nam Long (NLG)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(78, 'Công ty Cổ phần Đầu tư Phát triển Công nghệ Điện tử Viễn thông (ELCOM CORP)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(79, 'Công ty Cổ phần Đầu tư Phát triển Công nghiệp - Thương mại Củ Chi (CIDICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(80, 'Công ty Cổ phần Đầu tư Phát triển Cường Thuận IDICO (CTC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(81, 'Công ty Cổ phần Đầu tư Phát triển Đô thị và Khu Công nghiệp Sông Đà (SUDICO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(82, 'Công ty Cổ phần Đầu tư phát triển hạ tầng IDICO (IDICO-IDI.JSC )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(83, 'Công ty Cổ phần Đầu tư Phát triển Nhà và Đô thị Idico (IDICO - UDICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(84, 'Công ty Cổ phần Đầu tư Phát triển Thương mại Viễn Đông (VIDON )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(85, 'Công ty cổ phần Đầu tư Thế giới Di động (MWI CORP)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(86, 'Công ty Cổ phần Đầu tư Thương mại Bất động sản An Dương Thảo Điền (Công ty Cổ phần An Dương Thảo Điền)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(87, 'Công ty Cổ phần Ðầu tư Thương mại SMC (SMC )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(88, 'Công ty Cổ phần Đầu tư Thương mại Thủy Sản (INCOMFISH)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(89, 'Công ty Cổ phần Đầu tư và Công nghiệp Tân Tạo (ITACO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(90, 'Công ty Cổ phần Đầu tư và Dịch vụ Khánh Hội (Khahomex )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(91, 'Công ty cổ phần Đầu tư và Phát triển Cảng Đình Vũ (Dinh Vu Port J.S.C)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(92, 'Công ty Cổ phần Đầu tư và Phát triển Đa Quốc Gia I.D.I (IDI)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(93, 'Công ty cổ phần Đầu tư và Phát triển Đô thị Dầu khí Cửu Long (PVCL)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(94, 'Công ty cổ phần Đầu tư và Phát triển Đô thị Long Giang (Long Giang Land)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(95, 'Công ty Cổ phần Đầu tư và Phát triển Dự án Hạ tầng Thái Bình Dương (PPI JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(96, 'Công ty Cổ phần Đầu tư và Phát triển KSH (HAMICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(97, 'Công ty Cổ phần Đầu tư và Phát triển Nhà đất COTEC (COTECLAND JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(98, 'Công ty Cổ phần Đầu tư và Phát triển Sacom (SACOM )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(99, 'Công ty cổ phần Đầu tư và Sản xuất Thống Nhất (Công ty cổ phần Đầu tư và Sản Xuất Thống Nhất)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(100, 'Công ty Cổ phần Đầu tư và Thương mại DIC (DIC- INTRACO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(101, 'Công ty Cổ phần Đầu tư và Xây dựng Bưu điện (PTIC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(102, 'Công ty Cổ phần Đầu tư và Xây dựng Cấp thoát nước (WASECO.)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(103, 'Công ty Cổ phần Đầu tư và Xây dựng HUD1 (HUD1.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(104, 'Công ty cổ phần Đầu tư và Xây dựng HUD3 (HUD3)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(105, 'Công ty Cổ phần Đầu tư và Xây dựng Tiền Giang (TICCO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(106, 'Công ty Cổ phần Đầu tư Xây dựng 3-2 (CIC3-2)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(107, 'Công ty Cổ phần Đầu tư Xây dựng Bình Chánh (BCCI)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(108, 'Công ty cổ phần Đầu tư Xây dựng Thương mại Dầu khí-IDICO (PVC-IDICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(109, 'Công ty cổ phần Dây Cáp Điện Việt Nam (CADIVI)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(110, 'Công ty Cổ phần Dây và Cáp điện Taya Việt Nam (TAYA VIỆT NAM )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(111, 'Công ty Cổ phần Đệ Tam (DETACO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(112, 'Công ty Cổ phần Dệt may - Đầu tư - Thương mại Thành Công (TCG)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(113, 'Công ty Cổ phần Dịch vụ Hàng hóa Nội Bài (NCTS)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(114, 'Công ty Cổ phần Dịch vụ Ô tô Hàng Xanh (HAXACO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(115, 'Công ty Cổ phần Dịch vụ tổng hợp Sài Gòn (SAVICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(116, 'Công ty Cổ phần Dịch vụ và Xây dựng Địa ốc Đất Xanh (ĐẤT XANH)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(117, 'Công ty Cổ phần Điện lực Dầu khí Nhơn Trạch 2 (PV Power NT2)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(118, 'Công ty Cổ phần Điện lực Khánh Hòa (KHPC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(119, 'Công ty Cổ phần Docimexco (Docimexco)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(120, 'Công ty Cổ phần Đông Hải Bến Tre (DOHACO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(121, 'Công ty cổ phần Du lịch - Dịch vụ Hội An (HOIAN TOURIST SERVICE Co)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(122, 'Công ty Cổ phần Du lịch Thành Thành Công (Thanh Thanh Cong Tourist Joint Stock Company)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(123, 'Công ty Cổ phần Dược Hậu Giang (DHG)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(124, 'Công ty Cổ phần Dược phẩm Cửu Long (PHARIMEXCO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(125, 'Công ty Cổ phần Dược phẩm IMEXPHARM (IMEXPHARM )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(126, 'Công ty cổ phần Dược phẩm OPC (OPC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(127, 'Công ty Cổ phần Dược phẩm Viễn Đông (VIEN DONG PHARMA JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(128, 'Công ty Cổ phần Đường Biên Hoà (BSJC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(129, 'Công ty Cổ phần Đường Ninh Hòa (NSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(130, 'Công ty cổ phần Everpia Việt Nam (Everpia)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(131, 'Công ty Cổ phần FPT (FPT Corp)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(132, 'Công ty Cổ phần Full Power (FULL POWER JS CO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(133, 'Công ty Cổ phần Gạch men Chang Yih (Chang Yih )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(134, 'Công ty Cổ phần Gemadept (Gemadept )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(135, 'Công ty Cổ phần Giống cây trồng Miền Nam (SSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(136, 'Công ty cổ phần Giống cây trồng Trung ương (NSC )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(137, 'Công ty Cổ phần Gò Đàng (GODACO_SEAFOOD)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(138, 'Công ty cổ phần Hạ tầng nước Sài Gòn (SII JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(139, 'Công ty Cổ phần HACISCO (HACISCO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(140, 'Công ty Cổ phần Hóa - Dược phẩm MEKOPHAR (MEKOPHAR)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(141, 'Công ty Cổ phần Hóa An (DHA )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(142, 'Công ty Cổ phần Hóa chất Cơ bản miền Nam (SBCC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(143, 'Công ty Cổ phần Hoàng Anh Gia Lai (HAGL)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(144, 'Công ty Cổ phần Hợp tác kinh tế và Xuất nhập khẩu SAVIMEX (SAVIMEX )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(145, 'Công ty Cổ phần Hùng Vương (HV Corp)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(146, 'Công ty cổ phần In và Bao bì Mỹ Châu (MCP )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(147, 'Công ty Cổ phần Kết cấu Kim loại và Lắp máy Dầu khí (PVC-MS)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(148, 'Công ty Cổ phần Khai thác và Chế biến Khoáng sản Bắc Giang (BAC GIANG EXPLOITABLE.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(149, 'Công ty Cổ phần Khai thác và Chế biến Khoáng sản Lào Cai (LAOCAI JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(150, 'Công ty Cổ phần Kho vận Miền Nam (Sotrans)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(151, 'Công ty cổ phần Khoáng sản Bình Định (BIMICO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(152, 'Công ty cổ phần Khoáng sản FECON (Fecon Mining)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(153, 'Công ty Cổ phần Khoáng sản Na Rì Hamico (NA RI HAMICO JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(154, 'Công ty Cổ phần Khoáng sản và Vật liệu xây dựng Lâm Đồng (LBM )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(155, 'Công ty Cổ phần Khoáng sản và Xây dựng Bình Dương (BIMICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(156, 'Công ty Cổ phần Khử trùng Việt Nam (VFC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(157, 'Công ty Cổ phần Kim khí Thành phố Hồ Chí Minh (HMC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(158, 'Công ty Cổ phần Kinh doanh và Phát triển Bình Dương (TDC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(159, 'Công ty Cổ phần Kỹ nghệ Đô Thành (DTT )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(160, 'Công ty Cổ phần Kỹ Nghệ Lạnh (SEAREFICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(161, 'Công ty cổ phần Kỹ thuật nền móng và Công trình ngầm FECON', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(162, 'Công ty Cổ phần Kỹ thuật và Ô tô Trường Long (Truong Long JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(163, 'Công ty cổ phần LICOGI 16 (LICOGI 16)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(164, 'Công ty cổ phần Lilama 10 (Lilama 10.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(165, 'Công ty Cổ phần Lilama 18 (LILAMA 18 JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(166, 'Công ty cổ phần Logistics Vinalink (VINALINK)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(167, 'Công ty Cổ phần Long Hậu (LHC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(168, 'Công ty Cổ phần Lương thực Thực phẩm Vĩnh Long (VinhlongFood)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(169, 'Công ty Cổ phần MHC (Marina Holdings)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(170, 'Công ty Cổ phần Mía đường Lam Sơn (LASUCO JSC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(171, 'Công ty Cổ phần Mía đường Nhiệt điện Gia Lai (SEC)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(172, 'Công ty cổ phần Mía đường Thành Thành Công Tây Ninh (SBT)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(173, 'Công ty Cổ phần miền Đông', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(174, 'Công ty Cổ phần Mirae (MIRAE.', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(175, 'Công ty Cổ phần Nafoods Group (NAFOODS GROUP)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(176, 'Công ty Cổ phần Nam Việt (NAVICO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(177, 'Công ty Cổ phần Nam Việt (NAVIFICO )', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(178, 'Công ty Cổ phần Ngô Han (NHW)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(179, 'Công ty Cổ phần Ngoại thương và Phát triển Đầu tư Thành phố Hồ Chí Minh (FIDECO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(180, 'Công ty Cổ phần Nhà Việt Nam (HVN J.S CO)', '2017-01-19 17:51:32', '2017-01-19 17:51:32'),
(181, 'Công ty Cổ phần Cavico Việt Nam Khai thác Mỏ và Xây dựng (CVCM., JSC )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(182, 'Công ty Cổ phần Chứng khoán Thiên Việt (TVSC.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(183, 'Công ty Cổ phần COMA18 (Coma18.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(184, 'Công ty Cổ phần Công nghệ Mạng và Truyền thông (INFONET.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(185, 'Công ty cổ phần Đầu tư AMD Group (AMD GROUP.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(186, 'Công ty cổ phần Đầu tư F.I.T (FIT Investment JSC.,)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(187, 'Công ty Cổ phần Đầu tư Kinh doanh nhà Khang Điền (KhadiHouse., JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(188, 'Công ty Cổ phần Đầu tư và Xây dựng HUD1 (HUD1.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(189, 'Công ty Cổ phần Khai thác và Chế biến Khoáng sản Bắc Giang (BAC GIANG EXPLOITABLE.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(190, 'Công ty cổ phần Lilama 10 (Lilama 10.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(191, 'Công ty Cổ phần Mirae (MIRAE., JSC )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(192, 'Công ty Cổ phần Nhiên liệu Sài Gòn (SFC )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(193, 'Công ty Cổ phần Nhiệt điện Bà Rịa', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(194, 'Công ty Cổ phần Nhiệt điện Phả Lại (PLPC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(195, 'Công ty Cổ phần Nhựa Bình Minh (BMPLASCO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(196, 'Công ty Cổ phần Nhựa Rạng Đông (RDP JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(197, 'Công ty Cổ phần Nhựa Tân Đại Hưng (TAN DAI HUNG PLASTIC J.S. CO.,)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(198, 'Công ty Cổ phần Nông dược H.A.I (HAI jsc)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(199, 'Công ty cổ phần Nông nghiệp Quốc tế Hoàng Anh Gia Lai (HAGL AGRICO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(200, 'Công ty Cổ phần NTACO (NTACO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(201, 'Công ty Cổ phần Nước giải khát Chương Dương (CDBECO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(202, 'Công ty Cổ phần Nước giải khát Sài Gòn (TRIBECO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(203, 'Công ty Cổ phần Ô tô TMT (TMT AUTO., JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(204, 'Công ty cổ phần Phân bón Bình Điền (BFC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(205, 'Công ty Cổ phần Phân bón Dầu khí Cà Mau (PVCFC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(206, 'Công ty Cổ phần Phân bón Miền Nam (SFJC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(207, 'Công ty cổ phần Phân lân nung chảy Văn Điển (VADFCO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(208, 'Công ty Cổ phần Phân phối Khí thấp áp Dầu khí Việt Nam (PV GAS D JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(209, 'Công ty cổ phần Phát triển Bất động sản Phát Đạt', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(210, 'Công ty Cổ phần Phát triển Đô thị Công nghiệp Số 2 (D2D)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(211, 'Công ty Cổ phần Phát triển Đô thị Từ Liêm (LIDECO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(212, 'Công ty Cổ phần Phát triển Hạ tầng Kỹ thuật (BECAMEX IJC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(213, 'Công ty Cổ phần Phát triển nhà Bà Rịa-Vũng Tàu (Hodeco)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(214, 'Công ty Cổ phần Phát triển Nhà Thủ Đức (THUDUC HOUSE )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(215, 'Công ty Cổ phần Phú Tài (Phutaico)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(216, 'Công ty Cổ phần Pin Ắc quy Miền Nam (PINACO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(217, 'Công ty Cổ phần Quốc Cường Gia Lai (QCGL)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(218, 'Công ty Cổ phần Quốc tế Hoàng Gia (Royal International Corp )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(219, 'Công ty cổ phần Quốc tế Sơn Hà (SONHA.,CORP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(220, 'Công ty Cổ phần S.P.M (S.P.M CORP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(221, 'Công ty Cổ phần Sản xuất Kinh doanh Xuất nhập khẩu Bình Thạnh (GILIMEX )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(222, 'Công ty cổ phần Sản xuất Kinh doanh Xuất nhập khẩu Dịch vụ và Đầu tư Tân Bình (Tanimex J.S.C)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(223, 'Công ty Cổ phần Sản xuất Thương mại May Sài Gòn (GARMEX SAIGON JS)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(224, 'Công ty Cổ phần Sản xuất và Thương mại Phúc Tiến (PHT JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(225, 'Công ty Cổ phần Siêu Thanh (SJC )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(226, 'Công ty cổ phần Sợi Thế Kỷ', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(227, 'Công ty cổ phần Sonadezi Long Thành (SZL)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(228, 'Công ty Cổ phần Sông Ba (SBA)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(229, 'Công ty Cổ phần Sữa Việt Nam (VINAMILK )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(230, 'Công ty Cổ phần Tài nguyên (TAI NGUYEN CORP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(231, 'Công ty Cổ phần Tập đoàn Công nghệ CMC (CMC Corp)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(232, 'Công ty cổ phần Tập đoàn Container Việt Nam (Viconship)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(233, 'Công ty Cổ phần Tập đoàn Đại Dương', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(234, 'Công ty Cổ phần Tập đoàn Dầu khí An Pha (AN PHA S.G PETROL JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(235, 'Công ty Cổ phần Tập đoàn Đức Long Gia Lai (DUCLONG GIA LAI GROUP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(236, 'Công ty cổ phần Tập đoàn FLC (FLC., JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(237, 'Công ty Cổ phần Tập đoàn Hà Đô (Hadoco., JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(238, 'Công ty Cổ phần Tập đoàn Hapaco (HAPACO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(239, 'Công ty cổ phần Tập đoàn Hòa Phát (Hoa Phat Group)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(240, 'Công ty Cổ phần Tập đoàn Hoa Sen (HOASEN GROUP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(241, 'Công ty Cổ phần Tập đoàn Hoàng Long (Hoang Long)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(242, 'Công ty Cổ phần Tập đoàn Kido (Kido Group)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(243, 'Công ty Cổ phần Tập đoàn Kỹ nghệ Gỗ Trường Thành (TTFC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(244, 'Công ty Cổ phần Tập đoàn MaSan (Masan Group)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(245, 'Công ty Cổ phần Tập đoàn Nhựa Đông Á (Tập đoàn Đông Á)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(246, 'Công ty Cổ phần Tập đoàn PAN (Pan Pacific)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(247, 'Công ty Cổ phần Tập đoàn Sao Mai (SAOMAI GROUP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(248, 'Công ty Cổ phần Tập đoàn Thép Tiến Lên (TLC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(249, 'Công ty Cổ phần Tập đoàn Thiên Long (Tập đoàn Thiên Long)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(250, 'Công ty Cổ phần Tập đoàn Thủy sản Minh Phú (Minh Phu Seafood Corp)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(251, 'Công ty Cổ phần Tàu cao tốc Superdong – Kiên Giang (Superdong FF-(KG) Joint Stock Company)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(252, 'Công ty cổ phần Thế giới số (DIGIWORLD)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(253, 'Công ty cổ phần Thế kỷ 21 (C21)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(254, 'Công ty Cổ phần Thép Nam Kim (Nakisco)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(255, 'Công ty Cổ phần Thép Pomina (CÔNG TY THÉP POMINA)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(256, 'Công ty Cổ phần Thép Việt Ý (VISCO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(257, 'Công ty Cổ phần Thiết bị Phụ tùng Sài Gòn (SAIGON MACHINCO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(258, 'Công ty cổ phần Thiết bị Y tế Việt Nhật (JV., JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(259, 'Công ty Cổ phần Thuận Thảo (GTT)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(260, 'Công ty Cổ phần Thức ăn Chăn nuôi Việt Thắng (VTFEED)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(261, 'Công ty Cổ phần Thực phẩm Quốc tế (INTERFOOD )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(262, 'Công ty Cổ phần Thực phẩm Sao Ta (Sao Ta Foods Joint Stock Company)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(263, 'Công ty Cổ phần Thương mại - Dịch vụ Bến Thành (BEN THANH TSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(264, 'Công ty cổ phần Thương mại và Khai thác Khoáng sản Dương Hiếu (DH JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(265, 'Công ty Cổ phần Thương mại Xuất nhập khẩu Thiên Nam (TENIMEX )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(266, 'Công ty Cổ phần Thương nghiệp Cà Mau (Camex)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(267, 'Công ty Cổ phần Thủy điện – Điện lực 3 (PC3,HP.Co)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(268, 'Công ty Cổ phần Thủy điện Cần Đơn (CAN DON HSC )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(269, 'Công ty cổ phần Thủy điện Miền Nam (SHP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(270, 'Công ty cổ phần Thủy điện Thác Bà (TBHPC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(271, 'Công ty cổ phần Thủy điện Thác Mơ (TMHPC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(272, 'Công ty Cổ phần Thủy điện Vĩnh Sơn - Sông Hinh (VSHPC )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(273, 'Công ty Cổ phần Thủy hải sản Việt Nhật (VISEA CORP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(274, 'Công ty Cổ phần Thủy Sản Cửu Long (CUULONG SEAPRO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(275, 'Công ty Cổ phần Thủy sản Mekong (MEKONGFISH)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(276, 'Công ty cổ phần Thủy sản số 4 (Seapriexco No 4 )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(277, 'Công ty Cổ phần TIE (TIE)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(278, 'Công ty Cổ phần Transimex-Saigon (Transforwarding Warehousing Joint Stock Corporatio)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(279, 'Công ty Cổ phần TRAPHACO (TRAPHACO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(280, 'Công ty cổ phần Tư vấn Xây dựng Điện 1 (PECC1)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(281, 'Công ty cổ phần Tư vấn-Thương mại-Dịch vụ Địa ốc Hoàng Quân (HOANG QUAN CORP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(282, 'Công ty cổ phần Vận chuyển Sài Gòn Tourist (SATRACO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(283, 'Công ty Cổ phần Văn hóa Phương Nam (PNC )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(284, 'Công ty Cổ phần Vạn Phát Hưng (VPH Corp)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(285, 'Công ty Cổ phần Vận tải biển Việt Nam (VOSCO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(286, 'Công ty Cổ phần Vận tải Biển Vinaship (VINASHIP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(287, 'Công ty Cổ phần Vận tải Đa phương thức Duyên Hải (TASA DUYENHAI)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(288, 'Công ty Cổ phần Vận tải Hà Tiên (HATIEN TRANSCO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(289, 'Công ty cổ phần Vận tải Sản phẩm khí quốc tế (GAS SHIPPING JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(290, 'Công ty Cổ phần Vận tải và Giao nhận bia Sài Gòn (SABETRAN JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(291, 'Công ty Cổ phần Vận tải và Xếp dỡ Hải An (HAIANTS.,JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(292, 'Công ty Cổ phần Vận tải Xăng dầu Đường thủy Petrolimex (PJTACO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(293, 'Công ty Cổ phần Vận tải Xăng dầu VIPCO (VIPCO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(294, 'Công ty Cổ phần Vận tải Xăng dầu VITACO (VITACO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(295, 'Công ty Cổ phần Vàng bạc Đá quý Phú Nhuận (PNJ)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(296, 'Công ty Cổ phần Vật tư - Xăng dầu (COMECO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(297, 'Công ty Cổ phần Vật tư kỹ thuật Nông nghiệp Cần Thơ (TSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(298, 'Công ty Cổ phần VICEM Vật liệu Xây dựng Đà Nẵng (COXIVA)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(299, 'Công ty Cổ phần Viettronics Tân Bình (VTB )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(300, 'Công ty Cổ phần VinaCafé Biên Hòa (VINACAFÉ BH)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(301, 'Công ty Cổ phần Vĩnh Hoàn (VINHHOAN CORP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(302, 'Công ty Cổ phần Xây dựng 47 (CC47)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(303, 'Công ty Cổ phần Xây dựng Công nghiệp DESCON (DESCON)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(304, 'Công ty Cổ phần Xây dựng công nghiệp và dân dụng Dầu khí (PVC-IC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(305, 'Công ty Cổ phần Xây dựng Cotec (COTECCONS)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(306, 'Công ty Cổ phần Xây dựng số 5 (SC5)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(307, 'Công ty Cổ phần Xây dựng và Giao thông Bình Dương (Becamex BCE)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(308, 'Công ty cổ phần Xây dựng và Kinh doanh Địa ốc Hoà Bình (HoaBinh Corporation)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(309, 'Công ty cổ phần Xây dựng và Phát triển Đô thị tỉnh Bà Rịa-Vũng Tàu (UDEC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(310, 'Công ty Cổ phần Xây lắp Đường ống Bể chứa Dầu khí (PVC – PT)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(311, 'Công ty Cổ phần Xây lắp và Địa ốc Vũng Tàu (VTRECJ. Co.)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(312, 'Công ty Cổ phần Xi Măng Hà Tiên 1 (HA TIEN 1.J.S.CO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(313, 'Công ty Cổ phần Xi măng Hà Tiên 2 (HATIEN2.Co)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(314, 'Công ty Cổ phần Xi măng Vicem Hải Vân (Vicem Hải Vân)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(315, 'Công ty cổ phần Xuất nhập khẩu An Giang (Angimex)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(316, 'Công ty Cổ phần Xuất nhập khẩu Lâm Thủy sản Bến Tre (FAQUIMEX)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(317, 'Công ty Cổ phần Xuất nhập khẩu Petrolimex (PITCO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(318, 'Công ty Cổ phần Xuất nhập khẩu Quảng Bình (QUANG BINH JSC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(319, 'Công ty Cổ phần Xuất nhập khẩu Thủy sản An Giang (AGIFISH Co. )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(320, 'Công ty Cổ phần Xuất nhập khẩu Thủy sản Bến Tre (AQUATEX BENTRE )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(321, 'Công ty cổ phần Xuất nhập khẩu Thủy sản Cửu Long An Giang (CL - Fish Corp )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(322, 'Công ty Cổ phần Xuất nhập khẩu Y tế Domesco (DOMESCO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(323, 'Công ty cổ phần Y Dược phẩm Vimedimex (Vimedimex)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(324, 'Công ty TNHH Một thành viên Vinpearl (VINPEARL)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(325, 'CTCP Thuốc sát trùng Việt Nam (Vipesco)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(326, 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(327, 'Ngân hàng Thương mại cổ phần Đầu tư và Phát triển Việt Nam (BIDV)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(328, 'Ngân hàng Thương mại cổ phần Ngoại thương Việt Nam (VIETCOMBANK)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(329, 'Ngân hàng Thương mại Cổ phần Quân đội (MB)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(330, 'Ngân hàng Thương mại Cổ phần Sài Gòn Thương Tín (SACOMBANK)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(331, 'Ngân hàng Thương mại Cổ phần Xuất nhập khẩu Việt Nam (Vietnam Eximbank)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(332, 'Quỹ Đầu tư Cân bằng Prudential (PRUBF1 )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(333, 'Quỹ Đầu tư Chứng khoán Việt Nam (VF1)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(334, 'Quỹ đầu tư Doanh nghiệp Hàng đầu Việt Nam (Quỹ đầu tư VF4)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(335, 'Quỹ Đầu tư Năng động Việt Nam (VFM)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(336, 'Quỹ đầu tư tăng trưởng ACB (ACBGF)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(337, 'Quỹ đầu tư tăng trưởng Manulife (MAFPF1)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(338, 'Quỹ ETF VFMVN30 (VFMVN30 ETF)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(339, 'Tập đoàn Bảo Việt (TẬP ĐOÀN BẢO VIỆT)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(340, 'Tập đoàn Vingroup - Công ty Cổ phần (VINGROUP JOINT STOCK COMPANY)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(341, 'Tổng Công ty Cổ phần Bảo hiểm Ngân hàng Đầu tư và phát triển Việt Nam (BIC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(342, 'Tổng Công ty cổ phần Bảo hiểm Petrolimex (PJICO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(343, 'Tổng Công ty Cổ phần Bảo Minh (Bao Minh)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(344, 'Tổng Công ty Cổ phần Đầu tư Phát triển Xây dựng (DIC CORP)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(345, 'Tổng Công ty Cổ phần Dịch vụ Tổng hợp Dầu khí (PETROSETCO )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(346, 'Tổng Công ty Cổ phần Khoan và Dịch vụ Khoan Dầu khí (PV DRILLING )', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(347, 'Tổng công ty Cổ phần Vận tải Dầu khí (PV Trans Corp)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(348, 'Tổng công ty Cổ phần Xây dựng điện Việt Nam (VNECO)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(349, 'Tổng Công ty Gas Petrolimex-CTCP (PGC)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(350, 'Tổng Công ty Khí Việt Nam-CTCP (PVGas)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(351, 'Tổng Công ty Phân bón và Hóa chất Dầu khí-CTCP (PVFCCo)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(352, 'Tổng Công ty Phát triển Đô thị Kinh Bắc-CTCP (KinhBac City)', '2017-01-19 22:21:15', '2017-01-19 22:21:15'),
(353, 'Tổng Công ty Tài chính Cổ phần Dầu khí Việt Nam (PetroVietnam Finance Joint Stock Corporation)', '2017-01-19 22:21:15', '2017-01-19 22:21:15');

-- --------------------------------------------------------

--
-- Table structure for table `contact_ifnos`
--

CREATE TABLE `contact_ifnos` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `info1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `info2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `info3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `info4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contact_ifnos`
--

INSERT INTO `contact_ifnos` (`id`, `user_id`, `info1`, `info2`, `info3`, `info4`, `created_at`, `updated_at`) VALUES
(13, 146, 'maithao.ba@gmail.com', '', '', '', '2017-02-07 21:21:04', '2017-02-07 21:21:04');

-- --------------------------------------------------------

--
-- Table structure for table `contact_messages`
--

CREATE TABLE `contact_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_name` varchar(255) NOT NULL,
  `sender_email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `idCountry` int(5) NOT NULL,
  `countryCode` char(2) NOT NULL DEFAULT '',
  `countryName` varchar(45) NOT NULL DEFAULT '',
  `currencyCode` char(3) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`idCountry`, `countryCode`, `countryName`, `currencyCode`) VALUES
(1, 'AD', 'Andorra', 'EUR'),
(2, 'AE', 'United Arab Emirates', 'AED'),
(3, 'AF', 'Afghanistan', 'AFN'),
(4, 'AG', 'Antigua and Barbuda', 'XCD'),
(5, 'AI', 'Anguilla', 'XCD'),
(6, 'AL', 'Albania', 'ALL'),
(7, 'AM', 'Armenia', 'AMD'),
(8, 'AO', 'Angola', 'AOA'),
(9, 'AQ', 'Antarctica', ''),
(10, 'AR', 'Argentina', 'ARS'),
(11, 'AS', 'American Samoa', 'USD'),
(12, 'AT', 'Austria', 'EUR'),
(13, 'AU', 'Australia', 'AUD'),
(14, 'AW', 'Aruba', 'AWG'),
(15, 'AX', 'Aland', 'EUR'),
(16, 'AZ', 'Azerbaijan', 'AZN'),
(17, 'BA', 'Bosnia and Herzegovina', 'BAM'),
(18, 'BB', 'Barbados', 'BBD'),
(19, 'BD', 'Bangladesh', 'BDT'),
(20, 'BE', 'Belgium', 'EUR'),
(21, 'BF', 'Burkina Faso', 'XOF'),
(22, 'BG', 'Bulgaria', 'BGN'),
(23, 'BH', 'Bahrain', 'BHD'),
(24, 'BI', 'Burundi', 'BIF'),
(25, 'BJ', 'Benin', 'XOF'),
(26, 'BL', 'Saint Barthelemy', 'EUR'),
(27, 'BM', 'Bermuda', 'BMD'),
(28, 'BN', 'Brunei', 'BND'),
(29, 'BO', 'Bolivia', 'BOB'),
(30, 'BQ', 'Bonaire', 'USD'),
(31, 'BR', 'Brazil', 'BRL'),
(32, 'BS', 'Bahamas', 'BSD'),
(33, 'BT', 'Bhutan', 'BTN'),
(34, 'BV', 'Bouvet Island', 'NOK'),
(35, 'BW', 'Botswana', 'BWP'),
(36, 'BY', 'Belarus', 'BYR'),
(37, 'BZ', 'Belize', 'BZD'),
(38, 'CA', 'Canada', 'CAD'),
(39, 'CC', 'Cocos [Keeling] Islands', 'AUD'),
(40, 'CD', 'Democratic Republic of the Congo', 'CDF'),
(41, 'CF', 'Central African Republic', 'XAF'),
(42, 'CG', 'Republic of the Congo', 'XAF'),
(43, 'CH', 'Switzerland', 'CHF'),
(44, 'CI', 'Ivory Coast', 'XOF'),
(45, 'CK', 'Cook Islands', 'NZD'),
(46, 'CL', 'Chile', 'CLP'),
(47, 'CM', 'Cameroon', 'XAF'),
(48, 'CN', 'China', 'CNY'),
(49, 'CO', 'Colombia', 'COP'),
(50, 'CR', 'Costa Rica', 'CRC'),
(51, 'CU', 'Cuba', 'CUP'),
(52, 'CV', 'Cape Verde', 'CVE'),
(53, 'CW', 'Curacao', 'ANG'),
(54, 'CX', 'Christmas Island', 'AUD'),
(55, 'CY', 'Cyprus', 'EUR'),
(56, 'CZ', 'Czech Republic', 'CZK'),
(57, 'DE', 'Germany', 'EUR'),
(58, 'DJ', 'Djibouti', 'DJF'),
(59, 'DK', 'Denmark', 'DKK'),
(60, 'DM', 'Dominica', 'XCD'),
(61, 'DO', 'Dominican Republic', 'DOP'),
(62, 'DZ', 'Algeria', 'DZD'),
(63, 'EC', 'Ecuador', 'USD'),
(64, 'EE', 'Estonia', 'EUR'),
(65, 'EG', 'Egypt', 'EGP'),
(66, 'EH', 'Western Sahara', 'MAD'),
(67, 'ER', 'Eritrea', 'ERN'),
(68, 'ES', 'Spain', 'EUR'),
(69, 'ET', 'Ethiopia', 'ETB'),
(70, 'FI', 'Finland', 'EUR'),
(71, 'FJ', 'Fiji', 'FJD'),
(72, 'FK', 'Falkland Islands', 'FKP'),
(73, 'FM', 'Micronesia', 'USD'),
(74, 'FO', 'Faroe Islands', 'DKK'),
(75, 'FR', 'France', 'EUR'),
(76, 'GA', 'Gabon', 'XAF'),
(77, 'GB', 'United Kingdom', 'GBP'),
(78, 'GD', 'Grenada', 'XCD'),
(79, 'GE', 'Georgia', 'GEL'),
(80, 'GF', 'French Guiana', 'EUR'),
(81, 'GG', 'Guernsey', 'GBP'),
(82, 'GH', 'Ghana', 'GHS'),
(83, 'GI', 'Gibraltar', 'GIP'),
(84, 'GL', 'Greenland', 'DKK'),
(85, 'GM', 'Gambia', 'GMD'),
(86, 'GN', 'Guinea', 'GNF'),
(87, 'GP', 'Guadeloupe', 'EUR'),
(88, 'GQ', 'Equatorial Guinea', 'XAF'),
(89, 'GR', 'Greece', 'EUR'),
(90, 'GS', 'South Georgia and the South Sandwich Islands', 'GBP'),
(91, 'GT', 'Guatemala', 'GTQ'),
(92, 'GU', 'Guam', 'USD'),
(93, 'GW', 'Guinea-Bissau', 'XOF'),
(94, 'GY', 'Guyana', 'GYD'),
(95, 'HK', 'Hong Kong', 'HKD'),
(96, 'HM', 'Heard Island and McDonald Islands', 'AUD'),
(97, 'HN', 'Honduras', 'HNL'),
(98, 'HR', 'Croatia', 'HRK'),
(99, 'HT', 'Haiti', 'HTG'),
(100, 'HU', 'Hungary', 'HUF'),
(101, 'ID', 'Indonesia', 'IDR'),
(102, 'IE', 'Ireland', 'EUR'),
(103, 'IL', 'Israel', 'ILS'),
(104, 'IM', 'Isle of Man', 'GBP'),
(105, 'IN', 'India', 'INR'),
(106, 'IO', 'British Indian Ocean Territory', 'USD'),
(107, 'IQ', 'Iraq', 'IQD'),
(108, 'IR', 'Iran', 'IRR'),
(109, 'IS', 'Iceland', 'ISK'),
(110, 'IT', 'Italy', 'EUR'),
(111, 'JE', 'Jersey', 'GBP'),
(112, 'JM', 'Jamaica', 'JMD'),
(113, 'JO', 'Jordan', 'JOD'),
(114, 'JP', 'Japan', 'JPY'),
(115, 'KE', 'Kenya', 'KES'),
(116, 'KG', 'Kyrgyzstan', 'KGS'),
(117, 'KH', 'Cambodia', 'KHR'),
(118, 'KI', 'Kiribati', 'AUD'),
(119, 'KM', 'Comoros', 'KMF'),
(120, 'KN', 'Saint Kitts and Nevis', 'XCD'),
(121, 'KP', 'North Korea', 'KPW'),
(122, 'KR', 'South Korea', 'KRW'),
(123, 'KW', 'Kuwait', 'KWD'),
(124, 'KY', 'Cayman Islands', 'KYD'),
(125, 'KZ', 'Kazakhstan', 'KZT'),
(126, 'LA', 'Laos', 'LAK'),
(127, 'LB', 'Lebanon', 'LBP'),
(128, 'LC', 'Saint Lucia', 'XCD'),
(129, 'LI', 'Liechtenstein', 'CHF'),
(130, 'LK', 'Sri Lanka', 'LKR'),
(131, 'LR', 'Liberia', 'LRD'),
(132, 'LS', 'Lesotho', 'LSL'),
(133, 'LT', 'Lithuania', 'LTL'),
(134, 'LU', 'Luxembourg', 'EUR'),
(135, 'LV', 'Latvia', 'EUR'),
(136, 'LY', 'Libya', 'LYD'),
(137, 'MA', 'Morocco', 'MAD'),
(138, 'MC', 'Monaco', 'EUR'),
(139, 'MD', 'Moldova', 'MDL'),
(140, 'ME', 'Montenegro', 'EUR'),
(141, 'MF', 'Saint Martin', 'EUR'),
(142, 'MG', 'Madagascar', 'MGA'),
(143, 'MH', 'Marshall Islands', 'USD'),
(144, 'MK', 'Macedonia', 'MKD'),
(145, 'ML', 'Mali', 'XOF'),
(146, 'MM', 'Myanmar [Burma]', 'MMK'),
(147, 'MN', 'Mongolia', 'MNT'),
(148, 'MO', 'Macao', 'MOP'),
(149, 'MP', 'Northern Mariana Islands', 'USD'),
(150, 'MQ', 'Martinique', 'EUR'),
(151, 'MR', 'Mauritania', 'MRO'),
(152, 'MS', 'Montserrat', 'XCD'),
(153, 'MT', 'Malta', 'EUR'),
(154, 'MU', 'Mauritius', 'MUR'),
(155, 'MV', 'Maldives', 'MVR'),
(156, 'MW', 'Malawi', 'MWK'),
(157, 'MX', 'Mexico', 'MXN'),
(158, 'MY', 'Malaysia', 'MYR'),
(159, 'MZ', 'Mozambique', 'MZN'),
(160, 'NA', 'Namibia', 'NAD'),
(161, 'NC', 'New Caledonia', 'XPF'),
(162, 'NE', 'Niger', 'XOF'),
(163, 'NF', 'Norfolk Island', 'AUD'),
(164, 'NG', 'Nigeria', 'NGN'),
(165, 'NI', 'Nicaragua', 'NIO'),
(166, 'NL', 'Netherlands', 'EUR'),
(167, 'NO', 'Norway', 'NOK'),
(168, 'NP', 'Nepal', 'NPR'),
(169, 'NR', 'Nauru', 'AUD'),
(170, 'NU', 'Niue', 'NZD'),
(171, 'NZ', 'New Zealand', 'NZD'),
(172, 'OM', 'Oman', 'OMR'),
(173, 'PA', 'Panama', 'PAB'),
(174, 'PE', 'Peru', 'PEN'),
(175, 'PF', 'French Polynesia', 'XPF'),
(176, 'PG', 'Papua New Guinea', 'PGK'),
(177, 'PH', 'Philippines', 'PHP'),
(178, 'PK', 'Pakistan', 'PKR'),
(179, 'PL', 'Poland', 'PLN'),
(180, 'PM', 'Saint Pierre and Miquelon', 'EUR'),
(181, 'PN', 'Pitcairn Islands', 'NZD'),
(182, 'PR', 'Puerto Rico', 'USD'),
(183, 'PS', 'Palestine', 'ILS'),
(184, 'PT', 'Portugal', 'EUR'),
(185, 'PW', 'Palau', 'USD'),
(186, 'PY', 'Paraguay', 'PYG'),
(187, 'QA', 'Qatar', 'QAR'),
(188, 'RE', 'Reunion', 'EUR'),
(189, 'RO', 'Romania', 'RON'),
(190, 'RS', 'Serbia', 'RSD'),
(191, 'RU', 'Russia', 'RUB'),
(192, 'RW', 'Rwanda', 'RWF'),
(193, 'SA', 'Saudi Arabia', 'SAR'),
(194, 'SB', 'Solomon Islands', 'SBD'),
(195, 'SC', 'Seychelles', 'SCR'),
(196, 'SD', 'Sudan', 'SDG'),
(197, 'SE', 'Sweden', 'SEK'),
(198, 'SG', 'Singapore', 'SGD'),
(199, 'SH', 'Saint Helena', 'SHP'),
(200, 'SI', 'Slovenia', 'EUR'),
(201, 'SJ', 'Svalbard and Jan Mayen', 'NOK'),
(202, 'SK', 'Slovakia', 'EUR'),
(203, 'SL', 'Sierra Leone', 'SLL'),
(204, 'SM', 'San Marino', 'EUR'),
(205, 'SN', 'Senegal', 'XOF'),
(206, 'SO', 'Somalia', 'SOS'),
(207, 'SR', 'Suriname', 'SRD'),
(208, 'SS', 'South Sudan', 'SSP'),
(209, 'ST', 'Sao Tome and Principe', 'STD'),
(210, 'SV', 'El Salvador', 'USD'),
(211, 'SX', 'Sint Maarten', 'ANG'),
(212, 'SY', 'Syria', 'SYP'),
(213, 'SZ', 'Swaziland', 'SZL'),
(214, 'TC', 'Turks and Caicos Islands', 'USD'),
(215, 'TD', 'Chad', 'XAF'),
(216, 'TF', 'French Southern Territories', 'EUR'),
(217, 'TG', 'Togo', 'XOF'),
(218, 'TH', 'Thailand', 'THB'),
(219, 'TJ', 'Tajikistan', 'TJS'),
(220, 'TK', 'Tokelau', 'NZD'),
(221, 'TL', 'East Timor', 'USD'),
(222, 'TM', 'Turkmenistan', 'TMT'),
(223, 'TN', 'Tunisia', 'TND'),
(224, 'TO', 'Tonga', 'TOP'),
(225, 'TR', 'Turkey', 'TRY'),
(226, 'TT', 'Trinidad and Tobago', 'TTD'),
(227, 'TV', 'Tuvalu', 'AUD'),
(228, 'TW', 'Taiwan', 'TWD'),
(229, 'TZ', 'Tanzania', 'TZS'),
(230, 'UA', 'Ukraine', 'UAH'),
(231, 'UG', 'Uganda', 'UGX'),
(232, 'UM', 'U.S. Minor Outlying Islands', 'USD'),
(233, 'US', 'United States', 'USD'),
(234, 'UY', 'Uruguay', 'UYU'),
(235, 'UZ', 'Uzbekistan', 'UZS'),
(236, 'VA', 'Vatican City', 'EUR'),
(237, 'VC', 'Saint Vincent and the Grenadines', 'XCD'),
(238, 'VE', 'Venezuela', 'VEF'),
(239, 'VG', 'British Virgin Islands', 'USD'),
(240, 'VI', 'U.S. Virgin Islands', 'USD'),
(241, 'VN', 'Vietnam', 'VND'),
(242, 'VU', 'Vanuatu', 'VUV'),
(243, 'WF', 'Wallis and Futuna', 'XPF'),
(244, 'WS', 'Samoa', 'WST'),
(245, 'XK', 'Kosovo', 'EUR'),
(246, 'YE', 'Yemen', 'YER'),
(247, 'YT', 'Mayotte', 'EUR'),
(248, 'ZA', 'South Africa', 'ZAR'),
(249, 'ZM', 'Zambia', 'ZMW'),
(250, 'ZW', 'Zimbabwe', 'ZWL');

-- --------------------------------------------------------

--
-- Table structure for table `degrees`
--

CREATE TABLE `degrees` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `degrees`
--

INSERT INTO `degrees` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'School', '2016-11-29 15:10:06', '2016-11-29 15:10:26'),
(2, 'Graduate', '2016-11-29 15:10:34', '2016-11-29 15:10:34'),
(4, 'Post Graduate', '2016-11-29 15:11:34', '2016-11-29 15:11:34'),
(5, 'Bachelor', '2016-11-29 22:31:02', '2016-11-29 22:31:02'),
(6, 'Master', '2016-11-29 22:31:26', '2016-11-29 22:31:26'),
(7, 'Ph.D', '2016-11-29 22:31:37', '2016-11-29 22:31:37'),
(8, 'Professor', '2016-11-29 22:31:52', '2016-12-20 22:23:29'),
(9, 'High School', '2016-11-30 10:59:33', '2016-11-30 10:59:33'),
(11, 'Trung học phổ thông', '2017-02-07 21:14:47', '2017-02-07 21:14:47'),
(12, 'Thạc sĩ', '2017-02-07 21:13:16', '2017-02-07 21:13:16'),
(13, 'Tiến sĩ', '2017-02-07 21:13:22', '2017-02-07 21:13:22'),
(14, 'Cử nhân', '2017-02-07 21:13:10', '2017-02-07 21:13:10');

-- --------------------------------------------------------

--
-- Table structure for table `educations`
--

CREATE TABLE `educations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `institution` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `major` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `degree` int(11) NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` int(10) UNSIGNED NOT NULL,
  `from_month` int(10) UNSIGNED NOT NULL,
  `from_year` int(10) UNSIGNED NOT NULL,
  `to_month` int(10) UNSIGNED NOT NULL,
  `to_year` int(10) UNSIGNED NOT NULL,
  `status` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Active, N = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `educations`
--

INSERT INTO `educations` (`id`, `user_id`, `institution`, `major`, `degree`, `city`, `country`, `from_month`, `from_year`, `to_month`, `to_year`, `status`, `created_at`, `updated_at`) VALUES
(14, 102, 'Ultica College', 'Computer Science', 5, 'Utica, Tiểu bang New York, Hoa Kỳ', 0, 0, 2010, 0, 2014, 'Y', '2017-01-26 09:13:56', '2017-01-26 09:13:56'),
(15, 102, 'Trường Đại học Bách khoa Hà Nội', 'Khoa học máy tính', 5, 'Hà Nội, Việt Nam', 0, 0, 2008, 0, 2009, 'Y', '2017-01-26 09:15:10', '2017-01-26 09:15:10'),
(16, 103, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Việt Nam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-26 09:29:14', '2017-01-26 09:30:15'),
(17, 104, 'Trường Đại học Kinh tế Quốc dân', 'Kế toán', 14, 'Hà Nội, Việt Nam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-26 09:40:23', '2017-01-26 09:40:23'),
(18, 105, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 0, 'Hà Nội, Việt Nam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-26 09:48:49', '2017-01-26 09:48:49'),
(19, 105, 'THPT Chuyên Trần Phú (Hải Phòng)', 'Tự nhiên ', 0, 'Hải Phòng, Việt Nam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-26 09:51:04', '2017-01-26 09:51:04'),
(20, 106, 'Middlebury Institute of International Studies at Monterey', 'Translation and Interpretation', 5, 'Monterey, California, Hoa Kỳ', 0, 0, 2009, 0, 2011, 'Y', '2017-01-26 10:00:36', '2017-01-26 10:00:36'),
(21, 107, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Việt Nam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-27 07:58:03', '2017-01-27 07:58:03'),
(22, 107, 'Universitat Pompeu Fabra', 'Business with Marketing Management ', 6, 'Barcelona, Tây Ban Nha', 0, 0, 2015, 0, 2016, 'Y', '2017-01-27 07:59:50', '2017-01-27 07:59:50'),
(23, 107, 'THPT Thăng Long (Hà Nội)', 'N/A', 0, 'Hà Nội, Việt Nam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-27 08:00:32', '2017-01-27 08:00:32'),
(24, 108, 'Nanyang Technological University', 'Business, Actuarial Science', 0, 'Singapore', 0, 0, 2009, 0, 2012, 'Y', '2017-01-27 08:39:50', '2017-01-27 08:39:50'),
(25, 108, 'Trường Đại học Giáo dục, Đại học Quốc gia Hà Nội', 'Chuyên Hóa', 0, 'Hà Nội, Việt Nam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-27 08:41:08', '2017-01-27 08:41:08'),
(26, 109, 'Aalto-yliopisto', 'International Business', 5, 'Helsinki, Phần Lan', 0, 0, 2010, 0, 2013, 'Y', '2017-01-27 08:50:36', '2017-01-27 08:50:36'),
(27, 109, 'University of Florida', 'International Business', 6, 'Gainesville, Florida, Hoa Kỳ', 0, 0, 2012, 0, 2012, 'Y', '2017-01-27 08:51:39', '2017-01-27 08:51:39'),
(28, 110, 'Yokohama National University', 'Business Administration', 5, 'Yokohama, Kanagawa, Nhật Bản', 0, 0, 2011, 0, 2015, 'Y', '2017-01-27 09:07:31', '2017-01-27 09:07:31'),
(29, 111, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Việt Nam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-27 20:12:44', '2017-01-27 20:12:44'),
(30, 111, 'St. John\'s University Tobin College of Business', 'Business', 6, 'New York, Hoa Kỳ', 0, 0, 2016, 0, 2017, 'Y', '2017-01-27 20:14:57', '2017-01-27 20:14:57'),
(31, 112, 'Trường Đại học Kinh tế Quốc dân', 'Marketing', 14, 'Hà Nội, Việt Nam', 0, 0, 2005, 0, 2009, 'Y', '2017-01-27 20:28:20', '2017-01-27 20:28:20'),
(32, 112, 'THPT  Hạ Long (Quảng Ninh)', 'N/A', 0, 'Hạ Long, Quảng Ninh, Việt Nam', 0, 0, 2002, 0, 2005, 'Y', '2017-01-27 20:29:07', '2017-01-27 20:29:07'),
(33, 113, 'ESGCI Paris', 'Marketing & Commerce', 6, 'Paris, Pháp', 0, 0, 2011, 0, 2012, 'Y', '2017-01-27 20:42:14', '2017-01-27 20:42:32'),
(34, 113, 'Université Paris 2 Panthéon-Assas', 'AGE ', 0, 'Paris, Pháp', 0, 0, 2009, 0, 2011, 'Y', '2017-01-27 20:43:44', '2017-01-27 20:43:44'),
(35, 113, 'Université Paris 2 Panthéon-Assas', ' AES/Management', 5, 'Paris, Pháp', 0, 0, 2008, 0, 2010, 'Y', '2017-01-27 20:45:00', '2017-01-27 20:45:00'),
(36, 114, 'University of Portsmouth', 'Unknown', 5, 'Portsmouth, United Kingdom', 0, 0, 2010, 0, 2014, 'Y', '2017-01-27 21:05:32', '2017-01-27 21:05:32'),
(37, 115, 'THPT Marie-Curie (Tp. Hồ Chí Minh)', 'N/A', 0, 'Ho Chi Minh, Vietnam', 0, 0, 2002, 0, 2005, 'Y', '2017-01-27 22:04:41', '2017-01-27 22:04:41'),
(38, 116, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-27 22:15:04', '2017-01-27 22:15:04'),
(39, 117, 'Università degli Studi di Trento', 'Game Theory', 6, 'Trento, Italy', 0, 0, 2013, 0, 2017, 'Y', '2017-01-27 22:19:12', '2017-01-27 22:19:12'),
(40, 117, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-27 22:19:43', '2017-01-27 22:19:43'),
(41, 117, 'THPT Tiên Lãng (Hải Phòng)', 'N/A', 0, 'Tiên Lãng, Haiphong, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-27 22:20:54', '2017-01-27 22:20:54'),
(42, 118, 'Hanken School of Economics ', 'Quantitative Finance ', 6, 'Helsinki, Finland', 0, 0, 2014, 0, 2016, 'Y', '2017-01-27 22:29:02', '2017-01-27 22:29:02'),
(43, 118, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-27 22:29:34', '2017-01-27 22:29:34'),
(44, 118, 'THPT Chuyên Biên Hòa (Hà Nam)', 'N/A', 0, 'Hà Nam Province, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-27 22:30:54', '2017-01-27 22:30:54'),
(45, 119, 'THPT Chuyên Trần Phú (Hải Phòng)', 'Chuyên Lý', 0, 'Hải Phòng, Vietnam', 0, 0, 2002, 0, 2005, 'Y', '2017-01-28 02:59:26', '2017-01-28 02:59:26'),
(46, 120, 'Middlesex University', ' International Business Management', 6, 'London, United Kingdom', 0, 0, 2013, 0, 2014, 'Y', '2017-01-28 03:09:19', '2017-01-28 03:09:19'),
(47, 120, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-28 03:09:53', '2017-01-28 03:09:53'),
(48, 121, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-28 03:24:30', '2017-01-28 03:24:30'),
(49, 121, 'THPT Phan Đình Phùng', 'N/A', 0, 'Hà Nội, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-28 03:27:19', '2017-01-28 03:27:19'),
(50, 121, 'Massey University', 'MBS', 6, 'Palmerston North, Manawatu-Wanganui, New Zealand', 0, 0, 2012, 0, 2014, 'Y', '2017-01-28 03:29:17', '2017-01-28 03:29:17'),
(51, 122, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-28 03:36:26', '2017-01-28 03:36:26'),
(52, 122, 'THPT Chuyên Trần Phú (Hải Phòng)', 'Chuyên Lý', 0, 'Hải Phòng, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-28 03:37:15', '2017-01-28 03:37:15'),
(53, 123, 'The University of Glasgow', 'Financial Forecasting and Investment', 6, 'Glasgow, United Kingdom', 0, 0, 2012, 0, 2013, 'Y', '2017-01-28 06:47:11', '2017-01-28 06:47:11'),
(54, 123, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-28 06:47:56', '2017-01-28 06:47:56'),
(55, 124, 'University of Connecticut', 'Financial Risk Management ', 6, 'Storrs, Mansfield, CT, United States', 0, 0, 2012, 0, 2013, 'Y', '2017-01-28 07:09:52', '2017-01-28 07:09:52'),
(56, 124, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 0, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-28 07:10:30', '2017-01-28 07:10:30'),
(57, 125, 'Hult International Business School', 'International Business', 6, 'London, United Kingdom', 0, 0, 2012, 0, 2013, 'Y', '2017-01-28 08:17:39', '2017-01-28 08:17:39'),
(58, 125, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-28 08:18:05', '2017-01-28 08:18:05'),
(59, 125, 'THPT Chuyên Trần Phú (Hải Phòng)', 'Chuyên Hóa', 0, 'Hải Phòng, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-28 08:19:15', '2017-01-28 08:19:15'),
(60, 126, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 0, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-28 08:35:32', '2017-01-28 08:35:32'),
(61, 126, 'Đại học Quốc gia Hà Nội', 'N/A', 11, 'Hà Nội, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-28 08:37:04', '2017-01-28 08:37:04'),
(62, 127, 'The Australian National University', 'Economics', 5, 'Canberra, Australian Capital Territory, Australia', 0, 0, 2009, 0, 2013, 'Y', '2017-01-28 08:44:06', '2017-01-28 08:44:06'),
(63, 127, 'THPT Chuyên Trần Phú (Hải Phòng)', 'Chuyên Toán', 0, 'Hải Phòng, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-28 08:44:52', '2017-01-28 08:44:52'),
(64, 128, 'National University of Singapore', 'Real estate', 5, 'Singapore', 0, 0, 2009, 0, 2013, 'Y', '2017-01-28 08:52:31', '2017-01-28 08:52:31'),
(65, 128, 'Trường Đại học Khoa học Tự nhiên, Đại học Quốc gia Hà Nội', 'N/A', 0, 'Hà Nội, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-28 08:53:46', '2017-01-28 08:53:46'),
(66, 129, 'IAE Toulouse', 'Unknown', 6, 'Toulouse, France', 0, 0, 2012, 0, 2014, 'Y', '2017-01-28 09:01:44', '2017-01-28 09:01:44'),
(67, 129, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-28 09:02:10', '2017-01-28 09:02:10'),
(68, 129, 'THPT Lê Quý Đôn-Đống Đa (Hà Nội)', 'N/A', 0, 'Hà Nội, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-28 09:02:47', '2017-01-28 09:02:47'),
(69, 130, 'Paris Dauphine University', 'Business Administration', 6, 'Paris, France', 0, 0, 2009, 0, 2010, 'Y', '2017-01-28 09:10:56', '2017-01-28 09:10:56'),
(70, 130, 'IUT Fontainebleau', 'Business Administration', 5, 'Fontainebleau, France', 0, 0, 2007, 0, 2009, 'Y', '2017-01-28 09:12:01', '2017-01-28 09:12:01'),
(71, 130, 'THPT Chuyên Hà Nội-Amsterdam (Hà Nội)', 'Chuyên Pháp', 0, 'Hà Nội, Vietnam', 0, 0, 2004, 0, 2007, 'Y', '2017-01-28 09:12:38', '2017-01-28 09:12:38'),
(72, 131, 'University at Buffalo, the State University of New York', 'Business Admistration', 5, 'Buffalo, NY, United States', 0, 0, 2007, 0, 2011, 'Y', '2017-01-28 09:20:36', '2017-01-28 09:22:43'),
(73, 131, 'University of Seattle', 'Business Administration', 6, 'Seattle, WA, United States', 0, 0, 2011, 0, 2012, 'Y', '2017-01-28 09:22:22', '2017-01-28 09:22:22'),
(74, 131, 'THPT Chu Văn An (Hà Nội)', 'N/A', 0, 'Hà Nội, Vietnam', 0, 0, 2004, 0, 2007, 'Y', '2017-01-28 09:23:09', '2017-01-28 09:23:09'),
(75, 132, 'University of South Australia', 'Accounting', 6, 'Adelaide, South Australia, Australia', 0, 0, 2002, 0, 2006, 'Y', '2017-01-28 09:35:32', '2017-01-28 09:35:32'),
(76, 133, 'UNSW Sydney', 'Accounting', 6, 'Sydney, New South Wales, Australia', 0, 0, 2013, 0, 2015, 'Y', '2017-01-28 20:05:45', '2017-01-28 20:05:45'),
(77, 133, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-28 20:06:17', '2017-01-28 20:06:17'),
(78, 133, 'THPT Thăng Long (Hà Nội)', 'N/A', 0, 'Hà Nội, Vietnam', 0, 0, 2006, 0, 2008, 'Y', '2017-01-28 20:06:51', '2017-01-28 20:06:51'),
(79, 134, 'Université de Picardie Jules Verne (Amiens)', 'Management Entrepreneuriat et Gestion des Connaissances - Administration des Entreprises', 6, 'Paris, France', 0, 0, 2015, 0, 2016, 'Y', '2017-01-28 20:17:07', '2017-01-28 20:17:07'),
(80, 134, 'Università degli Studi della Calabria', 'Economia Aziendale (MiM)', 6, 'Paris, France', 0, 0, 2014, 0, 2016, 'Y', '2017-01-28 20:18:13', '2017-01-28 20:18:13'),
(81, 134, 'Trường Đại học Hà Nội', 'Tài chính - Ngân hàng', 14, 'Hà Nội, Vietnam', 0, 0, 2010, 0, 2014, 'Y', '2017-01-28 20:18:46', '2017-01-28 20:18:46'),
(82, 135, 'University of Leicester', 'Finance & Banking', 6, 'Leicester, United Kingdom', 0, 0, 2009, 0, 2010, 'Y', '2017-01-28 20:27:58', '2017-01-28 20:27:58'),
(83, 135, 'Trường Đại học Kinh tế Quốc dân', 'Kinh tế học', 14, 'Hà Nội, Vietnam', 0, 0, 2004, 0, 2008, 'Y', '2017-01-28 20:28:36', '2017-01-28 20:28:36'),
(84, 136, 'University of Leicester', 'Financial Economics', 6, 'Leicester, United Kingdom', 0, 0, 2012, 0, 2013, 'Y', '2017-01-28 20:38:34', '2017-01-28 20:38:34'),
(85, 136, 'Học viện Ngân hàng', 'Tài chính - Ngân hàng', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-28 20:39:13', '2017-01-28 20:39:13'),
(86, 136, 'THPT Lương Thế Vinh (Hà Nội)', 'N/A', 0, 'Hà Nội, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-28 20:39:46', '2017-01-28 20:39:46'),
(87, 137, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-28 21:06:55', '2017-01-28 21:06:55'),
(88, 137, 'THPT Chuyên Trần Phú (Hải Phòng)', 'Tự nhiên', 0, 'Hải Phòng, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-28 21:07:27', '2017-01-28 21:07:27'),
(89, 138, 'Corvinus University of Budapest', 'Business Administration and Management', 5, 'Budapest, Hungary', 0, 0, 2010, 0, 2013, 'Y', '2017-01-28 21:42:01', '2017-01-28 21:42:01'),
(90, 138, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 5, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2010, 'Y', '2017-01-28 21:42:43', '2017-01-28 21:42:43'),
(91, 139, 'Trường Đại học Ngoại thương', 'Kinh tế quốc tế', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-28 21:51:51', '2017-01-28 21:51:51'),
(92, 139, 'THPT Chuyên Trần Phú (Hải Phòng)', 'Chuyên Lý', 1, 'Hải Phòng, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-28 21:52:18', '2017-01-28 21:52:18'),
(93, 140, 'Trường Đại học Kinh tế, Đại học Quốc gia Hà Nội', 'Quản trị kinh doanh', 12, 'Hà Nội, Vietnam', 0, 0, 2011, 0, 2014, 'Y', '2017-01-28 21:58:49', '2017-01-28 21:58:49'),
(94, 140, 'Trường Đại học Giao thông Vận tải', 'Công nghệ kỹ thuật điện, điện tử và viễn thông', 14, 'Hà Nội, Vietnam', 0, 0, 2004, 0, 2008, 'Y', '2017-01-28 22:00:04', '2017-01-28 22:00:04'),
(95, 141, 'Trường Đại học Kinh tế Quốc dân', 'Kính tế đầu tư (Chất lượng cao)', 14, 'Hà Nội, Vietnam', 0, 0, 2011, 0, 2015, 'Y', '2017-01-29 02:30:31', '2017-01-29 02:30:31'),
(96, 142, 'Trường Đại học Kinh tế Quốc dân', 'Kinh tế đầu tư', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-29 03:47:05', '2017-01-29 03:47:05'),
(97, 143, 'The University of Auckland', 'Management', 6, 'Auckland, New Zealand', 0, 0, 2014, 0, 2015, 'Y', '2017-01-29 04:05:09', '2017-01-29 04:05:09'),
(98, 143, 'Trường Đại học Kinh tế Quốc dân', 'Kinh tế đầu tư', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-29 04:05:56', '2017-01-29 04:05:56'),
(99, 144, 'Trường Đại học Khoa học Tự nhiên, Đại học Quốc gia Hà Nội', 'Điện tử và Viễn thông', 14, 'Ho Chi Minh, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-29 04:13:49', '2017-01-29 04:13:49'),
(100, 144, 'THPT Chuyên Trần Phú (Hải Phòng)', 'Chuyên Lý', 0, 'Hải Phòng, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-29 04:14:23', '2017-01-29 04:14:23'),
(101, 146, 'Học viện Ngân hàng', 'Tài chính - Ngân hàng', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-29 06:32:28', '2017-01-29 06:32:28'),
(102, 146, 'Trường THPT Hùng Vương (Phú Thọ)', 'N/A', 0, 'Phu Tho Province, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-29 06:33:03', '2017-01-29 06:33:03'),
(103, 147, 'Trường ĐH Ngoại thương', 'Tài chính quốc tế', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-29 06:39:48', '2017-01-29 06:39:48'),
(104, 147, 'THPT Hồng Quang (Hải Dương)', 'N/A', 0, 'Hai Duong, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-29 06:40:27', '2017-01-29 06:40:27'),
(105, 148, 'Universiteit van Amsterdam', 'Education', 6, 'Amsterdam, Netherlands', 0, 0, 2015, 0, 2016, 'Y', '2017-01-29 06:49:55', '2017-01-29 07:09:59'),
(106, 148, 'Vrije Universiteit Amsterdam', 'Education', 5, 'Amsterdam, Netherlands', 0, 0, 2011, 0, 2015, 'Y', '2017-01-29 07:11:06', '2017-01-29 07:11:06'),
(107, 149, 'Trường Đại học Ngoại thương', 'Kinh tế đối ngoại', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-29 07:22:22', '2017-01-29 07:22:22'),
(108, 149, 'THPT Chuyên Trần Phú (Hải Phòng)', 'Tự nhiên', 0, 'Hải Phòng, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-29 07:22:46', '2017-01-29 07:22:46'),
(109, 150, 'Trường Đại học Ngoại ngữ, Đại học Quốc gia Hà Nội', 'Biên phiên dịch', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-29 07:29:27', '2017-01-29 07:29:27'),
(110, 150, 'THPT Thái Phiên (Hải Phòng)', 'N/A', 0, 'Hải Phòng, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-29 07:30:02', '2017-01-29 07:30:02'),
(111, 151, 'Trường Đại học Ngoại thương', 'Tài chính - Ngân hàng', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-29 07:41:41', '2017-01-29 07:41:41'),
(112, 151, 'THPT Chuyên (Thái Bình)', 'Chuyên Anh', 0, 'Thai Binh, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-29 07:42:25', '2017-01-29 07:43:14'),
(113, 152, 'Plymouth University', 'Nutrition and Exercise Physiology ', 5, 'Plymouth, United Kingdom', 0, 0, 2012, 0, 2016, 'Y', '2017-01-29 07:52:03', '2017-01-29 07:52:03'),
(114, 154, 'Trường Đại học Kinh tế Quốc dân', 'Marketing', 14, 'Hà Nội, Vietnam', 0, 0, 2005, 0, 2009, 'Y', '2017-01-29 08:48:18', '2017-01-29 08:48:18'),
(115, 154, 'THPT Chuyên Hạ Long (Quảng Ninh)', 'n/a', 0, 'Hạ Long, Quảng Ninh Province, Vietnam', 0, 0, 2002, 0, 2005, 'Y', '2017-01-29 08:49:09', '2017-01-29 08:49:09'),
(116, 156, 'University of Missouri - Columbia', 'Finance & Banking', 5, 'Missouri, United States', 0, 0, 2007, 0, 2011, 'Y', '2017-01-29 09:07:56', '2017-01-29 09:07:56'),
(117, 156, 'THPT Kim Liên (Hà Nội)', 'n/a', 0, 'Hà Nội, Vietnam', 0, 0, 2004, 0, 2007, 'Y', '2017-01-29 09:08:24', '2017-01-29 09:08:24'),
(118, 157, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-29 09:15:08', '2017-01-29 09:15:08'),
(119, 157, 'THPT Chuyên Trần Phú (Hải Phòng)', 'Chuyên Lý', 0, 'Hải Phòng, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-29 09:15:36', '2017-01-29 09:15:36'),
(120, 158, 'University of Birmingham', 'International Money and Banking ', 6, 'Birmingham, United Kingdom', 0, 0, 2012, 0, 2014, 'Y', '2017-01-29 19:37:41', '2017-01-29 19:37:41'),
(121, 158, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-29 19:38:11', '2017-01-29 19:38:11'),
(122, 158, 'THPT Kim Liên (Hà Nội)', 'n/a', 0, 'Hà Nội, Vietnam', 0, 0, 2005, 0, 2008, 'Y', '2017-01-29 19:38:43', '2017-01-29 19:38:43'),
(123, 159, 'Czech Technical University ', 'Microelectronics', 5, 'Prague, Czechia', 0, 0, 1990, 0, 1996, 'Y', '2017-01-29 20:03:54', '2017-01-29 20:03:54'),
(124, 159, 'GRIGGS University/Vietnam National University', 'MBA', 5, 'Hà Nội, Vietnam', 0, 0, 2010, 0, 2012, 'Y', '2017-01-29 20:04:44', '2017-01-29 20:04:44'),
(125, 159, 'University of Economics', 'Data Mining', 7, 'Prague, Czechia', 0, 0, 1999, 0, 2000, 'Y', '2017-01-29 20:05:46', '2017-01-29 20:05:46'),
(126, 160, 'The Australian National University', 'International and Development Economics', 6, 'Canberra, Australian Capital Territory, Australia', 0, 0, 2014, 0, 2015, 'Y', '2017-01-29 20:17:24', '2017-01-29 20:17:24'),
(127, 160, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính doanh nghiệp', 14, 'Hà Nội, Vietnam', 0, 0, 2006, 0, 2010, 'Y', '2017-01-29 20:18:03', '2017-01-29 20:18:03'),
(128, 160, 'THPT Chu Văn An (Hà Nội)', 'n/a', 0, 'Hà Nội, Vietnam', 0, 0, 2003, 0, 2006, 'Y', '2017-01-29 20:19:05', '2017-01-29 20:19:05'),
(129, 161, 'University of Maryland - Robert H. Smith School of Business', 'Finance', 6, 'Maryland, United States', 0, 0, 2014, 0, 2014, 'Y', '2017-01-29 20:31:17', '2017-01-29 20:31:17'),
(130, 161, 'University of Baltimore', 'Finance', 6, 'Baltimore, MD, United States', 0, 0, 2012, 0, 2014, 'Y', '2017-01-29 20:32:13', '2017-01-29 20:32:13'),
(131, 162, 'University of Nottingham', 'Banking & Finance', 6, 'Nottingham, United Kingdom', 0, 0, 2013, 0, 2014, 'Y', '2017-01-29 20:40:30', '2017-01-29 20:40:30'),
(132, 162, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính doanh nghiệp', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-29 20:41:11', '2017-01-29 20:41:11'),
(133, 163, 'Trường Đại học Kinh tế Quốc dân', 'Tài chính (Tiên tiến)', 14, 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2012, 'Y', '2017-01-29 20:49:17', '2017-01-29 20:49:17');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `email_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `email_title`, `description`, `locale`, `created_at`, `updated_at`) VALUES
(1, 'Activate accounts', '<header style=" margin: 0px auto; width: 600px;">{SITE_URL}</header>\r\n\r\n<section style=" margin: 0px auto; width: 600px; border: 1px solid #EBEBEB; padding: 10px;">\r\n\r\n    <div style="font-family:Verdana,Arial;font-weight:normal;font-size:16px; font-weight: bold;line-height:18px;margin-bottom:10px;margin-top:20px">Hi {USER_NAME},</div>\r\n\r\n    <div style="font-family:Verdana,Arial;font-weight:normal;font-size:12px;line-height:18px;margin-bottom:10px;margin-top:10px">Thanks for creating an account on {SITE_URL}.\r\n\r\n        Please follow the link below to verify your email address: </div>\r\n\r\n\r\n\r\n    <div style="font-family:Verdana,Arial;font-weight:normal;text-align:center;font-size:16px;line-height:18px;margin-bottom:30px;margin-top:30px"><a href="{ACTIVATION_LINK}" style="text-decoration: none; padding: 10px 40px; background: #3696C2; color: #fff;">ACTIVATE ACCOUNT</a></div>\r\n\r\n\r\n\r\n    <div style="font-family:Verdana,Arial;font-weight:normal;font-size:12px;margin-bottom:10px;margin-top:10px">If you have any questions, please feel free to contact us at {CONTACT_MAIL}.</div>\r\n\r\n</section>\r\n\r\n\r\n<footer style="font-family:Verdana,Arial;font-weight:normal;text-align:center;font-size:12px;line-height:18px;margin-bottom:75px;margin-top:30px">Thank you, {SITE_URL}</footer>', 'en', '2016-09-30 18:30:00', '2016-09-30 18:30:00'),
(3, 'New Invoice', 'Congrats.. A new invoice is created.  ', 'en', '2016-10-12 18:30:00', '2016-10-12 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `favorite_tropics`
--

CREATE TABLE `favorite_tropics` (
  `id` int(10) UNSIGNED NOT NULL,
  `tropic_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `filter_order_sets`
--

CREATE TABLE `filter_order_sets` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `filter_set` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `weight` int(10) UNSIGNED NOT NULL,
  `fallback_locale` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Deafult language',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `locale`, `weight`, `fallback_locale`, `created_at`, `updated_at`) VALUES
(1, 'English', 'en', 1, 'Y', '2016-07-12 07:51:59', '2016-07-12 07:51:59'),
(2, 'Polish ', 'pl', 2, 'N', '2016-09-28 18:30:00', '2016-09-28 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_07_06_075604_create_admins_table', 1),
('2016_07_06_112919_create_settings_table', 1),
('2016_07_12_092132_create_languages_table', 1),
('2016_09_16_092105_create_pages_table', 2),
('2016_09_16_093651_create_pages_table', 3),
('2016_09_16_093953_create_page_translations_table', 4),
('2016_09_16_110017_create_users_table', 5),
('2016_09_22_075004_create_groups_table', 6),
('2016_09_22_095444_create_group_members_table', 7),
('2016_09_22_130046_create_groups_table', 8),
('2016_09_22_130253_create_group_members_table', 9),
('2016_09_23_132931_create_group_user_table', 10),
('2016_09_23_134032_create_group_user_table', 11),
('2016_09_24_133840_create_categorys_table', 12),
('2016_09_26_071915_create_packages_table', 13),
('2016_09_26_105120_create_user_package_table', 14),
('2016_09_28_071834_add_column_to_settings', 15),
('2016_09_28_123624_add_column_to_package_user', 16),
('2016_10_01_081042_add_column_to_users', 17),
('2016_10_01_095334_create_email_templates_table', 18),
('2016_10_03_123350_add_column_to_setting', 19),
('2016_10_07_102140_create_invoices_table', 20),
('2016_10_18_140838_create_questions_table', 21),
('2016_10_19_141649_create_answers_table', 22),
('2016_10_20_061252_create_tests_tabel', 23),
('2016_10_20_102743_create_test_question_tabel', 24),
('2016_10_21_133133_add_column_to_questions', 25),
('2016_10_25_131934_create_sub_questions_table', 26),
('2016_10_25_135636_create_sub_amswers_table', 26),
('2016_10_26_075209_add_column_to_sub_answers', 27),
('2016_10_26_114058_add_column_to_tests', 27),
('2016_10_27_075703_add_column_to_question', 28),
('2016_10_27_080117_add_column_to_answers', 29),
('2016_10_27_111850_create_test_group_table', 30),
('2016_10_28_074023_create_test_evaluations_table', 31),
('2016_10_31_062736_create_test_answers_table', 32),
('2016_11_07_130827_add_column_to_test_evaluations', 33),
('2016_11_11_134633_create_work_experience_table', 34),
('2016_11_11_155027_create_educations_table', 35),
('2016_11_11_162456_create_tropic_table', 36);

-- --------------------------------------------------------

--
-- Table structure for table `notification_settings`
--

CREATE TABLE `notification_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `answer_question` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `upvote` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `new_follower` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `new_nessage` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `tag_me` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `comment_my` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `created_at`, `updated_at`) VALUES
(2, '2016-09-29 03:48:02', '2016-09-29 03:48:02'),
(5, '2016-10-13 02:43:30', '2016-10-13 02:43:30'),
(6, '2016-10-13 03:48:30', '2016-10-13 03:48:30'),
(7, '2016-10-14 01:17:00', '2016-10-14 01:17:00'),
(8, '2016-10-14 01:19:47', '2016-10-14 01:19:47'),
(9, '2016-10-14 01:21:13', '2016-10-14 01:21:13'),
(10, '2016-10-14 01:23:29', '2016-10-14 01:23:29'),
(11, '2016-10-14 01:24:34', '2016-10-14 01:24:34');

-- --------------------------------------------------------

--
-- Table structure for table `page_translations`
--

CREATE TABLE `page_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_content` text COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `page_translations`
--

INSERT INTO `page_translations` (`id`, `page_id`, `slug`, `title`, `page_content`, `locale`, `seo_title`, `seo_keywords`, `seo_description`) VALUES
(3, 2, 'stories', 'Stories', 'EnLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\r\n\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\r\n\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'en', 'Stories', '\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(5, 6, 'about-us', 'About Us', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'en', 'ttt', 'ing, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the indu', 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the indu'),
(8, 6, 'about-us', 'About Pl', 'PLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'pl', 'ddd', 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the indu', ' Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ing, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the indu'),
(11, 2, 'stories', 'Stories', '\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'pl', 'ddd', '\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', ''),
(12, 11, 'contact-us', 'Contact Us', '', 'en', 'Contact Us', 'Contact Us', 'Contact Us'),
(13, 11, 'contact-us', 'Contact Us', '', 'pl', 'Contact Us', 'Contact Us', 'Contact Us');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `privacy_settings`
--

CREATE TABLE `privacy_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `online` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `other_view_account` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No',
  `want_receive_message` enum('S','A','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A' COMMENT 'Y = Specific People, A = Anyone, N = No',
  `decline_request` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Y = Yes, N = No',
  `max_request` int(10) UNSIGNED NOT NULL,
  `request_intelval` enum('D','M') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'D' COMMENT 'D = Per Day, M = Per Month',
  `allow_comments` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Y = Yes, N = No',
  `deactivate_account` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'Y = Yes, N = No',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `school_majors`
--

CREATE TABLE `school_majors` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school_majors`
--

INSERT INTO `school_majors` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Biên phòng ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(2, 'Phục hồi chức năng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(3, 'Công nghệ kỹ thuật cơ khí', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(4, 'Âm nhạc học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(5, 'An ninh và trật tự xã hội', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(6, 'An ninh, Quốc phòng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(7, 'Bản đồ học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(8, 'Báo chí', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(9, 'Báo chí và thông tin', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(10, 'Báo chí và truyền thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(11, 'Bảo dưỡng công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(12, 'Bảo hiểm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(13, 'Bảo hộ lao động', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(14, 'Bảo tàng học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(15, 'Bảo vệ thực vật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(16, 'Bất động sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(17, 'Bệnh học thủy sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(18, 'Biên đạo múa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(19, 'Biên kịch điện ảnh - truyền hình', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(20, 'Biên kịch sân khấu', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(21, 'Biểu diễn nhạc cụ phương tây', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(22, 'Biểu diễn nhạc cụ truyền thống', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(23, 'Chăn nuôi', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(24, 'Chế biến lương thực, thực phẩm và đồ uống', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(25, 'Chỉ huy âm nhạc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(26, 'Chỉ huy kỹ thuật ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(27, 'Chỉ huy kỹ thuật Công binh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(28, 'Chỉ huy kỹ thuật Hoá học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(29, 'Chỉ huy kỹ thuật Phòng không', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(30, 'Chỉ huy kỹ thuật Tác chiến điện tử', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(31, 'Chỉ huy kỹ thuật Tăng - thiết giáp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(32, 'Chỉ huy kỹ thuật Thông tin', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(33, 'Chỉ huy tham mưu Đặc công', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(34, 'Chỉ huy tham mưu Hải quân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(35, 'Chỉ huy tham mưu Không quân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(36, 'Chỉ huy tham mưu Lục quân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(37, 'Chỉ huy tham mưu Pháo binh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(38, 'Chỉ huy tham mưu Phòng không', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(39, 'Chỉ huy tham mưu Tăng - thiết giáp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(40, 'Chính trị học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(41, 'Cơ kỹ thuật ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(42, 'Công nghệ chế biến lâm sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(43, 'Công nghệ chế biến thuỷ sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(44, 'Công nghệ chế tạo máy', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(45, 'Công nghệ da giày', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(46, 'Công nghệ điện ảnh - truyền hình', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(47, 'Công nghệ giấy và bột giấy', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(48, 'Công nghệ hoá học, vật liệu, luyện kim và môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(49, 'Công nghệ in', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(50, 'Công nghệ kỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(51, 'Công nghệ kỹ thuật cơ điện tử', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(52, 'Công nghệ kỹ thuật công trình xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(53, 'Công nghệ kỹ thuật địa chất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(54, 'Công nghệ kỹ thuật địa chất, địa vật lý và trắc địa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(55, 'Công nghệ kỹ thuật điện tử, truyền thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(56, 'Công nghệ kỹ thuật điện, điện tử', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(57, 'Công nghệ kỹ thuật điện, điện tử và viễn thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(58, 'Công nghệ kỹ thuật điều khiển và tự động hóa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(59, 'Công nghệ kỹ thuật giao thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(60, 'Công nghệ kỹ thuật hạt nhân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(61, 'Công nghệ kỹ thuật hoá học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(62, 'Công nghệ kỹ thuật kiến trúc ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(63, 'Công nghệ kỹ thuật kiến trúc và công trình xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(64, 'Công nghệ kỹ thuật máy tính', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(65, 'Công nghệ kỹ thuật mỏ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(66, 'Công nghệ kỹ thuật môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(67, 'Công nghệ kỹ thuật nhiệt', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(68, 'Công nghệ kỹ thuật ô tô', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(69, 'Công nghệ kỹ thuật tài nguyên nước', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(70, 'Công nghệ kỹ thuật trắc địa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(71, 'Công nghệ kỹ thuật vật liệu xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(72, 'Công nghệ kỹ thuật xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(73, 'Công nghệ may', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(74, 'Công nghệ rau hoa quả và cảnh quan', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(75, 'Công nghệ sản xuất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(76, 'Công nghệ sau thu hoạch', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(77, 'Công nghệ sinh học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(78, 'Công nghệ sợi, dệt', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(79, 'Công nghệ thông tin', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(80, 'Công nghệ thực phẩm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(81, 'Công nghệ truyền thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(82, 'Công nghệ tuyển khoáng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(83, 'Công nghệ vật liệu', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(84, 'Công tác thanh thiếu niên', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(85, 'Công tác xã hội', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(86, 'Công thôn                       ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(87, 'Đạo diễn điện ảnh - truyền hình', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(88, 'Đạo diễn sân khấu', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(89, 'Đào tạo giáo viên', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(90, 'Địa chất học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(91, 'Địa kỹ thuật xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(92, 'Địa lý học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(93, 'Địa lý tự nhiên', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(94, 'Dịch vụ an toàn lao động và vệ sinh công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(95, 'Dịch vụ pháp lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(96, 'Dịch vụ thú y', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(97, 'Dịch vụ vận tải', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(98, 'Dịch vụ xã hội', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(99, 'Dịch vụ y tế', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(100, 'Diễn viên kịch - điện ảnh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(101, 'Diễn viên múa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(102, 'Diễn viên sân khấu kịch hát', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(103, 'Điều dưỡng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(104, 'Điều dưỡng, hộ sinh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(105, 'Điêu khắc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(106, 'Điều khiển tàu biển', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(107, 'Điều tra hình sự', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(108, 'Điều tra trinh sát', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(109, 'Đồ hoạ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(110, 'Đông Nam Á học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(111, 'Đông phương học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(112, 'Dược học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(113, 'Giáo dục Chính trị', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(114, 'Giáo dục Công dân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(115, 'Giáo dục Đặc biệt', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(116, 'Giáo dục học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(117, 'Giáo dục Mầm non', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(118, 'Giáo dục Quốc phòng - An ninh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(119, 'Giáo dục Thể chất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(120, 'Giáo dục Tiểu học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(121, 'Gốm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(122, 'Hải dương học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(123, 'Hán Nôm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(124, 'Hàn Quốc học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(125, 'Hậu cần công an nhân dân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(126, 'Hậu cần quân sự', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(127, 'Hệ thống thông tin', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(128, 'Hệ thống thông tin quản lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(129, 'Hộ sinh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(130, 'Hoá dược', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(131, 'Hoá học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(132, 'Hội hoạ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(133, 'Huấn luyện múa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(134, 'Huấn luyện thể thao*', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(135, 'Kế toán', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(136, 'Kế toán – Kiểm toán', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(137, 'Khách sạn, du lịch, thể thao và dịch vụ cá nhân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(138, 'Khách sạn, nhà hàng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(139, 'Khai thác vận tải', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(140, 'Khí tượng học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(141, 'Khoa học cây trồng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(142, 'Khoa học chính trị', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(143, 'Khoa học đất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(144, 'Khoa học giáo dục', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(145, 'Khoa học hàng hải', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(146, 'Khoa học máy tính', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(147, 'Khoa học môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(148, 'Khoa học quản lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(149, 'Khoa học sự sống', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(150, 'Khoa học thư viện', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(151, 'Khoa học trái đất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(152, 'Khoa học tự nhiên', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(153, 'Khoa học vật chất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(154, 'Khoa học vật liệu', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(155, 'Khoa học xã hội và hành vi', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(156, 'Khu vực Thái Bình Dương học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(157, 'Khuyến nông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(158, 'Kiểm soát và bảo vệ môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(159, 'Kiểm toán', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(160, 'Kiến trúc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(161, 'Kiến trúc cảnh quan', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(162, 'Kiến trúc và quy hoạch', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(163, 'Kiến trúc và xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(164, 'Kinh doanh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(165, 'Kinh doanh nông nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(166, 'Kinh doanh quốc tế ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(167, 'Kinh doanh thương mại', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(168, 'Kinh doanh và quản lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(169, 'Kinh doanh xuất bản phẩm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(170, 'Kinh tế', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(171, 'Kinh tế công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(172, 'Kinh tế gia đình', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(173, 'Kinh tế học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(174, 'Kinh tế nông nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(175, 'Kinh tế quốc tế', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(176, 'Kinh tế tài nguyên thiên nhiên', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(177, 'Kinh tế vận tải', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(178, 'Kinh tế xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(179, 'Kỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(180, 'Kỹ thuật biển', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(181, 'Kỹ thuật cơ - điện tử', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(182, 'Kỹ thuật cơ khí', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(183, 'Kỹ thuật cơ khí và cơ kỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(184, 'Kỹ thuật cơ sở hạ tầng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(185, 'Kỹ thuật công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(186, 'Kỹ thuật công trình biển', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(187, 'Kỹ thuật công trình thuỷ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(188, 'Kỹ thuật công trình xây dựng ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(189, 'Kỹ thuật dầu khí', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(190, 'Kỹ thuật dệt', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(191, 'Kỹ thuật địa chất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(192, 'Kỹ thuật địa chất, địa vật lý và trắc địa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(193, 'Kỹ thuật địa vật lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(194, 'Kỹ thuật điện tử, truyền thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(195, 'Kỹ thuật điện, điện tử', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(196, 'Kỹ thuật điện, điện tử và viễn thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(197, 'Kỹ thuật điều khiển và tự động hoá', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(198, 'Kỹ thuật hàng không', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(199, 'Kỹ thuật hạt nhân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(200, 'Kỹ thuật hệ thống công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(201, 'Kỹ thuật hình ảnh y học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(202, 'Kỹ thuật hình sự', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(203, 'Kỹ thuật hoá học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(204, 'Kỹ thuật hoá học, vật liệu, luyện kim và môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(205, 'Kỹ thuật khai thác thủy sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(206, 'Kỹ thuật máy tính', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(207, 'Kỹ thuật mỏ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(208, 'Kỹ thuật môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(209, 'Kỹ thuật nhiệt', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(210, 'Kỹ thuật phần mềm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(211, 'Kỹ thuật phục hình răng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(212, 'Kỹ thuật sinh học*', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(213, 'Kỹ thuật tài nguyên nước', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(214, 'Kỹ thuật tàu thuỷ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(215, 'Kỹ thuật trắc địa - bản đồ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(216, 'Kỹ thuật tuyển khoáng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(217, 'Kỹ thuật vật liệu', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(218, 'Kỹ thuật vật liệu kim loại', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(219, 'Kỹ thuật xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(220, 'Kỹ thuật xây dựng công trình giao thông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(221, 'Kỹ thuật y sinh*', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(222, 'Lâm nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(223, 'Lâm nghiệp đô thị', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(224, 'Lâm sinh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(225, 'Lịch sử', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(226, 'Luật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(227, 'Luật kinh tế', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(228, 'Luật quốc tế', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(229, 'Lưu trữ học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(230, 'Lý luận và phê bình điện ảnh - truyền hình', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(231, 'Lý luận và phê bình sân khấu', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(232, 'Lý luận, lịch sử và phê bình mỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(233, 'Lý luận, phê bình múa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(234, 'Marketing', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(235, 'Máy tính', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(236, 'Máy tính và công nghệ thông tin', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(237, 'Môi trường và bảo vệ môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(238, 'Mỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(239, 'Mỹ thuật ứng dụng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(240, 'Nghệ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(241, 'Nghệ thuật nghe nhìn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(242, 'Nghệ thuật trình diễn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(243, 'Ngôn ngữ Anh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(244, 'Ngôn ngữ Ảrập', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(245, 'Ngôn ngữ Bồ Đào Nha', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(246, 'Ngôn ngữ Chăm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(247, 'Ngôn ngữ Đức', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(248, 'Ngôn ngữ Hàn Quốc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(249, 'Ngôn ngữ H\'mong', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(250, 'Ngôn ngữ học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(251, 'Ngôn ngữ Italia', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(252, 'Ngôn ngữ Jrai', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(253, 'Ngôn ngữ Khme', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(254, 'Ngôn ngữ Nga', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(255, 'Ngôn ngữ Nhật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(256, 'Ngôn ngữ Pháp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(257, 'Ngôn ngữ Tây Ban Nha', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(258, 'Ngôn ngữ Trung Quốc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(259, 'Ngôn ngữ và văn hoá nước ngoài', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(260, 'Ngôn ngữ và văn hoá Việt Nam', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(261, 'Nhạc Jazz', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(262, 'Nhân học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(263, 'Nhân văn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(264, 'Nhân văn khác', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(265, 'Nhật Bản học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(266, 'Nhiếp ảnh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(267, 'Nông học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(268, 'Nông nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(269, 'Nông, lâm nghiệp và thuỷ sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(270, 'Nuôi trồng thuỷ sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(271, 'Pháp luật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(272, 'Phát triển nông thôn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(273, 'Phòng cháy chữa cháy và cứu hộ cứu nạn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(274, 'Piano', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(275, 'Quan hệ công chúng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(276, 'Quan hệ quốc tế', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(277, 'Quản lý bệnh viện', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(278, 'Quản lý công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(279, 'Quản lý đất đai ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(280, 'Quản lý giáo dục', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(281, 'Quản lý hoạt động bay', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(282, 'Quản lý nguồn lợi thủy sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(283, 'Quản lý nhà nước', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(284, 'Quản lý nhà nước về an ninh trật tự', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(285, 'Quản lý tài nguyên rừng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(286, 'Quản lý tài nguyên và môi trường', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(287, 'Quản lý thể dục thể thao*', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(288, 'Quản lý văn hoá', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(289, 'Quản lý xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(290, 'Quản lý, giáo dục và cải tạo phạm nhân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(291, 'Quân sự', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(292, 'Quân sự cơ sở', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(293, 'Quản trị – Quản lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(294, 'Quản trị dịch vụ du lịch và lữ hành', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(295, 'Quản trị khách sạn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(296, 'Quản trị kinh doanh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(297, 'Quản trị nhà hàng và dịch vụ ăn uống', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(298, 'Quản trị nhân lực', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(299, 'Quản trị văn phòng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(300, 'Quay phim', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(301, 'Quốc tế học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(302, 'Quy hoạch vùng và đô thị', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(303, 'Răng - Hàm - Mặt', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(304, 'Sản xuất và chế biến', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(305, 'Sản xuất, chế biến khác', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(306, 'Sản xuất, chế biến sợi, vải, giày, da', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(307, 'Sáng tác âm nhạc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(308, 'Sáng tác văn học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(309, 'Sinh học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(310, 'Sinh học ứng dụng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(311, 'Sư phạm Âm nhạc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(312, 'Sư phạm Công tác Đội thiếu niên Tiền phong HCM', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(313, 'Sư phạm Địa lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(314, 'Sư phạm Hoá học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(315, 'Sư phạm Kinh tế gia đình', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(316, 'Sư phạm Kỹ thuật công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(317, 'Sư phạm Kỹ thuật nông nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(318, 'Sư phạm Lịch sử', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(319, 'Sư phạm Mỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(320, 'Sư phạm Ngữ văn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(321, 'Sư phạm Sinh học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(322, 'Sư phạm Tiếng Anh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(323, 'Sư phạm Tiếng Bahna', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(324, 'Sư phạm Tiếng Bana', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(325, 'Sư phạm Tiếng Chăm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(326, 'Sư phạm Tiếng Đức', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(327, 'Sư phạm Tiếng Êđê', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(328, 'Sư phạm Tiếng H\'mong', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(329, 'Sư phạm Tiếng Jrai', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(330, 'Sư phạm Tiếng Khme', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(331, 'Sư phạm Tiếng M\'nông', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(332, 'Sư phạm Tiếng Nga', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(333, 'Sư phạm Tiếng Nhật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(334, 'Sư phạm Tiếng Pháp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(335, 'Sư phạm Tiếng Trung Quốc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(336, 'Sư phạm Tiếng Xêđăng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(337, 'Sư phạm Tin học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(338, 'Sư phạm Toán học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(339, 'Sư phạm Vật lý', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(340, 'Sức khoẻ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(341, 'Tài chính – Ngân hàng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(342, 'Tài chính – Ngân hàng – Bảo hiểm', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(343, 'Tâm lý học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(344, 'Tâm lý học giáo dục', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(345, 'Tham mưu, chỉ huy vũ trang bảo vệ an ninh trật tự', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(346, 'Thanh nhạc', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(347, 'Thiên văn học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(348, 'Thiết kế âm thanh - ánh sáng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(349, 'Thiết kế công nghiệp', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(350, 'Thiết kế đồ họa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(351, 'Thiết kế mỹ thuật sân khấu - điện ảnh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(352, 'Thiết kế nội thất', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(353, 'Thiết kế thời trang', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(354, 'Thống kê', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(355, 'Thông tin - Thư viện', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(356, 'Thông tin học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(357, 'Thông tin -Thư viện', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(358, 'Thư ký văn phòng ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(359, 'Thú y', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(360, 'Thuỷ sản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(361, 'Thuỷ văn', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(362, 'Tiếng Anh                       ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(363, 'Tiếng Hàn Quốc ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(364, 'Tiếng Khơ me', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(365, 'Tiếng Lào', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(366, 'Tiếng Nhật      ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(367, 'Tiếng Pháp           ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(368, 'Tiếng Thái', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(369, 'Tiếng Trung Quốc           ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(370, 'Tiếng Việt và văn hoá Việt Nam', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(371, 'Tin học ứng dụng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(372, 'Tình báo an ninh', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(373, 'Tình báo quân sự', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(374, 'Toán cơ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(375, 'Toán học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(376, 'Toán ứng dụng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(377, 'Toán và thống kê', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(378, 'Triết học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(379, 'Trinh sát kỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(380, 'Trung Quốc học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(381, 'Truyền thông đa phương tiện', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(382, 'Truyền thông quốc tế', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(383, 'Truyền thông và mạng máy tính', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(384, 'Vận hành khai thác máy tàu', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(385, 'Văn hoá các dân tộc thiểu số Việt Nam', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(386, 'Văn hoá học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(387, 'Văn học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(388, 'Văn thư - Lưu trữ - Bảo tàng ', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(389, 'Vật lý hạt nhân', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(390, 'Vật lý học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(391, 'Vật lý kỹ thuật', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(392, 'Việt Nam học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(393, 'Xã hội học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(394, 'Xã hội học và Nhân học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(395, 'Xây dựng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(396, 'Xây dựng Đảng và chính quyền nhà nước', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(397, 'Xét nghiệm y học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(398, 'Xuất bản', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(399, 'Xuất bản - Phát hành', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(400, 'Y đa khoa', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(401, 'Y học', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(402, 'Y học cổ truyền', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(403, 'Y học dự phòng', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(404, 'Y sinh học thể dục thể thao', '2017-01-19 22:30:26', '2017-01-19 22:30:26'),
(405, 'Y tế công cộng', '2017-01-19 22:30:26', '2017-01-19 22:30:26');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_fb_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_twitter_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_gplus_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_linkedin_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_pinterest_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_address` text COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lng` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `admin_name`, `admin_email`, `site_title`, `contact_email`, `contact_name`, `contact_phone`, `site_logo`, `site_fb_link`, `site_twitter_link`, `site_gplus_link`, `site_linkedin_link`, `site_pinterest_link`, `created_at`, `updated_at`, `seo_title`, `seo_keywords`, `seo_description`, `contact_address`, `lat`, `lng`) VALUES
(1, 'Admin', 'admin@admin.com', 'iKnow', 'info@admin.com', 'Contact iKnow', '9234567890', '14809006755mGTvG5Elh.png', 'https://www.facebook.com/example', 'https://www.twitter.com/example', 'https://plus.google.com/u/0/+example', 'https://www.linkedin.com/example', 'https://www.pinterest.com/example', NULL, '2016-12-05 07:18:16', 'iKnow', '', '', '141 Lê Duẩn, Cửa Nam, Hanoi, Vietnam', '21.0231979', '105.84164680000004');

-- --------------------------------------------------------

--
-- Table structure for table `timezones`
--

CREATE TABLE `timezones` (
  `id` int(10) UNSIGNED NOT NULL,
  `CountryCode` char(2) NOT NULL,
  `Coordinates` char(15) NOT NULL,
  `TimeZone` char(32) NOT NULL,
  `Comments` varchar(85) NOT NULL,
  `UTC_offset` char(8) NOT NULL,
  `UTC_DST_offset` char(8) NOT NULL,
  `Notes` varchar(79) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `timezones`
--

INSERT INTO `timezones` (`id`, `CountryCode`, `Coordinates`, `TimeZone`, `Comments`, `UTC_offset`, `UTC_DST_offset`, `Notes`) VALUES
(1, 'CI', '+0519-00402', 'Africa/Abidjan', '', '+00:00', '+00:00', ''),
(2, 'GH', '+0533-00013', 'Africa/Accra', '', '+00:00', '+00:00', ''),
(3, 'ET', '+0902+03842', 'Africa/Addis_Ababa', '', '+03:00', '+03:00', ''),
(4, 'DZ', '+3647+00303', 'Africa/Algiers', '', '+01:00', '+01:00', ''),
(5, 'ER', '+1520+03853', 'Africa/Asmara', '', '+03:00', '+03:00', ''),
(6, '', '', 'Africa/Asmera', '', '+03:00', '+03:00', 'Link to Africa/Asmara'),
(7, 'ML', '+1239-00800', 'Africa/Bamako', '', '+00:00', '+00:00', ''),
(8, 'CF', '+0422+01835', 'Africa/Bangui', '', '+01:00', '+01:00', ''),
(9, 'GM', '+1328-01639', 'Africa/Banjul', '', '+00:00', '+00:00', ''),
(10, 'GW', '+1151-01535', 'Africa/Bissau', '', '+00:00', '+00:00', ''),
(11, 'MW', '-1547+03500', 'Africa/Blantyre', '', '+02:00', '+02:00', ''),
(12, 'CG', '-0416+01517', 'Africa/Brazzaville', '', '+01:00', '+01:00', ''),
(13, 'BI', '-0323+02922', 'Africa/Bujumbura', '', '+02:00', '+02:00', ''),
(14, 'EG', '+3003+03115', 'Africa/Cairo', '', '+02:00', '+02:00', 'DST has been canceled since 2011'),
(15, 'MA', '+3339-00735', 'Africa/Casablanca', '', '+00:00', '+01:00', ''),
(16, 'ES', '+3553-00519', 'Africa/Ceuta', 'Ceuta & Melilla', '+01:00', '+02:00', ''),
(17, 'GN', '+0931-01343', 'Africa/Conakry', '', '+00:00', '+00:00', ''),
(18, 'SN', '+1440-01726', 'Africa/Dakar', '', '+00:00', '+00:00', ''),
(19, 'TZ', '-0648+03917', 'Africa/Dar_es_Salaam', '', '+03:00', '+03:00', ''),
(20, 'DJ', '+1136+04309', 'Africa/Djibouti', '', '+03:00', '+03:00', ''),
(21, 'CM', '+0403+00942', 'Africa/Douala', '', '+01:00', '+01:00', ''),
(22, 'EH', '+2709-01312', 'Africa/El_Aaiun', '', '+00:00', '+00:00', ''),
(23, 'SL', '+0830-01315', 'Africa/Freetown', '', '+00:00', '+00:00', ''),
(24, 'BW', '-2439+02555', 'Africa/Gaborone', '', '+02:00', '+02:00', ''),
(25, 'ZW', '-1750+03103', 'Africa/Harare', '', '+02:00', '+02:00', ''),
(26, 'ZA', '-2615+02800', 'Africa/Johannesburg', '', '+02:00', '+02:00', ''),
(27, 'SS', '+0451+03136', 'Africa/Juba', '', '+03:00', '+03:00', ''),
(28, 'UG', '+0019+03225', 'Africa/Kampala', '', '+03:00', '+03:00', ''),
(29, 'SD', '+1536+03232', 'Africa/Khartoum', '', '+03:00', '+03:00', ''),
(30, 'RW', '-0157+03004', 'Africa/Kigali', '', '+02:00', '+02:00', ''),
(31, 'CD', '-0418+01518', 'Africa/Kinshasa', 'west Dem. Rep. of Congo', '+01:00', '+01:00', ''),
(32, 'NG', '+0627+00324', 'Africa/Lagos', '', '+01:00', '+01:00', ''),
(33, 'GA', '+0023+00927', 'Africa/Libreville', '', '+01:00', '+01:00', ''),
(34, 'TG', '+0608+00113', 'Africa/Lome', '', '+00:00', '+00:00', ''),
(35, 'AO', '-0848+01314', 'Africa/Luanda', '', '+01:00', '+01:00', ''),
(36, 'CD', '-1140+02728', 'Africa/Lubumbashi', 'east Dem. Rep. of Congo', '+02:00', '+02:00', ''),
(37, 'ZM', '-1525+02817', 'Africa/Lusaka', '', '+02:00', '+02:00', ''),
(38, 'GQ', '+0345+00847', 'Africa/Malabo', '', '+01:00', '+01:00', ''),
(39, 'MZ', '-2558+03235', 'Africa/Maputo', '', '+02:00', '+02:00', ''),
(40, 'LS', '-2928+02730', 'Africa/Maseru', '', '+02:00', '+02:00', ''),
(41, 'SZ', '-2618+03106', 'Africa/Mbabane', '', '+02:00', '+02:00', ''),
(42, 'SO', '+0204+04522', 'Africa/Mogadishu', '', '+03:00', '+03:00', ''),
(43, 'LR', '+0618-01047', 'Africa/Monrovia', '', '+00:00', '+00:00', ''),
(44, 'KE', '-0117+03649', 'Africa/Nairobi', '', '+03:00', '+03:00', ''),
(45, 'TD', '+1207+01503', 'Africa/Ndjamena', '', '+01:00', '+01:00', ''),
(46, 'NE', '+1331+00207', 'Africa/Niamey', '', '+01:00', '+01:00', ''),
(47, 'MR', '+1806-01557', 'Africa/Nouakchott', '', '+00:00', '+00:00', ''),
(48, 'BF', '+1222-00131', 'Africa/Ouagadougou', '', '+00:00', '+00:00', ''),
(49, 'BJ', '+0629+00237', 'Africa/Porto-Novo', '', '+01:00', '+01:00', ''),
(50, 'ST', '+0020+00644', 'Africa/Sao_Tome', '', '+00:00', '+00:00', ''),
(51, '', '', 'Africa/Timbuktu', '', '+00:00', '+00:00', 'Link to Africa/Bamako'),
(52, 'LY', '+3254+01311', 'Africa/Tripoli', '', '+01:00', '+02:00', ''),
(53, 'TN', '+3648+01011', 'Africa/Tunis', '', '+01:00', '+01:00', ''),
(54, 'NA', '-2234+01706', 'Africa/Windhoek', '', '+01:00', '+02:00', ''),
(55, '', '', 'AKST9AKDT', '', '−09:00', '−08:00', 'Link to America/Anchorage'),
(56, 'US', '+515248-1763929', 'America/Adak', 'Aleutian Islands', '−10:00', '−09:00', ''),
(57, 'US', '+611305-1495401', 'America/Anchorage', 'Alaska Time', '−09:00', '−08:00', ''),
(58, 'AI', '+1812-06304', 'America/Anguilla', '', '−04:00', '−04:00', ''),
(59, 'AG', '+1703-06148', 'America/Antigua', '', '−04:00', '−04:00', ''),
(60, 'BR', '-0712-04812', 'America/Araguaina', 'Tocantins', '−03:00', '−03:00', ''),
(61, 'AR', '-3436-05827', 'America/Argentina/Buenos_Aires', 'Buenos Aires (BA, CF)', '−03:00', '−03:00', ''),
(62, 'AR', '-2828-06547', 'America/Argentina/Catamarca', 'Catamarca (CT), Chubut (CH)', '−03:00', '−03:00', ''),
(63, '', '', 'America/Argentina/ComodRivadavia', '', '−03:00', '−03:00', 'Link to America/Argentina/Catamarca'),
(64, 'AR', '-3124-06411', 'America/Argentina/Cordoba', 'most locations (CB, CC, CN, ER, FM, MN, SE, SF)', '−03:00', '−03:00', ''),
(65, 'AR', '-2411-06518', 'America/Argentina/Jujuy', 'Jujuy (JY)', '−03:00', '−03:00', ''),
(66, 'AR', '-2926-06651', 'America/Argentina/La_Rioja', 'La Rioja (LR)', '−03:00', '−03:00', ''),
(67, 'AR', '-3253-06849', 'America/Argentina/Mendoza', 'Mendoza (MZ)', '−03:00', '−03:00', ''),
(68, 'AR', '-5138-06913', 'America/Argentina/Rio_Gallegos', 'Santa Cruz (SC)', '−03:00', '−03:00', ''),
(69, 'AR', '-2447-06525', 'America/Argentina/Salta', '(SA, LP, NQ, RN)', '−03:00', '−03:00', ''),
(70, 'AR', '-3132-06831', 'America/Argentina/San_Juan', 'San Juan (SJ)', '−03:00', '−03:00', ''),
(71, 'AR', '-3319-06621', 'America/Argentina/San_Luis', 'San Luis (SL)', '−03:00', '−03:00', ''),
(72, 'AR', '-2649-06513', 'America/Argentina/Tucuman', 'Tucuman (TM)', '−03:00', '−03:00', ''),
(73, 'AR', '-5448-06818', 'America/Argentina/Ushuaia', 'Tierra del Fuego (TF)', '−03:00', '−03:00', ''),
(74, 'AW', '+1230-06958', 'America/Aruba', '', '−04:00', '−04:00', ''),
(75, 'PY', '-2516-05740', 'America/Asuncion', '', '−04:00', '−03:00', ''),
(76, 'CA', '+484531-0913718', 'America/Atikokan', 'Eastern Standard Time - Atikokan, Ontario and Southampton I, Nunavut', '−05:00', '−05:00', ''),
(77, '', '', 'America/Atka', '', '−10:00', '−09:00', 'Link to America/Adak'),
(78, 'BR', '-1259-03831', 'America/Bahia', 'Bahia', '−03:00', '−03:00', ''),
(79, 'MX', '+2048-10515', 'America/Bahia_Banderas', 'Mexican Central Time - Bahia de Banderas', '−06:00', '−05:00', ''),
(80, 'BB', '+1306-05937', 'America/Barbados', '', '−04:00', '−04:00', ''),
(81, 'BR', '-0127-04829', 'America/Belem', 'Amapa, E Para', '−03:00', '−03:00', ''),
(82, 'BZ', '+1730-08812', 'America/Belize', '', '−06:00', '−06:00', ''),
(83, 'CA', '+5125-05707', 'America/Blanc-Sablon', 'Atlantic Standard Time - Quebec - Lower North Shore', '−04:00', '−04:00', ''),
(84, 'BR', '+0249-06040', 'America/Boa_Vista', 'Roraima', '−04:00', '−04:00', ''),
(85, 'CO', '+0436-07405', 'America/Bogota', '', '−05:00', '−05:00', ''),
(86, 'US', '+433649-1161209', 'America/Boise', 'Mountain Time - south Idaho & east Oregon', '−07:00', '−06:00', ''),
(87, '', '', 'America/Buenos_Aires', '', '−03:00', '−03:00', 'Link to America/Argentina/Buenos_Aires'),
(88, 'CA', '+690650-1050310', 'America/Cambridge_Bay', 'Mountain Time - west Nunavut', '−07:00', '−06:00', ''),
(89, 'BR', '-2027-05437', 'America/Campo_Grande', 'Mato Grosso do Sul', '−04:00', '−03:00', ''),
(90, 'MX', '+2105-08646', 'America/Cancun', 'Central Time - Quintana Roo', '−06:00', '−05:00', ''),
(91, 'VE', '+1030-06656', 'America/Caracas', '', '−04:30', '−04:30', ''),
(92, '', '', 'America/Catamarca', '', '−03:00', '−03:00', 'Link to America/Argentina/Catamarca'),
(93, 'GF', '+0456-05220', 'America/Cayenne', '', '−03:00', '−03:00', ''),
(94, 'KY', '+1918-08123', 'America/Cayman', '', '−05:00', '−05:00', ''),
(95, 'US', '+415100-0873900', 'America/Chicago', 'Central Time', '−06:00', '−05:00', ''),
(96, 'MX', '+2838-10605', 'America/Chihuahua', 'Mexican Mountain Time - Chihuahua away from US border', '−07:00', '−06:00', ''),
(97, '', '', 'America/Coral_Harbour', '', '−05:00', '−05:00', 'Link to America/Atikokan'),
(98, '', '', 'America/Cordoba', '', '−03:00', '−03:00', 'Link to America/Argentina/Cordoba'),
(99, 'CR', '+0956-08405', 'America/Costa_Rica', '', '−06:00', '−06:00', ''),
(100, 'CA', '+4906-11631', 'America/Creston', 'Mountain Standard Time - Creston, British Columbia', '−07:00', '−07:00', ''),
(101, 'BR', '-1535-05605', 'America/Cuiaba', 'Mato Grosso', '−04:00', '−03:00', ''),
(102, 'CW', '+1211-06900', 'America/Curacao', '', '−04:00', '−04:00', ''),
(103, 'GL', '+7646-01840', 'America/Danmarkshavn', 'east coast, north of Scoresbysund', '+00:00', '+00:00', ''),
(104, 'CA', '+6404-13925', 'America/Dawson', 'Pacific Time - north Yukon', '−08:00', '−07:00', ''),
(105, 'CA', '+5946-12014', 'America/Dawson_Creek', 'Mountain Standard Time - Dawson Creek & Fort Saint John, British Columbia', '−07:00', '−07:00', ''),
(106, 'US', '+394421-1045903', 'America/Denver', 'Mountain Time', '−07:00', '−06:00', ''),
(107, 'US', '+421953-0830245', 'America/Detroit', 'Eastern Time - Michigan - most locations', '−05:00', '−04:00', ''),
(108, 'DM', '+1518-06124', 'America/Dominica', '', '−04:00', '−04:00', ''),
(109, 'CA', '+5333-11328', 'America/Edmonton', 'Mountain Time - Alberta, east British Columbia & west Saskatchewan', '−07:00', '−06:00', ''),
(110, 'BR', '-0640-06952', 'America/Eirunepe', 'W Amazonas', '−04:00', '−04:00', ''),
(111, 'SV', '+1342-08912', 'America/El_Salvador', '', '−06:00', '−06:00', ''),
(112, '', '', 'America/Ensenada', '', '−08:00', '−07:00', 'Link to America/Tijuana'),
(113, 'BR', '-0343-03830', 'America/Fortaleza', 'NE Brazil (MA, PI, CE, RN, PB)', '−03:00', '−03:00', ''),
(114, '', '', 'America/Fort_Wayne', '', '−05:00', '−04:00', 'Link to America/Indiana/Indianapolis'),
(115, 'CA', '+4612-05957', 'America/Glace_Bay', 'Atlantic Time - Nova Scotia - places that did not observe DST 1966-1971', '−04:00', '−03:00', ''),
(116, 'GL', '+6411-05144', 'America/Godthab', 'most locations', '−03:00', '−02:00', ''),
(117, 'CA', '+5320-06025', 'America/Goose_Bay', 'Atlantic Time - Labrador - most locations', '−04:00', '−03:00', ''),
(118, 'TC', '+2128-07108', 'America/Grand_Turk', '', '−05:00', '−04:00', ''),
(119, 'GD', '+1203-06145', 'America/Grenada', '', '−04:00', '−04:00', ''),
(120, 'GP', '+1614-06132', 'America/Guadeloupe', '', '−04:00', '−04:00', ''),
(121, 'GT', '+1438-09031', 'America/Guatemala', '', '−06:00', '−06:00', ''),
(122, 'EC', '-0210-07950', 'America/Guayaquil', 'mainland', '−05:00', '−05:00', ''),
(123, 'GY', '+0648-05810', 'America/Guyana', '', '−04:00', '−04:00', ''),
(124, 'CA', '+4439-06336', 'America/Halifax', 'Atlantic Time - Nova Scotia (most places), PEI', '−04:00', '−03:00', ''),
(125, 'CU', '+2308-08222', 'America/Havana', '', '−05:00', '−04:00', ''),
(126, 'MX', '+2904-11058', 'America/Hermosillo', 'Mountain Standard Time - Sonora', '−07:00', '−07:00', ''),
(127, 'US', '+394606-0860929', 'America/Indiana/Indianapolis', 'Eastern Time - Indiana - most locations', '−05:00', '−04:00', ''),
(128, 'US', '+411745-0863730', 'America/Indiana/Knox', 'Central Time - Indiana - Starke County', '−06:00', '−05:00', ''),
(129, 'US', '+382232-0862041', 'America/Indiana/Marengo', 'Eastern Time - Indiana - Crawford County', '−05:00', '−04:00', ''),
(130, 'US', '+382931-0871643', 'America/Indiana/Petersburg', 'Eastern Time - Indiana - Pike County', '−05:00', '−04:00', ''),
(131, 'US', '+375711-0864541', 'America/Indiana/Tell_City', 'Central Time - Indiana - Perry County', '−06:00', '−05:00', ''),
(132, 'US', '+384452-0850402', 'America/Indiana/Vevay', 'Eastern Time - Indiana - Switzerland County', '−05:00', '−04:00', ''),
(133, 'US', '+384038-0873143', 'America/Indiana/Vincennes', 'Eastern Time - Indiana - Daviess, Dubois, Knox & Martin Counties', '−05:00', '−04:00', ''),
(134, 'US', '+410305-0863611', 'America/Indiana/Winamac', 'Eastern Time - Indiana - Pulaski County', '−05:00', '−04:00', ''),
(135, '', '', 'America/Indianapolis', '', '−05:00', '−04:00', 'Link to America/Indiana/Indianapolis'),
(136, 'CA', '+682059-1334300', 'America/Inuvik', 'Mountain Time - west Northwest Territories', '−07:00', '−06:00', ''),
(137, 'CA', '+6344-06828', 'America/Iqaluit', 'Eastern Time - east Nunavut - most locations', '−05:00', '−04:00', ''),
(138, 'JM', '+1800-07648', 'America/Jamaica', '', '−05:00', '−05:00', ''),
(139, '', '', 'America/Jujuy', '', '−03:00', '−03:00', 'Link to America/Argentina/Jujuy'),
(140, 'US', '+581807-1342511', 'America/Juneau', 'Alaska Time - Alaska panhandle', '−09:00', '−08:00', ''),
(141, 'US', '+381515-0854534', 'America/Kentucky/Louisville', 'Eastern Time - Kentucky - Louisville area', '−05:00', '−04:00', ''),
(142, 'US', '+364947-0845057', 'America/Kentucky/Monticello', 'Eastern Time - Kentucky - Wayne County', '−05:00', '−04:00', ''),
(143, '', '', 'America/Knox_IN', '', '−06:00', '−05:00', 'Link to America/Indiana/Knox'),
(144, 'BQ', '+120903-0681636', 'America/Kralendijk', '', '−04:00', '−04:00', 'Link to America/Curacao'),
(145, 'BO', '-1630-06809', 'America/La_Paz', '', '−04:00', '−04:00', ''),
(146, 'PE', '-1203-07703', 'America/Lima', '', '−05:00', '−05:00', ''),
(147, 'US', '+340308-1181434', 'America/Los_Angeles', 'Pacific Time', '−08:00', '−07:00', ''),
(148, '', '', 'America/Louisville', '', '−05:00', '−04:00', 'Link to America/Kentucky/Louisville'),
(149, 'SX', '+180305-0630250', 'America/Lower_Princes', '', '−04:00', '−04:00', 'Link to America/Curacao'),
(150, 'BR', '-0940-03543', 'America/Maceio', 'Alagoas, Sergipe', '−03:00', '−03:00', ''),
(151, 'NI', '+1209-08617', 'America/Managua', '', '−06:00', '−06:00', ''),
(152, 'BR', '-0308-06001', 'America/Manaus', 'E Amazonas', '−04:00', '−04:00', ''),
(153, 'MF', '+1804-06305', 'America/Marigot', '', '−04:00', '−04:00', 'Link to America/Guadeloupe'),
(154, 'MQ', '+1436-06105', 'America/Martinique', '', '−04:00', '−04:00', ''),
(155, 'MX', '+2550-09730', 'America/Matamoros', 'US Central Time - Coahuila, Durango, Nuevo León, Tamaulipas near US border', '−06:00', '−05:00', ''),
(156, 'MX', '+2313-10625', 'America/Mazatlan', 'Mountain Time - S Baja, Nayarit, Sinaloa', '−07:00', '−06:00', ''),
(157, '', '', 'America/Mendoza', '', '−03:00', '−03:00', 'Link to America/Argentina/Mendoza'),
(158, 'US', '+450628-0873651', 'America/Menominee', 'Central Time - Michigan - Dickinson, Gogebic, Iron & Menominee Counties', '−06:00', '−05:00', ''),
(159, 'MX', '+2058-08937', 'America/Merida', 'Central Time - Campeche, Yucatán', '−06:00', '−05:00', ''),
(160, 'US', '+550737-1313435', 'America/Metlakatla', 'Metlakatla Time - Annette Island', '−08:00', '−08:00', ''),
(161, 'MX', '+1924-09909', 'America/Mexico_City', 'Central Time - most locations', '−06:00', '−05:00', ''),
(162, 'PM', '+4703-05620', 'America/Miquelon', '', '−03:00', '−02:00', ''),
(163, 'CA', '+4606-06447', 'America/Moncton', 'Atlantic Time - New Brunswick', '−04:00', '−03:00', ''),
(164, 'MX', '+2540-10019', 'America/Monterrey', 'Mexican Central Time - Coahuila, Durango, Nuevo León, Tamaulipas away from US border', '−06:00', '−05:00', ''),
(165, 'UY', '-3453-05611', 'America/Montevideo', '', '−03:00', '−02:00', ''),
(166, 'CA', '+4531-07334', 'America/Montreal', 'Eastern Time - Quebec - most locations', '−05:00', '−04:00', ''),
(167, 'MS', '+1643-06213', 'America/Montserrat', '', '−04:00', '−04:00', ''),
(168, 'BS', '+2505-07721', 'America/Nassau', '', '−05:00', '−04:00', ''),
(169, 'US', '+404251-0740023', 'America/New_York', 'Eastern Time', '−05:00', '−04:00', ''),
(170, 'CA', '+4901-08816', 'America/Nipigon', 'Eastern Time - Ontario & Quebec - places that did not observe DST 1967-1973', '−05:00', '−04:00', ''),
(171, 'US', '+643004-1652423', 'America/Nome', 'Alaska Time - west Alaska', '−09:00', '−08:00', ''),
(172, 'BR', '-0351-03225', 'America/Noronha', 'Atlantic islands', '−02:00', '−02:00', ''),
(173, 'US', '+471551-1014640', 'America/North_Dakota/Beulah', 'Central Time - North Dakota - Mercer County', '−06:00', '−05:00', ''),
(174, 'US', '+470659-1011757', 'America/North_Dakota/Center', 'Central Time - North Dakota - Oliver County', '−06:00', '−05:00', ''),
(175, 'US', '+465042-1012439', 'America/North_Dakota/New_Salem', 'Central Time - North Dakota - Morton County (except Mandan area)', '−06:00', '−05:00', ''),
(176, 'MX', '+2934-10425', 'America/Ojinaga', 'US Mountain Time - Chihuahua near US border', '−07:00', '−06:00', ''),
(177, 'PA', '+0858-07932', 'America/Panama', '', '−05:00', '−05:00', ''),
(178, 'CA', '+6608-06544', 'America/Pangnirtung', 'Eastern Time - Pangnirtung, Nunavut', '−05:00', '−04:00', ''),
(179, 'SR', '+0550-05510', 'America/Paramaribo', '', '−03:00', '−03:00', ''),
(180, 'US', '+332654-1120424', 'America/Phoenix', 'Mountain Standard Time - Arizona', '−07:00', '−07:00', ''),
(181, 'HT', '+1832-07220', 'America/Port-au-Prince', '', '−05:00', '−04:00', ''),
(182, '', '', 'America/Porto_Acre', '', '−04:00', '−04:00', 'Link to America/Rio_Branco'),
(183, 'BR', '-0846-06354', 'America/Porto_Velho', 'Rondonia', '−04:00', '−04:00', ''),
(184, 'TT', '+1039-06131', 'America/Port_of_Spain', '', '−04:00', '−04:00', ''),
(185, 'PR', '+182806-0660622', 'America/Puerto_Rico', '', '−04:00', '−04:00', ''),
(186, 'CA', '+4843-09434', 'America/Rainy_River', 'Central Time - Rainy River & Fort Frances, Ontario', '−06:00', '−05:00', ''),
(187, 'CA', '+624900-0920459', 'America/Rankin_Inlet', 'Central Time - central Nunavut', '−06:00', '−05:00', ''),
(188, 'BR', '-0803-03454', 'America/Recife', 'Pernambuco', '−03:00', '−03:00', ''),
(189, 'CA', '+5024-10439', 'America/Regina', 'Central Standard Time - Saskatchewan - most locations', '−06:00', '−06:00', ''),
(190, 'CA', '+744144-0944945', 'America/Resolute', 'Central Standard Time - Resolute, Nunavut', '−06:00', '−05:00', ''),
(191, 'BR', '-0958-06748', 'America/Rio_Branco', 'Acre', '−04:00', '−04:00', ''),
(192, '', '', 'America/Rosario', '', '−03:00', '−03:00', 'Link to America/Argentina/Cordoba'),
(193, 'BR', '-0226-05452', 'America/Santarem', 'W Para', '−03:00', '−03:00', ''),
(194, 'MX', '+3018-11452', 'America/Santa_Isabel', 'Mexican Pacific Time - Baja California away from US border', '−08:00', '−07:00', ''),
(195, 'CL', '-3327-07040', 'America/Santiago', 'most locations', '−04:00', '−03:00', ''),
(196, 'DO', '+1828-06954', 'America/Santo_Domingo', '', '−04:00', '−04:00', ''),
(197, 'BR', '-2332-04637', 'America/Sao_Paulo', 'S & SE Brazil (GO, DF, MG, ES, RJ, SP, PR, SC, RS)', '−03:00', '−02:00', ''),
(198, 'GL', '+7029-02158', 'America/Scoresbysund', 'Scoresbysund / Ittoqqortoormiit', '−01:00', '+00:00', ''),
(199, 'US', '+364708-1084111', 'America/Shiprock', 'Mountain Time - Navajo', '−07:00', '−06:00', 'Link to America/Denver'),
(200, 'US', '+571035-1351807', 'America/Sitka', 'Alaska Time - southeast Alaska panhandle', '−09:00', '−08:00', ''),
(201, 'BL', '+1753-06251', 'America/St_Barthelemy', '', '−04:00', '−04:00', 'Link to America/Guadeloupe'),
(202, 'CA', '+4734-05243', 'America/St_Johns', 'Newfoundland Time, including SE Labrador', '−03:30', '−02:30', ''),
(203, 'KN', '+1718-06243', 'America/St_Kitts', '', '−04:00', '−04:00', ''),
(204, 'LC', '+1401-06100', 'America/St_Lucia', '', '−04:00', '−04:00', ''),
(205, 'VI', '+1821-06456', 'America/St_Thomas', '', '−04:00', '−04:00', ''),
(206, 'VC', '+1309-06114', 'America/St_Vincent', '', '−04:00', '−04:00', ''),
(207, 'CA', '+5017-10750', 'America/Swift_Current', 'Central Standard Time - Saskatchewan - midwest', '−06:00', '−06:00', ''),
(208, 'HN', '+1406-08713', 'America/Tegucigalpa', '', '−06:00', '−06:00', ''),
(209, 'GL', '+7634-06847', 'America/Thule', 'Thule / Pituffik', '−04:00', '−03:00', ''),
(210, 'CA', '+4823-08915', 'America/Thunder_Bay', 'Eastern Time - Thunder Bay, Ontario', '−05:00', '−04:00', ''),
(211, 'MX', '+3232-11701', 'America/Tijuana', 'US Pacific Time - Baja California near US border', '−08:00', '−07:00', ''),
(212, 'CA', '+4339-07923', 'America/Toronto', 'Eastern Time - Ontario - most locations', '−05:00', '−04:00', ''),
(213, 'VG', '+1827-06437', 'America/Tortola', '', '−04:00', '−04:00', ''),
(214, 'CA', '+4916-12307', 'America/Vancouver', 'Pacific Time - west British Columbia', '−08:00', '−07:00', ''),
(215, '', '', 'America/Virgin', '', '−04:00', '−04:00', 'Link to America/St_Thomas'),
(216, 'CA', '+6043-13503', 'America/Whitehorse', 'Pacific Time - south Yukon', '−08:00', '−07:00', ''),
(217, 'CA', '+4953-09709', 'America/Winnipeg', 'Central Time - Manitoba & west Ontario', '−06:00', '−05:00', ''),
(218, 'US', '+593249-1394338', 'America/Yakutat', 'Alaska Time - Alaska panhandle neck', '−09:00', '−08:00', ''),
(219, 'CA', '+6227-11421', 'America/Yellowknife', 'Mountain Time - central Northwest Territories', '−07:00', '−06:00', ''),
(220, 'AQ', '-6617+11031', 'Antarctica/Casey', 'Casey Station, Bailey Peninsula', '+11:00', '+08:00', ''),
(221, 'AQ', '-6835+07758', 'Antarctica/Davis', 'Davis Station, Vestfold Hills', '+05:00', '+07:00', ''),
(222, 'AQ', '-6640+14001', 'Antarctica/DumontDUrville', 'Dumont-d\'Urville Station, Terre Adelie', '+10:00', '+10:00', ''),
(223, 'AQ', '-5430+15857', 'Antarctica/Macquarie', 'Macquarie Island Station, Macquarie Island', '+11:00', '+11:00', ''),
(224, 'AQ', '-6736+06253', 'Antarctica/Mawson', 'Mawson Station, Holme Bay', '+05:00', '+05:00', ''),
(225, 'AQ', '-7750+16636', 'Antarctica/McMurdo', 'McMurdo Station, Ross Island', '+12:00', '+13:00', ''),
(226, 'AQ', '-6448-06406', 'Antarctica/Palmer', 'Palmer Station, Anvers Island', '−04:00', '−03:00', ''),
(227, 'AQ', '-6734-06808', 'Antarctica/Rothera', 'Rothera Station, Adelaide Island', '−03:00', '−03:00', ''),
(228, 'AQ', '-9000+00000', 'Antarctica/South_Pole', 'Amundsen-Scott Station, South Pole', '+12:00', '+13:00', 'Link to Antarctica/McMurdo'),
(229, 'AQ', '-690022+0393524', 'Antarctica/Syowa', 'Syowa Station, E Ongul I', '+03:00', '+03:00', ''),
(230, 'AQ', '-7824+10654', 'Antarctica/Vostok', 'Vostok Station, Lake Vostok', '+06:00', '+06:00', ''),
(231, 'SJ', '+7800+01600', 'Arctic/Longyearbyen', '', '+01:00', '+02:00', 'Link to Europe/Oslo'),
(232, 'YE', '+1245+04512', 'Asia/Aden', '', '+03:00', '+03:00', ''),
(233, 'KZ', '+4315+07657', 'Asia/Almaty', 'most locations', '+06:00', '+06:00', ''),
(234, 'JO', '+3157+03556', 'Asia/Amman', '', '+03:00', '+03:00', ''),
(235, 'RU', '+6445+17729', 'Asia/Anadyr', 'Moscow+08 - Bering Sea', '+12:00', '+12:00', ''),
(236, 'KZ', '+4431+05016', 'Asia/Aqtau', 'Atyrau (Atirau, Gur\'yev), Mangghystau (Mankistau)', '+05:00', '+05:00', ''),
(237, 'KZ', '+5017+05710', 'Asia/Aqtobe', 'Aqtobe (Aktobe)', '+05:00', '+05:00', ''),
(238, 'TM', '+3757+05823', 'Asia/Ashgabat', '', '+05:00', '+05:00', ''),
(239, '', '', 'Asia/Ashkhabad', '', '+05:00', '+05:00', 'Link to Asia/Ashgabat'),
(240, 'IQ', '+3321+04425', 'Asia/Baghdad', '', '+03:00', '+03:00', ''),
(241, 'BH', '+2623+05035', 'Asia/Bahrain', '', '+03:00', '+03:00', ''),
(242, 'AZ', '+4023+04951', 'Asia/Baku', '', '+04:00', '+05:00', ''),
(243, 'TH', '+1345+10031', 'Asia/Bangkok', '', '+07:00', '+07:00', ''),
(244, 'LB', '+3353+03530', 'Asia/Beirut', '', '+02:00', '+03:00', ''),
(245, 'KG', '+4254+07436', 'Asia/Bishkek', '', '+06:00', '+06:00', ''),
(246, 'BN', '+0456+11455', 'Asia/Brunei', '', '+08:00', '+08:00', ''),
(247, '', '', 'Asia/Calcutta', '', '+05:30', '+05:30', 'Link to Asia/Kolkata'),
(248, 'MN', '+4804+11430', 'Asia/Choibalsan', 'Dornod, Sukhbaatar', '+08:00', '+08:00', ''),
(249, 'CN', '+2934+10635', 'Asia/Chongqing', 'central China - Sichuan, Yunnan, Guangxi, Shaanxi, Guizhou, etc.', '+08:00', '+08:00', 'Covering historic Kansu-Szechuan time zone.'),
(250, '', '', 'Asia/Chungking', '', '+08:00', '+08:00', 'Link to Asia/Chongqing'),
(251, 'LK', '+0656+07951', 'Asia/Colombo', '', '+05:30', '+05:30', ''),
(252, '', '', 'Asia/Dacca', '', '+06:00', '+06:00', 'Link to Asia/Dhaka'),
(253, 'SY', '+3330+03618', 'Asia/Damascus', '', '+02:00', '+03:00', ''),
(254, 'BD', '+2343+09025', 'Asia/Dhaka', '', '+06:00', '+06:00', ''),
(255, 'TL', '-0833+12535', 'Asia/Dili', '', '+09:00', '+09:00', ''),
(256, 'AE', '+2518+05518', 'Asia/Dubai', '', '+04:00', '+04:00', ''),
(257, 'TJ', '+3835+06848', 'Asia/Dushanbe', '', '+05:00', '+05:00', ''),
(258, 'PS', '+3130+03428', 'Asia/Gaza', 'Gaza Strip', '+02:00', '+03:00', ''),
(259, 'CN', '+4545+12641', 'Asia/Harbin', 'Heilongjiang (except Mohe), Jilin', '+08:00', '+08:00', 'Covering historic Changpai time zone.'),
(260, 'PS', '+313200+0350542', 'Asia/Hebron', 'West Bank', '+02:00', '+03:00', ''),
(261, 'HK', '+2217+11409', 'Asia/Hong_Kong', '', '+08:00', '+08:00', ''),
(262, 'MN', '+4801+09139', 'Asia/Hovd', 'Bayan-Olgiy, Govi-Altai, Hovd, Uvs, Zavkhan', '+07:00', '+07:00', ''),
(263, 'VN', '+1045+10640', 'Asia/Ho_Chi_Minh', '', '+07:00', '+07:00', ''),
(264, 'RU', '+5216+10420', 'Asia/Irkutsk', 'Moscow+05 - Lake Baikal', '+09:00', '+09:00', ''),
(265, '', '', 'Asia/Istanbul', '', '+02:00', '+03:00', 'Link to Europe/Istanbul'),
(266, 'ID', '-0610+10648', 'Asia/Jakarta', 'Java & Sumatra', '+07:00', '+07:00', ''),
(267, 'ID', '-0232+14042', 'Asia/Jayapura', 'west New Guinea (Irian Jaya) & Malukus (Moluccas)', '+09:00', '+09:00', ''),
(268, 'IL', '+3146+03514', 'Asia/Jerusalem', '', '+02:00', '+03:00', ''),
(269, 'AF', '+3431+06912', 'Asia/Kabul', '', '+04:30', '+04:30', ''),
(270, 'RU', '+5301+15839', 'Asia/Kamchatka', 'Moscow+08 - Kamchatka', '+12:00', '+12:00', ''),
(271, 'PK', '+2452+06703', 'Asia/Karachi', '', '+05:00', '+05:00', ''),
(272, 'CN', '+3929+07559', 'Asia/Kashgar', 'west Tibet & Xinjiang', '+08:00', '+08:00', 'Covering historic Kunlun time zone.'),
(273, 'NP', '+2743+08519', 'Asia/Kathmandu', '', '+05:45', '+05:45', ''),
(274, '', '', 'Asia/Katmandu', '', '+05:45', '+05:45', 'Link to Asia/Kathmandu'),
(275, 'IN', '+2232+08822', 'Asia/Kolkata', '', '+05:30', '+05:30', 'Note: Different zones in history, see Time in India.'),
(276, 'RU', '+5601+09250', 'Asia/Krasnoyarsk', 'Moscow+04 - Yenisei River', '+08:00', '+08:00', ''),
(277, 'MY', '+0310+10142', 'Asia/Kuala_Lumpur', 'peninsular Malaysia', '+08:00', '+08:00', ''),
(278, 'MY', '+0133+11020', 'Asia/Kuching', 'Sabah & Sarawak', '+08:00', '+08:00', ''),
(279, 'KW', '+2920+04759', 'Asia/Kuwait', '', '+03:00', '+03:00', ''),
(280, '', '', 'Asia/Macao', '', '+08:00', '+08:00', 'Link to Asia/Macau'),
(281, 'MO', '+2214+11335', 'Asia/Macau', '', '+08:00', '+08:00', ''),
(282, 'RU', '+5934+15048', 'Asia/Magadan', 'Moscow+08 - Magadan', '+12:00', '+12:00', ''),
(283, 'ID', '-0507+11924', 'Asia/Makassar', 'east & south Borneo, Sulawesi (Celebes), Bali, Nusa Tenggara, west Timor', '+08:00', '+08:00', ''),
(284, 'PH', '+1435+12100', 'Asia/Manila', '', '+08:00', '+08:00', ''),
(285, 'OM', '+2336+05835', 'Asia/Muscat', '', '+04:00', '+04:00', ''),
(286, 'CY', '+3510+03322', 'Asia/Nicosia', '', '+02:00', '+03:00', ''),
(287, 'RU', '+5345+08707', 'Asia/Novokuznetsk', 'Moscow+03 - Novokuznetsk', '+07:00', '+07:00', ''),
(288, 'RU', '+5502+08255', 'Asia/Novosibirsk', 'Moscow+03 - Novosibirsk', '+07:00', '+07:00', ''),
(289, 'RU', '+5500+07324', 'Asia/Omsk', 'Moscow+03 - west Siberia', '+07:00', '+07:00', ''),
(290, 'KZ', '+5113+05121', 'Asia/Oral', 'West Kazakhstan', '+05:00', '+05:00', ''),
(291, 'KH', '+1133+10455', 'Asia/Phnom_Penh', '', '+07:00', '+07:00', ''),
(292, 'ID', '-0002+10920', 'Asia/Pontianak', 'west & central Borneo', '+07:00', '+07:00', ''),
(293, 'KP', '+3901+12545', 'Asia/Pyongyang', '', '+09:00', '+09:00', ''),
(294, 'QA', '+2517+05132', 'Asia/Qatar', '', '+03:00', '+03:00', ''),
(295, 'KZ', '+4448+06528', 'Asia/Qyzylorda', 'Qyzylorda (Kyzylorda, Kzyl-Orda)', '+06:00', '+06:00', ''),
(296, 'MM', '+1647+09610', 'Asia/Rangoon', '', '+06:30', '+06:30', ''),
(297, 'SA', '+2438+04643', 'Asia/Riyadh', '', '+03:00', '+03:00', ''),
(298, '', '', 'Asia/Saigon', '', '+07:00', '+07:00', 'Link to Asia/Ho_Chi_Minh'),
(299, 'RU', '+4658+14242', 'Asia/Sakhalin', 'Moscow+07 - Sakhalin Island', '+11:00', '+11:00', ''),
(300, 'UZ', '+3940+06648', 'Asia/Samarkand', 'west Uzbekistan', '+05:00', '+05:00', ''),
(301, 'KR', '+3733+12658', 'Asia/Seoul', '', '+09:00', '+09:00', ''),
(302, 'CN', '+3114+12128', 'Asia/Shanghai', 'east China - Beijing, Guangdong, Shanghai, etc.', '+08:00', '+08:00', 'Covering historic Chungyuan time zone.'),
(303, 'SG', '+0117+10351', 'Asia/Singapore', '', '+08:00', '+08:00', ''),
(304, 'TW', '+2503+12130', 'Asia/Taipei', '', '+08:00', '+08:00', ''),
(305, 'UZ', '+4120+06918', 'Asia/Tashkent', 'east Uzbekistan', '+05:00', '+05:00', ''),
(306, 'GE', '+4143+04449', 'Asia/Tbilisi', '', '+04:00', '+04:00', ''),
(307, 'IR', '+3540+05126', 'Asia/Tehran', '', '+03:30', '+04:30', ''),
(308, '', '', 'Asia/Tel_Aviv', '', '+02:00', '+03:00', 'Link to Asia/Jerusalem'),
(309, '', '', 'Asia/Thimbu', '', '+06:00', '+06:00', 'Link to Asia/Thimphu'),
(310, 'BT', '+2728+08939', 'Asia/Thimphu', '', '+06:00', '+06:00', ''),
(311, 'JP', '+353916+1394441', 'Asia/Tokyo', '', '+09:00', '+09:00', ''),
(312, '', '', 'Asia/Ujung_Pandang', '', '+08:00', '+08:00', 'Link to Asia/Makassar'),
(313, 'MN', '+4755+10653', 'Asia/Ulaanbaatar', 'most locations', '+08:00', '+08:00', ''),
(314, '', '', 'Asia/Ulan_Bator', '', '+08:00', '+08:00', 'Link to Asia/Ulaanbaatar'),
(315, 'CN', '+4348+08735', 'Asia/Urumqi', 'most of Tibet & Xinjiang', '+08:00', '+08:00', 'Covering historic Sinkiang-Tibet time zone.'),
(316, 'LA', '+1758+10236', 'Asia/Vientiane', '', '+07:00', '+07:00', ''),
(317, 'RU', '+4310+13156', 'Asia/Vladivostok', 'Moscow+07 - Amur River', '+11:00', '+11:00', ''),
(318, 'RU', '+6200+12940', 'Asia/Yakutsk', 'Moscow+06 - Lena River', '+10:00', '+10:00', ''),
(319, 'RU', '+5651+06036', 'Asia/Yekaterinburg', 'Moscow+02 - Urals', '+06:00', '+06:00', ''),
(320, 'AM', '+4011+04430', 'Asia/Yerevan', '', '+04:00', '+04:00', ''),
(321, 'PT', '+3744-02540', 'Atlantic/Azores', 'Azores', '−01:00', '+00:00', ''),
(322, 'BM', '+3217-06446', 'Atlantic/Bermuda', '', '−04:00', '−03:00', ''),
(323, 'ES', '+2806-01524', 'Atlantic/Canary', 'Canary Islands', '+00:00', '+01:00', ''),
(324, 'CV', '+1455-02331', 'Atlantic/Cape_Verde', '', '−01:00', '−01:00', ''),
(325, '', '', 'Atlantic/Faeroe', '', '+00:00', '+01:00', 'Link to Atlantic/Faroe'),
(326, 'FO', '+6201-00646', 'Atlantic/Faroe', '', '+00:00', '+01:00', ''),
(327, '', '', 'Atlantic/Jan_Mayen', '', '+01:00', '+02:00', 'Link to Europe/Oslo'),
(328, 'PT', '+3238-01654', 'Atlantic/Madeira', 'Madeira Islands', '+00:00', '+01:00', ''),
(329, 'IS', '+6409-02151', 'Atlantic/Reykjavik', '', '+00:00', '+00:00', ''),
(330, 'GS', '-5416-03632', 'Atlantic/South_Georgia', '', '−02:00', '−02:00', ''),
(331, 'FK', '-5142-05751', 'Atlantic/Stanley', '', '−03:00', '−03:00', ''),
(332, 'SH', '-1555-00542', 'Atlantic/St_Helena', '', '+00:00', '+00:00', ''),
(333, '', '', 'Australia/ACT', '', '+10:00', '+11:00', 'Link to Australia/Sydney'),
(334, 'AU', '-3455+13835', 'Australia/Adelaide', 'South Australia', '+09:30', '+10:30', ''),
(335, 'AU', '-2728+15302', 'Australia/Brisbane', 'Queensland - most locations', '+10:00', '+10:00', ''),
(336, 'AU', '-3157+14127', 'Australia/Broken_Hill', 'New South Wales - Yancowinna', '+09:30', '+10:30', ''),
(337, '', '', 'Australia/Canberra', '', '+10:00', '+11:00', 'Link to Australia/Sydney'),
(338, 'AU', '-3956+14352', 'Australia/Currie', 'Tasmania - King Island', '+10:00', '+11:00', ''),
(339, 'AU', '-1228+13050', 'Australia/Darwin', 'Northern Territory', '+09:30', '+09:30', ''),
(340, 'AU', '-3143+12852', 'Australia/Eucla', 'Western Australia - Eucla area', '+08:45', '+08:45', ''),
(341, 'AU', '-4253+14719', 'Australia/Hobart', 'Tasmania - most locations', '+10:00', '+11:00', ''),
(342, '', '', 'Australia/LHI', '', '+10:30', '+11:00', 'Link to Australia/Lord_Howe'),
(343, 'AU', '-2016+14900', 'Australia/Lindeman', 'Queensland - Holiday Islands', '+10:00', '+10:00', ''),
(344, 'AU', '-3133+15905', 'Australia/Lord_Howe', 'Lord Howe Island', '+10:30', '+11:00', ''),
(345, 'AU', '-3749+14458', 'Australia/Melbourne', 'Victoria', '+10:00', '+11:00', ''),
(346, '', '', 'Australia/North', '', '+09:30', '+09:30', 'Link to Australia/Darwin'),
(347, '', '', 'Australia/NSW', '', '+10:00', '+11:00', 'Link to Australia/Sydney'),
(348, 'AU', '-3157+11551', 'Australia/Perth', 'Western Australia - most locations', '+08:00', '+08:00', ''),
(349, '', '', 'Australia/Queensland', '', '+10:00', '+10:00', 'Link to Australia/Brisbane'),
(350, '', '', 'Australia/South', '', '+09:30', '+10:30', 'Link to Australia/Adelaide'),
(351, 'AU', '-3352+15113', 'Australia/Sydney', 'New South Wales - most locations', '+10:00', '+11:00', ''),
(352, '', '', 'Australia/Tasmania', '', '+10:00', '+11:00', 'Link to Australia/Hobart'),
(353, '', '', 'Australia/Victoria', '', '+10:00', '+11:00', 'Link to Australia/Melbourne'),
(354, '', '', 'Australia/West', '', '+08:00', '+08:00', 'Link to Australia/Perth'),
(355, '', '', 'Australia/Yancowinna', '', '+09:30', '+10:30', 'Link to Australia/Broken_Hill'),
(356, '', '', 'Brazil/Acre', '', '−04:00', '−04:00', 'Link to America/Rio_Branco'),
(357, '', '', 'Brazil/DeNoronha', '', '−02:00', '−02:00', 'Link to America/Noronha'),
(358, '', '', 'Brazil/East', '', '−03:00', '−02:00', 'Link to America/Sao_Paulo'),
(359, '', '', 'Brazil/West', '', '−04:00', '−04:00', 'Link to America/Manaus'),
(360, '', '', 'Canada/Atlantic', '', '−04:00', '−03:00', 'Link to America/Halifax'),
(361, '', '', 'Canada/Central', '', '−06:00', '−05:00', 'Link to America/Winnipeg'),
(362, '', '', 'Canada/East-Saskatchewan', '', '−06:00', '−06:00', 'Link to America/Regina'),
(363, '', '', 'Canada/Eastern', '', '−05:00', '−04:00', 'Link to America/Toronto'),
(364, '', '', 'Canada/Mountain', '', '−07:00', '−06:00', 'Link to America/Edmonton'),
(365, '', '', 'Canada/Newfoundland', '', '−03:30', '−02:30', 'Link to America/St_Johns'),
(366, '', '', 'Canada/Pacific', '', '−08:00', '−07:00', 'Link to America/Vancouver'),
(367, '', '', 'Canada/Saskatchewan', '', '−06:00', '−06:00', 'Link to America/Regina'),
(368, '', '', 'Canada/Yukon', '', '−08:00', '−07:00', 'Link to America/Whitehorse'),
(369, '', '', 'CET', '', '+01:00', '+02:00', ''),
(370, '', '', 'Chile/Continental', '', '−04:00', '−03:00', 'Link to America/Santiago'),
(371, '', '', 'Chile/EasterIsland', '', '−06:00', '−05:00', 'Link to Pacific/Easter'),
(372, '', '', 'CST6CDT', '', '−06:00', '−05:00', ''),
(373, '', '', 'Cuba', '', '−05:00', '−04:00', 'Link to America/Havana'),
(374, '', '', 'EET', '', '+02:00', '+03:00', ''),
(375, '', '', 'Egypt', '', '+02:00', '+02:00', 'Link to Africa/Cairo'),
(376, '', '', 'Eire', '', '+00:00', '+01:00', 'Link to Europe/Dublin'),
(377, '', '', 'EST', '', '−05:00', '−05:00', ''),
(378, '', '', 'EST5EDT', '', '−05:00', '−04:00', ''),
(379, '', '', 'Etc./GMT', '', '+00:00', '+00:00', 'Link to UTC'),
(380, '', '', 'Etc./GMT+0', '', '+00:00', '+00:00', 'Link to UTC'),
(381, '', '', 'Etc./UCT', '', '+00:00', '+00:00', 'Link to UTC'),
(382, '', '', 'Etc./Universal', '', '+00:00', '+00:00', 'Link to UTC'),
(383, '', '', 'Etc./UTC', '', '+00:00', '+00:00', 'Link to UTC'),
(384, '', '', 'Etc./Zulu', '', '+00:00', '+00:00', 'Link to UTC'),
(385, 'NL', '+5222+00454', 'Europe/Amsterdam', '', '+01:00', '+02:00', ''),
(386, 'AD', '+4230+00131', 'Europe/Andorra', '', '+01:00', '+02:00', ''),
(387, 'GR', '+3758+02343', 'Europe/Athens', '', '+02:00', '+03:00', ''),
(388, '', '', 'Europe/Belfast', '', '+00:00', '+01:00', 'Link to Europe/London'),
(389, 'RS', '+4450+02030', 'Europe/Belgrade', '', '+01:00', '+02:00', ''),
(390, 'DE', '+5230+01322', 'Europe/Berlin', '', '+01:00', '+02:00', 'In 1945, the Trizone did not follow Berlin\'s switch to DST, see Time in Germany'),
(391, 'SK', '+4809+01707', 'Europe/Bratislava', '', '+01:00', '+02:00', 'Link to Europe/Prague'),
(392, 'BE', '+5050+00420', 'Europe/Brussels', '', '+01:00', '+02:00', ''),
(393, 'RO', '+4426+02606', 'Europe/Bucharest', '', '+02:00', '+03:00', ''),
(394, 'HU', '+4730+01905', 'Europe/Budapest', '', '+01:00', '+02:00', ''),
(395, 'MD', '+4700+02850', 'Europe/Chisinau', '', '+02:00', '+03:00', ''),
(396, 'DK', '+5540+01235', 'Europe/Copenhagen', '', '+01:00', '+02:00', ''),
(397, 'IE', '+5320-00615', 'Europe/Dublin', '', '+00:00', '+01:00', ''),
(398, 'GI', '+3608-00521', 'Europe/Gibraltar', '', '+01:00', '+02:00', ''),
(399, 'GG', '+4927-00232', 'Europe/Guernsey', '', '+00:00', '+01:00', 'Link to Europe/London'),
(400, 'FI', '+6010+02458', 'Europe/Helsinki', '', '+02:00', '+03:00', ''),
(401, 'IM', '+5409-00428', 'Europe/Isle_of_Man', '', '+00:00', '+01:00', 'Link to Europe/London'),
(402, 'TR', '+4101+02858', 'Europe/Istanbul', '', '+02:00', '+03:00', ''),
(403, 'JE', '+4912-00207', 'Europe/Jersey', '', '+00:00', '+01:00', 'Link to Europe/London'),
(404, 'RU', '+5443+02030', 'Europe/Kaliningrad', 'Moscow-01 - Kaliningrad', '+03:00', '+03:00', ''),
(405, 'UA', '+5026+03031', 'Europe/Kiev', 'most locations', '+02:00', '+03:00', ''),
(406, 'PT', '+3843-00908', 'Europe/Lisbon', 'mainland', '+00:00', '+01:00', ''),
(407, 'SI', '+4603+01431', 'Europe/Ljubljana', '', '+01:00', '+02:00', 'Link to Europe/Belgrade'),
(408, 'GB', '+513030-0000731', 'Europe/London', '', '+00:00', '+01:00', ''),
(409, 'LU', '+4936+00609', 'Europe/Luxembourg', '', '+01:00', '+02:00', ''),
(410, 'ES', '+4024-00341', 'Europe/Madrid', 'mainland', '+01:00', '+02:00', ''),
(411, 'MT', '+3554+01431', 'Europe/Malta', '', '+01:00', '+02:00', ''),
(412, 'AX', '+6006+01957', 'Europe/Mariehamn', '', '+02:00', '+03:00', 'Link to Europe/Helsinki'),
(413, 'BY', '+5354+02734', 'Europe/Minsk', '', '+03:00', '+03:00', ''),
(414, 'MC', '+4342+00723', 'Europe/Monaco', '', '+01:00', '+02:00', ''),
(415, 'RU', '+5545+03735', 'Europe/Moscow', 'Moscow+00 - west Russia', '+04:00', '+04:00', ''),
(416, '', '', 'Europe/Nicosia', '', '+02:00', '+03:00', 'Link to Asia/Nicosia'),
(417, 'NO', '+5955+01045', 'Europe/Oslo', '', '+01:00', '+02:00', ''),
(418, 'FR', '+4852+00220', 'Europe/Paris', '', '+01:00', '+02:00', ''),
(419, 'ME', '+4226+01916', 'Europe/Podgorica', '', '+01:00', '+02:00', 'Link to Europe/Belgrade'),
(420, 'CZ', '+5005+01426', 'Europe/Prague', '', '+01:00', '+02:00', ''),
(421, 'LV', '+5657+02406', 'Europe/Riga', '', '+02:00', '+03:00', ''),
(422, 'IT', '+4154+01229', 'Europe/Rome', '', '+01:00', '+02:00', ''),
(423, 'RU', '+5312+05009', 'Europe/Samara', 'Moscow+00 - Samara, Udmurtia', '+04:00', '+04:00', ''),
(424, 'SM', '+4355+01228', 'Europe/San_Marino', '', '+01:00', '+02:00', 'Link to Europe/Rome'),
(425, 'BA', '+4352+01825', 'Europe/Sarajevo', '', '+01:00', '+02:00', 'Link to Europe/Belgrade'),
(426, 'UA', '+4457+03406', 'Europe/Simferopol', 'central Crimea', '+02:00', '+03:00', ''),
(427, 'MK', '+4159+02126', 'Europe/Skopje', '', '+01:00', '+02:00', 'Link to Europe/Belgrade'),
(428, 'BG', '+4241+02319', 'Europe/Sofia', '', '+02:00', '+03:00', ''),
(429, 'SE', '+5920+01803', 'Europe/Stockholm', '', '+01:00', '+02:00', ''),
(430, 'EE', '+5925+02445', 'Europe/Tallinn', '', '+02:00', '+03:00', ''),
(431, 'AL', '+4120+01950', 'Europe/Tirane', '', '+01:00', '+02:00', ''),
(432, '', '', 'Europe/Tiraspol', '', '+02:00', '+03:00', 'Link to Europe/Chisinau'),
(433, 'UA', '+4837+02218', 'Europe/Uzhgorod', 'Ruthenia', '+02:00', '+03:00', ''),
(434, 'LI', '+4709+00931', 'Europe/Vaduz', '', '+01:00', '+02:00', ''),
(435, 'VA', '+415408+0122711', 'Europe/Vatican', '', '+01:00', '+02:00', 'Link to Europe/Rome'),
(436, 'AT', '+4813+01620', 'Europe/Vienna', '', '+01:00', '+02:00', ''),
(437, 'LT', '+5441+02519', 'Europe/Vilnius', '', '+02:00', '+03:00', ''),
(438, 'RU', '+4844+04425', 'Europe/Volgograd', 'Moscow+00 - Caspian Sea', '+04:00', '+04:00', ''),
(439, 'PL', '+5215+02100', 'Europe/Warsaw', '', '+01:00', '+02:00', ''),
(440, 'HR', '+4548+01558', 'Europe/Zagreb', '', '+01:00', '+02:00', 'Link to Europe/Belgrade'),
(441, 'UA', '+4750+03510', 'Europe/Zaporozhye', 'Zaporozh\'ye, E Lugansk / Zaporizhia, E Luhansk', '+02:00', '+03:00', ''),
(442, 'CH', '+4723+00832', 'Europe/Zurich', '', '+01:00', '+02:00', ''),
(443, '', '', 'GB', '', '+00:00', '+01:00', 'Link to Europe/London'),
(444, '', '', 'GB-Eire', '', '+00:00', '+01:00', 'Link to Europe/London'),
(445, '', '', 'GMT', '', '+00:00', '+00:00', 'Link to UTC'),
(446, '', '', 'GMT+0', '', '+00:00', '+00:00', 'Link to UTC'),
(447, '', '', 'GMT-0', '', '+00:00', '+00:00', 'Link to UTC'),
(448, '', '', 'GMT0', '', '+00:00', '+00:00', 'Link to UTC'),
(449, '', '', 'Greenwich', '', '+00:00', '+00:00', 'Link to UTC'),
(450, '', '', 'Hong Kong', '', '+08:00', '+08:00', 'Link to Asia/Hong_Kong'),
(451, '', '', 'HST', '', '−10:00', '−10:00', ''),
(452, '', '', 'Iceland', '', '+00:00', '+00:00', 'Link to Atlantic/Reykjavik'),
(453, 'MG', '-1855+04731', 'Indian/Antananarivo', '', '+03:00', '+03:00', ''),
(454, 'IO', '-0720+07225', 'Indian/Chagos', '', '+06:00', '+06:00', ''),
(455, 'CX', '-1025+10543', 'Indian/Christmas', '', '+07:00', '+07:00', ''),
(456, 'CC', '-1210+09655', 'Indian/Cocos', '', '+06:30', '+06:30', ''),
(457, 'KM', '-1141+04316', 'Indian/Comoro', '', '+03:00', '+03:00', ''),
(458, 'TF', '-492110+0701303', 'Indian/Kerguelen', '', '+05:00', '+05:00', ''),
(459, 'SC', '-0440+05528', 'Indian/Mahe', '', '+04:00', '+04:00', ''),
(460, 'MV', '+0410+07330', 'Indian/Maldives', '', '+05:00', '+05:00', ''),
(461, 'MU', '-2010+05730', 'Indian/Mauritius', '', '+04:00', '+04:00', ''),
(462, 'YT', '-1247+04514', 'Indian/Mayotte', '', '+03:00', '+03:00', ''),
(463, 'RE', '-2052+05528', 'Indian/Reunion', '', '+04:00', '+04:00', ''),
(464, '', '', 'Iran', '', '+03:30', '+04:30', 'Link to Asia/Tehran'),
(465, '', '', 'Israel', '', '+02:00', '+03:00', 'Link to Asia/Jerusalem'),
(466, '', '', 'Jamaica', '', '−05:00', '−05:00', 'Link to America/Jamaica'),
(467, '', '', 'Japan', '', '+09:00', '+09:00', 'Link to Asia/Tokyo'),
(468, '', '', 'JST-9', '', '+09:00', '+09:00', 'Link to Asia/Tokyo'),
(469, '', '', 'Kwajalein', '', '+12:00', '+12:00', 'Link to Pacific/Kwajalein'),
(470, '', '', 'Libya', '', '+02:00', '+02:00', 'Link to Africa/Tripoli'),
(471, '', '', 'MET', '', '+01:00', '+02:00', ''),
(472, '', '', 'Mexico/BajaNorte', '', '−08:00', '−07:00', 'Link to America/Tijuana'),
(473, '', '', 'Mexico/BajaSur', '', '−07:00', '−06:00', 'Link to America/Mazatlan'),
(474, '', '', 'Mexico/General', '', '−06:00', '−05:00', 'Link to America/Mexico_City'),
(475, '', '', 'MST', '', '−07:00', '−07:00', ''),
(476, '', '', 'MST7MDT', '', '−07:00', '−06:00', ''),
(477, '', '', 'Navajo', '', '−07:00', '−06:00', 'Link to America/Denver'),
(478, '', '', 'NZ', '', '+12:00', '+13:00', 'Link to Pacific/Auckland'),
(479, '', '', 'NZ-CHAT', '', '+12:45', '+13:45', 'Link to Pacific/Chatham'),
(480, 'WS', '-1350-17144', 'Pacific/Apia', '', '+13:00', '+14:00', ''),
(481, 'NZ', '-3652+17446', 'Pacific/Auckland', 'most locations', '+12:00', '+13:00', ''),
(482, 'NZ', '-4357-17633', 'Pacific/Chatham', 'Chatham Islands', '+12:45', '+13:45', ''),
(483, 'FM', '+0725+15147', 'Pacific/Chuuk', 'Chuuk (Truk) and Yap', '+10:00', '+10:00', ''),
(484, 'CL', '-2709-10926', 'Pacific/Easter', 'Easter Island & Sala y Gomez', '−06:00', '−05:00', ''),
(485, 'VU', '-1740+16825', 'Pacific/Efate', '', '+11:00', '+11:00', ''),
(486, 'KI', '-0308-17105', 'Pacific/Enderbury', 'Phoenix Islands', '+13:00', '+13:00', ''),
(487, 'TK', '-0922-17114', 'Pacific/Fakaofo', '', '+13:00', '+13:00', ''),
(488, 'FJ', '-1808+17825', 'Pacific/Fiji', '', '+12:00', '+13:00', ''),
(489, 'TV', '-0831+17913', 'Pacific/Funafuti', '', '+12:00', '+12:00', ''),
(490, 'EC', '-0054-08936', 'Pacific/Galapagos', 'Galapagos Islands', '−06:00', '−06:00', ''),
(491, 'PF', '-2308-13457', 'Pacific/Gambier', 'Gambier Islands', '−09:00', '−09:00', ''),
(492, 'SB', '-0932+16012', 'Pacific/Guadalcanal', '', '+11:00', '+11:00', ''),
(493, 'GU', '+1328+14445', 'Pacific/Guam', '', '+10:00', '+10:00', ''),
(494, 'US', '+211825-1575130', 'Pacific/Honolulu', 'Hawaii', '−10:00', '−10:00', ''),
(495, 'UM', '+1645-16931', 'Pacific/Johnston', 'Johnston Atoll', '−10:00', '−10:00', ''),
(496, 'KI', '+0152-15720', 'Pacific/Kiritimati', 'Line Islands', '+14:00', '+14:00', ''),
(497, 'FM', '+0519+16259', 'Pacific/Kosrae', 'Kosrae', '+11:00', '+11:00', ''),
(498, 'MH', '+0905+16720', 'Pacific/Kwajalein', 'Kwajalein', '+12:00', '+12:00', ''),
(499, 'MH', '+0709+17112', 'Pacific/Majuro', 'most locations', '+12:00', '+12:00', ''),
(500, 'PF', '-0900-13930', 'Pacific/Marquesas', 'Marquesas Islands', '−09:30', '−09:30', ''),
(501, 'UM', '+2813-17722', 'Pacific/Midway', 'Midway Islands', '−11:00', '−11:00', ''),
(502, 'NR', '-0031+16655', 'Pacific/Nauru', '', '+12:00', '+12:00', ''),
(503, 'NU', '-1901-16955', 'Pacific/Niue', '', '−11:00', '−11:00', ''),
(504, 'NF', '-2903+16758', 'Pacific/Norfolk', '', '+11:30', '+11:30', ''),
(505, 'NC', '-2216+16627', 'Pacific/Noumea', '', '+11:00', '+11:00', ''),
(506, 'AS', '-1416-17042', 'Pacific/Pago_Pago', '', '−11:00', '−11:00', ''),
(507, 'PW', '+0720+13429', 'Pacific/Palau', '', '+09:00', '+09:00', ''),
(508, 'PN', '-2504-13005', 'Pacific/Pitcairn', '', '−08:00', '−08:00', ''),
(509, 'FM', '+0658+15813', 'Pacific/Pohnpei', 'Pohnpei (Ponape)', '+11:00', '+11:00', ''),
(510, '', '', 'Pacific/Ponape', '', '+11:00', '+11:00', 'Link to Pacific/Pohnpei'),
(511, 'PG', '-0930+14710', 'Pacific/Port_Moresby', '', '+10:00', '+10:00', ''),
(512, 'CK', '-2114-15946', 'Pacific/Rarotonga', '', '−10:00', '−10:00', ''),
(513, 'MP', '+1512+14545', 'Pacific/Saipan', '', '+10:00', '+10:00', ''),
(514, '', '', 'Pacific/Samoa', '', '−11:00', '−11:00', 'Link to Pacific/Pago_Pago'),
(515, 'PF', '-1732-14934', 'Pacific/Tahiti', 'Society Islands', '−10:00', '−10:00', ''),
(516, 'KI', '+0125+17300', 'Pacific/Tarawa', 'Gilbert Islands', '+12:00', '+12:00', ''),
(517, 'TO', '-2110-17510', 'Pacific/Tongatapu', '', '+13:00', '+13:00', ''),
(518, '', '', 'Pacific/Truk', '', '+10:00', '+10:00', 'Link to Pacific/Chuuk'),
(519, 'UM', '+1917+16637', 'Pacific/Wake', 'Wake Island', '+12:00', '+12:00', ''),
(520, 'WF', '-1318-17610', 'Pacific/Wallis', '', '+12:00', '+12:00', ''),
(521, '', '', 'Pacific/Yap', '', '+10:00', '+10:00', 'Link to Pacific/Chuuk'),
(522, '', '', 'Poland', '', '+01:00', '+02:00', 'Link to Europe/Warsaw'),
(523, '', '', 'Portugal', '', '+00:00', '+01:00', 'Link to Europe/Lisbon'),
(524, '', '', 'PRC', '', '+08:00', '+08:00', 'Link to Asia/Shanghai'),
(525, '', '', 'PST8PDT', '', '−08:00', '−07:00', ''),
(526, '', '', 'ROC', '', '+08:00', '+08:00', 'Link to Asia/Taipei'),
(527, '', '', 'ROK', '', '+09:00', '+09:00', 'Link to Asia/Seoul'),
(528, '', '', 'Singapore', '', '+08:00', '+08:00', 'Link to Asia/Singapore'),
(529, '', '', 'Turkey', '', '+02:00', '+03:00', 'Link to Europe/Istanbul'),
(530, '', '', 'UCT', '', '+00:00', '+00:00', 'Link to UTC'),
(531, '', '', 'Universal', '', '+00:00', '+00:00', 'Link to UTC'),
(532, '', '', 'US/Alaska', '', '−09:00', '−08:00', 'Link to America/Anchorage'),
(533, '', '', 'US/Aleutian', '', '−10:00', '−09:00', 'Link to America/Adak'),
(534, '', '', 'US/Arizona', '', '−07:00', '−07:00', 'Link to America/Phoenix'),
(535, '', '', 'US/Central', '', '−06:00', '−05:00', 'Link to America/Chicago'),
(536, '', '', 'US/East-Indiana', '', '−05:00', '−04:00', 'Link to America/Indiana/Indianapolis'),
(537, '', '', 'US/Eastern', '', '−05:00', '−04:00', 'Link to America/New_York'),
(538, '', '', 'US/Hawaii', '', '−10:00', '−10:00', 'Link to Pacific/Honolulu'),
(539, '', '', 'US/Indiana-Starke', '', '−06:00', '−05:00', 'Link to America/Indiana/Knox'),
(540, '', '', 'US/Michigan', '', '−05:00', '−04:00', 'Link to America/Detroit'),
(541, '', '', 'US/Mountain', '', '−07:00', '−06:00', 'Link to America/Denver'),
(542, '', '', 'US/Pacific', '', '−08:00', '−07:00', 'Link to America/Los_Angeles'),
(543, '', '', 'US/Pacific-New', '', '−08:00', '−07:00', 'Link to America/Los_Angeles'),
(544, '', '', 'US/Samoa', '', '−11:00', '−11:00', 'Link to Pacific/Pago_Pago'),
(545, '', '', 'UTC', '', '+00:00', '+00:00', ''),
(546, '', '', 'W-SU', '', '+04:00', '+04:00', 'Link to Europe/Moscow'),
(547, '', '', 'WET', '', '+00:00', '+01:00', ''),
(548, '', '', 'Zulu', '', '+00:00', '+00:00', 'Link to UTC');

-- --------------------------------------------------------

--
-- Table structure for table `tropics`
--

CREATE TABLE `tropics` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `tropic_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `type` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1 = Know, 2 = Like',
  `status` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Active, N = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tropic_groups`
--

CREATE TABLE `tropic_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tropic_groups`
--

INSERT INTO `tropic_groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Sports', '2016-11-25 18:30:00', '2016-11-25 18:30:00'),
(2, 'Language', '2016-11-25 18:30:00', '2016-11-25 18:30:00'),
(6, 'Major', '2016-11-29 09:33:15', '2016-11-29 09:33:15'),
(7, 'Profession', '2016-11-29 09:34:20', '2016-11-29 09:34:20'),
(8, 'Movie', '2016-11-30 07:58:19', '2016-11-30 07:58:19'),
(9, 'Company', '2016-11-30 10:15:03', '2016-11-30 10:15:03'),
(10, 'Country', '2016-11-30 10:15:32', '2016-11-30 10:15:32'),
(13, 'KHDN', '2017-01-07 22:28:22', '2017-01-07 22:28:22');

-- --------------------------------------------------------

--
-- Table structure for table `universities`
--

CREATE TABLE `universities` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `universities`
--

INSERT INTO `universities` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Trường Đại học Bách khoa Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(2, 'Trường Đại học Công đoàn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(3, 'Trường Đại học Công nghệ', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(4, 'Trường Đại học Công nghệ Giao thông Vận tải', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(5, 'Trường Đại học Công nghệ Dệt may Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(6, 'Trường Đại học Công nghệ và Quản lý Hữu nghị', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(7, 'Trường Đại học Công nghiệp Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(8, 'Trường Đại học Công nghiệp Việt Hung', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(9, 'Trường Đại học Dược Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(10, 'Trường Đại học Đại Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(11, 'Trường Đại học Điện lực', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(12, 'Trường Đại học Đông Đô', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(13, 'Trường Đại học FPT', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(14, 'Trường Đại học Giao thông Vận tải', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(15, 'Trường Đại học Giáo dục', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(16, 'Trường Đại học Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(17, 'Trường Đại học Hòa Bình', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(18, 'Trường Đại học Khoa học Tự nhiên', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(19, 'Trường Đại học Khoa học và Công nghệ Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(20, 'Trường Đại học Khoa học Xã hội và Nhân văn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(21, 'Trường Đại học Kiến trúc Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(22, 'Trường Đại học Kinh doanh và Công nghệ Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(23, 'Trường Đại học Kinh tế', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(24, 'Trường Đại học Kinh tế - Kỹ thuật Công nghiệp', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(25, 'Trường Đại học Kinh tế Quốc dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(26, 'Trường Đại học Kỹ thuật Lê Quý Đôn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(27, 'Trường Đại học Lao động - Xã hội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(28, 'Trường Đại học Lâm nghiệp Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(29, 'Trường Đại học Luật Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(30, 'Trường Đại học Mỏ - Địa chất', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(31, 'Trường Đại học Mỹ thuật Công nghiệp Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(32, 'Trường Đại học Mỹ thuật Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(33, 'Trường Đại học Ngoại ngữ', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(34, 'Trường Đại học Ngoại thương', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(35, 'Trường Đại học Nguyễn Trãi', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(36, 'Trường Đại học Nội vụ Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(37, 'Trường Đại học Phương Đông', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(38, 'Trường Đại học Quốc tế Bắc Hà', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(39, 'Trường Đại học Răng Hàm Mặt', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(40, 'Trường Đại học Sân khấu - Điện ảnh Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(41, 'Trường Đại học Sư phạm Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(42, 'Trường Đại học Sư phạm Nghệ thuật Trung ương', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(43, 'Trường Đại học Sư phạm Thể dục Thể thao Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(44, 'Trường Đại học Tài chính Ngân hàng Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(45, 'Trường Đại học Tài nguyên và Môi trường Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(46, 'Trường Đại học Thành Đô', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(47, 'Trường Đại học Thành Tây', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(48, 'Trường Đại học Thăng Long', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(49, 'Trường Đại học Thủ đô Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(50, 'Trường Đại học Thủy lợi', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(51, 'Trường Đại học Thương mại', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(52, 'Trường Đại học Văn hóa Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(53, 'Trường Đại học Xây dựng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(54, 'Trường Đại học Y Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(55, 'Trường Đại học Y tế Công cộng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(56, 'Học viện Âm nhạc Quốc gia Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(57, 'Học viện Thiết kế và Thời trang London', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(58, 'Học viện Báo chí và Tuyên truyền', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(59, 'Học viện Biên phòng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(60, 'Học viện Công nghệ Thông tin Bách Khoa', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(61, 'Học viện Công nghệ Bưu chính Viễn thông', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(62, 'Học viện Chính trị', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(63, 'Học viện Chính trị - Hành chính Quốc gia Hồ Chí Minh', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(64, 'Học viện Hành chính', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(65, 'Học viện Kỹ thuật Mật mã', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(66, 'Học viện Ngân hàng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(67, 'Học viện Ngoại giao', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(68, 'Học viện Nông nghiệp Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(69, 'Học viện Phụ nữ Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(70, 'Học viện Quản lý Giáo dục', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(71, 'Học viện Tư pháp Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(72, 'Học viện Tài chính', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(73, 'Học viện Quân y', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(74, 'Học viện Y dược học cổ truyền Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(75, 'Học viện Chính sách và Phát triển', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(76, 'Học viện Thanh thiếu niên Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(77, 'Viện Đại học Mở Hà Nội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(78, 'Học viện An ninh Nhân dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(79, 'Học viện Cảnh sát Nhân dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(80, 'Học viện Phòng không - Không quân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(81, 'Học viện Hàng không Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(82, 'Học viện Hành chính cơ sở phía Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(83, 'Học viện Kỹ thuật Mật mã cơ sở phía Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(84, 'Học viện Kỹ thuật Quân sự cơ sở 2', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(85, 'Nhạc viện Thành phố Hồ Chí Minh', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(86, 'Trường ĐH An ninh Nhân dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(87, 'Trường ĐH Bách khoa', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(88, 'Trường ĐH Công nghiệp Thực phẩm TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(89, 'Trường ĐH Công nghiệp TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(90, 'Trường ĐH Công nghệ Sài Gòn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(91, 'Trường ĐH Công nghệ thông tin Gia Định', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(92, 'Trường ĐH Công nghệ Thông tin', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(93, 'Trường ĐH Công nghệ TP.HCM (HUTECH)', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(94, 'Trường ĐH Cảnh sát Nhân dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(95, 'Trường ĐH Dân lập Văn Lang', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(96, 'Trường ĐH FPT', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(97, 'Trường ĐH Giao thông Vận tải - cơ sở 2 phía Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(98, 'Trường ĐH Giao thông Vận tải TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(99, 'Trường ĐH Hoa Sen', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(100, 'Trường ĐH Hùng Vương', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(101, 'Trường ĐH Khoa học Tự nhiên', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(102, 'Trường ĐH Khoa học Xã hội và Nhân văn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(103, 'Trường ĐH Kinh tế - Luật', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(104, 'Trường ĐH Kinh tế - Tài chính TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(105, 'Trường ĐH Kinh tế TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(106, 'Trường ĐH Kiến trúc TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(107, 'Trường ĐH Lao động - Xã hội (cơ sở 2 TP.HCM)', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(108, 'Trường ĐH Luật TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(109, 'Trường ĐH Mở TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(110, 'Trường ĐH Mỹ thuật TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(111, 'Trường ĐH Ngoại ngữ - Tin học TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(112, 'Trường ĐH Ngoại thương cơ sỏ phía Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(113, 'Trường ĐH Nguyễn Tất Thành', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(114, 'Trường ĐH Ngân hàng TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(115, 'Trường ĐH Nông Lâm TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(116, 'Trường ĐH Quốc tế Hồng Bàng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(117, 'Trường ĐH Quốc tế RMIT Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(118, 'Trường ĐH Quốc tế', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(119, 'Trường ĐH Sài Gòn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(120, 'Trường ĐH Sân khấu', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(121, 'Trường ĐH Sư phạm Kỹ thuật TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(122, 'Trường ĐH Sư phạm Thể dục Thể thao TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(123, 'Trường ĐH Sư phạm TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(124, 'Trường ĐH Thể dục Thể thao TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(125, 'Trường ĐH Thủy lợi cơ sở 2', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(126, 'Trường ĐH Trần Đại Nghĩa', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(127, 'Trường ĐH Tài chính - Marketing', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(128, 'Trường ĐH Tài nguyên - Môi trường TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(129, 'Trường ĐH Tôn Đức Thắng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(130, 'Trường ĐH Tư thục Quốc tế Sài Gòn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(131, 'Trường ĐH Việt Đức', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(132, 'Trường ĐH Văn Hiến', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(133, 'Trường ĐH Văn hóa TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(134, 'Trường ĐH Y Dược TP.HCM', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(135, 'Trường ĐH Y khoa Phạm Ngọc Thạch', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(136, 'ĐH Quốc gia Thành phố Hồ Chí Minh', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(137, 'Trường Cao đẳng Phát thanh Truyền hình II', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(138, 'Trường Đại học Tây Bắc', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(139, 'Học viện Chính trị Quân sự', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(140, 'Học viện Hải quân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(141, 'Học viện Hậu cần', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(142, 'Học viện Khoa học Quân sự', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(143, 'Học viện Kỹ thuật Quân sự', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(144, 'Học viện Lục quân Đà Lạt', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(145, 'Học viện Quốc phòng Việt Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(146, 'Trường Đại học Ngô Quyền', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(147, 'Trường Sĩ quan Không quân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(148, 'Trường Đại học Chính trị', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(149, 'Trường Đại học Trần Đại Nghĩa', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(150, 'Trường Đại học Trần Quốc Tuấn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(151, 'Trường Đại học Nguyễn Huệ', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(152, 'Trường Đại học Thông tin liên lạc', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(153, 'Trường Sĩ quan Tăng-Thiết giáp', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(154, 'Trường Đại học Văn hóa - Nghệ thuật Quân đội', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(155, 'Trường Đại học An ninh Nhân dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(156, 'Trường Đại học Cảnh sát Nhân dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(157, 'Trường Đại học Phòng cháy Chữa cháy', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(158, 'Trường Đại học Kỹ thuật - Hậu cần Công an Nhân dân', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(159, 'Trường Cao đẳng An ninh Nhân dân I', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(160, 'Trường Cao đẳng An ninh Nhân dân II', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(161, 'Trường Cao đẳng Cảnh sát Nhân dân I', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(162, 'Trường Cao đẳng Cảnh sát Nhân dân II', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(163, 'Đại học Thái Nguyên', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(164, 'Trường Đại học Vinh', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(165, 'Đại học Huế', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(166, 'Đại học Đà Nẵng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(167, 'Trường Đại học Quy Nhơn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(168, 'Trường Đại học Tây Nguyên', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(169, 'Trường Đại học Cần Thơ', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(170, 'Trường Đại học An Giang', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(171, 'Trường Đại học Bạc Liêu', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(172, 'Trường Đại học Đồng Nai', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(173, 'Trường Đại học Hà Tĩnh', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(174, 'Trường Đại học Hạ Long', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(175, 'Trường Đại học Hải Dương', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(176, 'Trường Đại học Hải Phòng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(177, 'Trường Đại học Hồng Đức', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(178, 'Trường Đại học Hoa Lư', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(179, 'Trường Đại học Hùng Vương', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(180, 'Trường Đại học Khánh Hòa', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(181, 'Trường Đại học Kinh tế Nghệ An', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(182, 'Trường Đại học Kỹ thuật - Công nghệ Cần Thơ', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(183, 'Trường Đại học Phạm Văn Đồng', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(184, 'Trường Đại học Phú Yên', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(185, 'Trường Đại học Quảng Bình', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(186, 'Trường Đại học Quảng Nam', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(187, 'Trường Đại học Sài Gòn', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(188, 'Trường Đại học Thủ Dầu Một', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(189, 'Trường Đại học Tân Trào', '2017-01-19 16:57:24', '2017-01-19 16:57:24'),
(190, 'Trường Đại học Tiền Giang', '2017-01-19 16:57:24', '2017-01-19 16:57:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nickname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci,
  `user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `recovery_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 = Normal user, 2 = Client',
  `gender` enum('M','F') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'M' COMMENT '''M''=>''Male'', ''F''=>''Female''',
  `dob` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `profile_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vat_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `current_address` text COLLATE utf8_unicode_ci NOT NULL,
  `postal_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lng` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `confirmation_code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `utc_timezone` int(11) NOT NULL,
  `status` enum('Y','N','E') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'E' COMMENT 'Y = Active, N = Block, E = Pending Activation',
  `in_step` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `nickname`, `details`, `user_name`, `email`, `recovery_email`, `password`, `user_type`, `gender`, `dob`, `age`, `profile_image`, `company_name`, `company_number`, `vat_number`, `address`, `current_address`, `postal_code`, `lat`, `lng`, `confirmation_code`, `utc_timezone`, `status`, `in_step`, `remember_token`, `created_at`, `updated_at`) VALUES
(101, 'Nguyễn', ' Tuấn Quang', 'Nguyễn Tuấn Quang', NULL, NULL, 'nguyentuanquang@gmail.com', '', '$2y$10$mbTaTVWvMVFN/iUHoylOfegNxZ0BwJQO6.iMajQhkGc3yovFFbNbu', 1, 'M', '26-9-1989', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'FWf49WIlVzQd1a2kX1ZsDoTgyNqLHFcb3QHqt2jssibLYF2uc4w0Tu0MxQCt', '2017-01-26 08:52:18', '2017-01-26 09:04:15'),
(102, 'Bùi', ' MInh Trí', 'Bùi MInh Trí', NULL, NULL, 'bui.minh.tri.318@gmail.com', '', '$2y$10$HpBYJmkxIZZpDcwvp8XuYOkoLsecTGT0s1/3.sqiuFcUQY6GPUI16', 1, 'M', '31-8-1990', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'oUY85VGYFCvJb0JiLAodCfOloZXGfWbTwRMtVYYGAmU2qdxSSE35YS3Qrllr', '2017-01-26 09:05:27', '2017-01-26 09:20:02'),
(103, 'Đỗ', ' Thanh Tùng', 'Đỗ Thanh Tùng', NULL, NULL, 'tung0408@live.com', '', '$2y$10$cOdwyp8Jk3f3Y.0tcAyC2uKYt.I.vEFr0Vo6Tf60HWqCCzyjZNIee', 1, 'M', '4-8-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'kCiLqR4iuLdFzrL5Xrp7tfSrA0M9xIAytqGS982DuMkZYdojeNqYdUJwoN6D', '2017-01-26 09:22:08', '2017-01-26 09:33:01'),
(104, 'Ngô', ' Ngọc Diệp', 'Ngô Ngọc Diệp', NULL, NULL, 'diep.nn.2011@gmail.com', '', '$2y$10$eSyAfx9SVHajwnxXw8tJ9.5Xu/.nA2cmsyCLj6KeO447UUFrm8olW', 1, 'F', '9-1-1991', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'MNvydZCIz2hLgcN2e6HVvKo49Or4GNRLgfrcE2Ua7zxN1VQ2awFfFmACY0Ji', '2017-01-26 09:33:30', '2017-01-26 09:43:20'),
(105, 'Quản', ' Thị Hạnh Mai', 'Quản Thị Hạnh Mai', NULL, NULL, 'mai.qth2710@gmail.com', '', '$2y$10$crm1qbdqtjp93N8wrd/uMuKEa8rhQPrW0A/JdFvBG4ng.LomPp3XC', 1, 'F', '27-10-1990', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'W02iWMpqjK5Dk1oC49FrlGULiahzZvrDu28C1OIx7fliiGIMsQJ2k6xGdrFw', '2017-01-26 09:44:11', '2017-01-26 09:51:57'),
(106, 'Hailey', ' Pola', 'Hailey Pola', NULL, NULL, 'haileypola@navipi.com', '', '$2y$10$U9T5ihj3orMlQXx6kbtl0OTXUWaBkZKlP9ir39HI.Od68lpaanJhC', 1, 'F', '13-9-1991', 25, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'HMWpRS1ovDaJl9upX61gpXqwGtLxgM1tt8cH2MDkMgUxuxPwtdxQiEGHcXIo', '2017-01-26 09:55:19', '2017-01-26 10:02:50'),
(107, 'Hoàng', ' Ngân Hà', 'Hoàng Ngân Hà', NULL, NULL, 'hoangnganha@gmail.com', '', '$2y$10$O0.VqqGjpt2vutnCuOi1tehgVPCijDa0oXhRhnHCTPZo5ck.mHyVC', 1, 'F', '1-1-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'Pzm8J7cSWysaNTAaBde9nnnprx3z1EJoV1fO1LrPvaWNIzE4D5j7Re63cV4G', '2017-01-27 07:33:24', '2017-01-27 08:24:40'),
(108, 'Trần', ' Ngọc Lân', 'Trần Ngọc Lân', NULL, NULL, 'lantran.hn@gmail.com', '', '$2y$10$Yr7pJgx5Voe1sG6JgA8jwO/XDm/TP8bdLIvBj8khaw9OxeCjVKEJO', 1, 'M', '1-1-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'dPOHT58Tuu0Ansi43TsL3MowxgLeW15xRV9V9eECBNuaExSa746AFeaE5XSZ', '2017-01-27 08:30:25', '2017-01-27 08:42:14'),
(109, 'Đặng', ' Thanh Trúc', 'Đặng Thanh Trúc', NULL, NULL, 'thanhtrucdang1990@gmail.com', '', '$2y$10$JY6g6YiT9N.AqhSw/v8tK.H5IHaYHMnsZ5oE3fVv9pgkUwMxeUaKC', 1, 'F', '10-5-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'bTmVcsM9WEadoD4bmh4F95ZqdocTkgUpyXVM3Ca5bN3wXt2TWf3R5mJ5lOPC', '2017-01-27 08:43:08', '2017-01-27 08:52:51'),
(110, 'Phạm', ' Quỳnh Anh', 'Phạm Quỳnh Anh', NULL, NULL, 'phamquynhanh0215@gmail.com', '', '$2y$10$L/pHQxYthEKcazk4xgo6ceEQzoXF2Y7qT6eAZkiMpZhx6/T7s/bgq', 1, 'F', '1-1-1991', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, NULL, '2017-01-27 08:54:44', '2017-01-27 09:10:01'),
(111, 'Nguyễn', ' Quang Huy', 'Nguyễn Quang Huy', NULL, NULL, 'nguyenquanghuy@gmail.com', '', '$2y$10$B9cz3N87XDvgH6VjCMsA/uYYkxKj0Kj3us2SXU8pBxChw5G5Law46', 1, 'M', '17-6-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'TSXYk1U9GhZOeWUy1Y8t3eL48Od1fXtxCxDlq8zT4hFdpPc6kclZl7p46IMZ', '2017-01-27 20:03:10', '2017-01-27 20:16:11'),
(112, 'Đào', ' Cẩm Thủy', 'Đào Cẩm Thủy', NULL, NULL, 'daocamthuy@gmail.com', '', '$2y$10$cL1XWksFITZm3ULNZVFlL.VPyR.GRQvEUUwpl/Xx/moiXkV2tzkfG', 1, 'F', '16-6-1987', 30, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'hwgPPpBbXUaLdDwO6hvGoDzUkHTJUDziZWv6vNNc763Dz1PLRvdDG9AlvtSS', '2017-01-27 20:18:20', '2017-01-27 20:30:08'),
(113, 'Nguyễn', ' Ngọc Hương', 'Nguyễn Ngọc Hương', NULL, NULL, 'nguyenngochuong@gmail.com', '', '$2y$10$VSNj7J6pVD6Hu3ecrMFm.uapdlOYpw8XF4wzw/1KHwwXddwSqldha', 1, 'F', '25-12-1989', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'XidmeMBQFYpSqNoWExtjMyvKL9VuSv2dPLQZnBLoQS6GOHOk8sRP24nyoFRZ', '2017-01-27 20:30:37', '2017-01-27 20:48:43'),
(114, 'Nguyễn', ' Vân Trang', 'Nguyễn Vân Trang', NULL, NULL, 'vantrang118@gmail.com', '', '$2y$10$9GcvqykPiwO782CovH1lp.k/cxSxNG2cAMIPywjCtVDsb4BEvPTJe', 1, 'F', '8-11-1992', 24, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'b9TvcKqqXRfNA0WGvmYq0yqbL6DAKDv1sDdxe7OXF9fp28uOLyxjK0amCMz2', '2017-01-27 20:57:05', '2017-01-27 21:06:21'),
(115, 'Phạm', ' Thị Trúc Hà', 'Phạm Thị Trúc Hà', NULL, NULL, 'pttrucha@vietinbank.vn', '', '$2y$10$rac2E8.vPhRStGZ2D8NwceSTILaMytnmU0.2QfUrVjhjipgTB5X4G', 1, 'F', '11-8-1987', 29, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'bfHiByNpzZ6TIaUgswAwBONyoDF7hndqLPkd2j9WcjCAQTFAUtoEv26XUBHZ', '2017-01-27 21:53:14', '2017-01-27 22:07:54'),
(116, 'Trần', ' Hải Thắng', 'Trần Hải Thắng', NULL, NULL, 'tranhaithang@gmail.com', '', '$2y$10$u/JmD000QI5ez4VioxvWNea6jEeMMIVhirGrrF6CJvibn7WwA8NRq', 1, 'M', '5-8-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, '6bidK9Y4OcX184D3etEFu5YJSdLFnaLP99vYfFU1rN6cHZBkgCYFhSUWNj71', '2017-01-27 22:09:37', '2017-01-27 22:15:49'),
(117, 'Nguyễn', ' Linh Chi', 'Nguyễn Linh Chi', NULL, NULL, 'nguyenlinhchi@gmail.com', '', '$2y$10$WqVMXKEXewwcBNKNsuX46.0ZUlwG6afbAFNKzmknM/g8YOiJRPNRa', 1, 'F', '1-1-1991', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'OKtDsSKfSRqHCiASiqOiS8Jn0DbsacMJdSaBffixIPWmG4vw5mtvQiRxejIi', '2017-01-27 22:16:33', '2017-01-27 22:23:26'),
(118, 'Nguyễn', ' Diệu Linh', 'Nguyễn Diệu Linh', NULL, NULL, 'oc.sen.1411@gmail.com', '', '$2y$10$3wzPSJBXcTEo37WhwcBzH.UCZWn8u5rSC40j4ohcugyTFDXoohHBW', 1, 'F', '14-11-1990', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, NULL, '2017-01-27 22:23:55', '2017-01-27 22:35:06'),
(119, 'Đỗ', ' Đức Thịnh', 'Đỗ Đức Thịnh', NULL, NULL, 'dd.thinh@vietinbank.vn', '', '$2y$10$Ku8Z5ivjhMKOM5ixCrzK4eDGd9gKx1fhEc3EQHcaUCAK4MZpTDUuO', 1, 'M', '27-2-1987', 30, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'OTujUNugFzFnos1LQsvrU5M4UPznqMjcGgaoSAqA5XuO7KKiq5X3tJvK6sfm', '2017-01-28 02:54:48', '2017-01-28 03:00:11'),
(120, 'Nguyễn', ' Khương Duy', 'Nguyễn Khương Duy', NULL, NULL, 'duynguyenkhuong@gmail.com', '', '$2y$10$mg4SFmEA5mxnB/dYzrOf0eSqpfWhVUCVSAqIeNE756xK74U1nuNve', 1, 'M', '1-1-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'whm5j8ra2yjRdrtDjeSOXg45Vc0pSCACgjq2zzNQP9wLZlfZI2mNrWalDV76', '2017-01-28 03:02:31', '2017-01-28 03:11:52'),
(121, 'Nguyễn', ' Thị Thu Hà', 'Nguyễn Thị Thu Hà', NULL, NULL, 'hanguyen@gmail.com', '', '$2y$10$AsfNuJF5N7jFMX9ZGZ2qaO7pn2UXGFNCIFmmEhJ/MZtLi6o8vLarW', 1, 'F', '1-1-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'vrjvMG8j5h7Sf4RJV86eUXbvuJ6v4hG9TMuU7XraB2tvKkiq1lMH0iDP2bnj', '2017-01-28 03:12:56', '2017-01-28 03:31:03'),
(122, 'Nguyễn', ' Thanh Tùng', 'Nguyễn Thanh Tùng', NULL, NULL, 'thanhtung281290@gmail.com', '', '$2y$10$k6xQotb9fyil45Fb/6StY.XuHxAa8UBl6sESsikUGJYAlxmu1hSuG', 1, 'M', '28-12-1990', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'WJKRwc4a5IwIXRJMTnzWKKIOjjZZA1tR5jt9fX4eYKOV9DvVnVxpvFrlJHsE', '2017-01-28 03:32:52', '2017-01-28 07:13:26'),
(123, 'Đào', ' Lê Trang Anh', 'Đào Lê Trang Anh', NULL, NULL, 'daoletranganh@gmail.com', '', '$2y$10$qcsbwxwfqn7KnawgROlO3.D55iA0lnBFGl9VcXjOgHI66BG88UA8m', 1, 'F', '24-11-1990', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'dyaKlAHyQCFxpGItobNapDAr4cjcn0xnIaXjqH6WYjmMEXTyd7BFNdaBF3z1', '2017-01-28 03:39:44', '2017-01-28 06:52:38'),
(124, 'Trần', ' Quỳnh Anh', 'Trần Quỳnh Anh', NULL, NULL, 'tranquynhanh@gmail.com', '', '$2y$10$k6GvGsjx76YDV..1AVApOuYw8reMdGGn1Hjr3m5gTEHhgbrSauOxC', 1, 'F', '29-5-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'p0wUnYmB5KgFjuXdv4Usqfdz3yL3dwop0btbP1psL1yZoZ4JJWn0UxfNxfxi', '2017-01-28 06:55:03', '2017-01-28 07:11:26'),
(125, 'Đàm', ' Thị Thu Quỳnh', 'Đàm Thị Thu Quỳnh', NULL, NULL, 'damthithuquynh@gmail.com', '', '$2y$10$XcQVQk1LuCpEa4Kkuwx2CuEjnR4jc0o8Dn18QzgnwKg201Tq69LzO', 1, 'F', '28-7-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'UtOhrj38rmgVyw3hptMGK3X9PiUNNgqZnLMmggVyw35T39ni4pHNfTd1v4VL', '2017-01-28 08:12:01', '2017-01-28 08:20:12'),
(126, 'Trần', ' Thu Huyền', 'Trần Thu Huyền', NULL, NULL, 'huyentran@gmail.com', '', '$2y$10$pRV437xd0yiV03b95qFqg.DQBuzwLQZvzOQnAG.ARhHgIm9gynMGy', 1, 'F', '20-2-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'RPsfdZYXgQW9jwzw4ljhEIepwtdsISIJxpCqvL7m2x5RCRaHf3QjkEXQgsKy', '2017-01-28 08:33:17', '2017-01-28 08:37:34'),
(127, 'Nguyễn', ' Huy Hoàng', 'Nguyễn Huy Hoàng', NULL, NULL, 'hoangnguyen@gmail.com', '', '$2y$10$MLZoSnlpjRBIuwvACdlWSud4W5MKsCKZw1vI/sQdpbcdJwKxe/IlK', 1, 'M', '9-2-1991', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, '6TsOD10y5MnklEnT0mezKV8gVZW0Jy2z7zXJv8rdKvcOB1Gz1tcVGNfZQ0dz', '2017-01-28 08:38:58', '2017-01-29 02:24:08'),
(128, 'Đặng', ' Huyền Anh', 'Đặng Huyền Anh', NULL, NULL, 'mitdac25@gmail.com', '', '$2y$10$VJ.3BMOHelh68mjQTs3RGOBPeLKh6tfA3hRJHrbpENJ.lMhQXVVFe', 1, 'F', '1-1-1991', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'OD0d3QzVO4MP58l1dEWtntJnNPxV5w7jaIRSbb0pYJuDULhbJmaAaZ7aFFLT', '2017-01-28 08:47:50', '2017-01-28 08:55:27'),
(129, 'Nguyễn', ' Thu Hằng', 'Nguyễn Thu Hằng', NULL, NULL, 'nguyenthuhang@gmail.com', '', '$2y$10$y1LRSbZhGd4gQ2Rh08hU9eXmmP0MC5to/28gfuA.hUGxkZbfx44Wm', 1, 'F', '1-1-1991', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'y4Ul6HZv63H320yBWHlxtsFJvouP37NimgxAlXF8wPA16e8KdCPY8rTDmGXx', '2017-01-28 08:55:49', '2017-01-28 09:03:56'),
(130, 'Lưu', ' Minh Phương', 'Lưu Minh Phương', NULL, NULL, 'phuonglm@vietinbank.vn', '', '$2y$10$qPAl50BE.9mamgjC.hvdZ.jpKSPM83yEdHflg2r4iTK0kxYF37rRW', 1, 'F', '1-8-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'WQNJQIOxN4QjJpT9KE8dfdILecTQ49wVfiTN6Agf39T3ORHMITGD4i4NhpWg', '2017-01-28 09:05:16', '2017-01-28 09:14:14'),
(131, 'Đỗ', ' Mạnh Thắng', 'Đỗ Mạnh Thắng', NULL, NULL, 'thang.dm@vietinbank.vn', '', '$2y$10$u2l7AlivYVk9bxb6oouk.Opv7e7J8cdlQvuuF1T34mhVhtY1ZlIVa', 1, 'M', '2-1-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'PXkQEXvfTcia3Hr97izX4EHHwGJRc8mzdUwGdhnTCnza4FoAJRbM8FhRYzum', '2017-01-28 09:17:19', '2017-01-28 09:23:59'),
(132, 'Nguyễn', ' Huyền Trang', 'Nguyễn Huyền Trang', NULL, NULL, 'nguyen.huyentrang@vietinbank.vn', '', '$2y$10$Tv1E5LLPPJ1fRrLHvTenju3l93QOhoGvgfQQUYosXETdQ1JCduvkm', 1, 'F', '15-3-1982', 35, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, '48anMpeQAEWuGLOkwpBGfo7LorqNJYPoMm18lcnkQt9cMOvHUcZW0LYjRwBE', '2017-01-28 09:26:14', '2017-01-28 09:36:14'),
(133, 'Tạ', ' Hoàng Linh', 'Tạ Hoàng Linh', NULL, NULL, 'tahoanglinh@gmail.com', '', '$2y$10$47sbhOm573cC6NLn3M6BVeRJaneIeOu8AEhKK2M1qB3U5n/5e/t2i', 1, 'M', '1-1-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'GlJlcCu0B41fgioowmBu9yZ5SUbGK6QZNvICi4H8W6KSpKzf2UUSGioa3LLG', '2017-01-28 19:58:52', '2017-01-28 20:08:05'),
(134, 'Lê', ' Thu Nga', 'Lê Thu Nga', NULL, NULL, 'lethunga@gmail.com', '', '$2y$10$1Ke9wFAW9WxAALWru2Lt8.POUIXQW8MahRY5YhG2cxGHt.2S8Sehy', 1, 'F', '17-6-1992', 25, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'VMfnraLVX4YTYK8Rtcp53twigNqzZk2wk17E9maHY87QvKkj1Y9rt6RtPOrm', '2017-01-28 20:09:40', '2017-01-28 20:20:15'),
(135, 'Nguyễn', ' Duy Quân', 'Nguyễn Duy Quân', NULL, NULL, 'quan.nd@vietinbank.vn', '', '$2y$10$Yka9g9kSe4A.HTShsKnlEuFD3zIzIR2soOL4pWMgccPh6irEu3YRi', 1, 'M', '1-1-1986', 31, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 's9EZ6FFUPcIk2bwKVwe58ql09HWzSb4mbOESJ740drGU0tDKsETu704ucaVH', '2017-01-28 20:22:27', '2017-01-28 20:29:28'),
(136, 'Phạm', ' Việt Hùng', 'Phạm Việt Hùng', NULL, NULL, 'hungpv4@vietinbank.vn', '', '$2y$10$gPhZ9QByWtutwN9o7batIumoj9qPE9zjH/LOaE2KxH0DPKTeqcG4e', 1, 'M', '27-5-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'Gzf11dDP3ZdRrb8mw8y48To0sHyEskFxt7Y53gZf0Wdd2KjryFPC5kzbQxXy', '2017-01-28 20:31:03', '2017-01-28 20:41:30'),
(137, 'Nguyễn', ' Susan', 'Nguyễn Susan', NULL, NULL, 'susannguyen@gmail.com', '', '$2y$10$pUvkgfvml4GQa/weXLtiHuCKqRbd1XEz9mfo0s4RDKpwlnSeokfYS', 1, 'F', '21-9-1990', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'Cq2svOTyPINNgIwkuxCAaADSulZE0kyU45uUf3bPDv1PNra8jxB0PdBxwoRN', '2017-01-28 20:58:01', '2017-01-28 21:10:03'),
(138, 'Nguyễn', ' Đức Khang', 'Nguyễn Đức Khang', NULL, NULL, 'khangnguyen@gmail.com', '', '$2y$10$OB/hz7RjmXlVNZ0I38eK/OETscG2Ol7coRGVJxESoJNa04L4cgQoO', 1, 'M', '31-5-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, '3zHKJwOwks0wYSB49dq98nsAknBwY4wYZS7vlX3j2L3IXF5h0oFefN8fj5mm', '2017-01-28 21:11:06', '2017-01-28 21:45:22'),
(139, 'Phạm', ' Hải', 'Phạm Hải', NULL, NULL, 'khiconbattu@gmail.com', '', '$2y$10$0urcgepmfD2Fwao5fhV.IOqrfD66XwM7eU/liyhQ9JPLOG1d5J4vi', 1, 'M', '28-10-1990', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, '0XcuKa4q51v4aORCrJGBJ8rmGPCFFEeaBLHe6VS7n8oyQRpqmGMg52OSXwr7', '2017-01-28 21:47:07', '2017-01-28 21:53:16'),
(140, 'Nguyễn', ' Sơn Hải', 'Nguyễn Sơn Hải', NULL, NULL, 'nguyensonhai@gmail.com', '', '$2y$10$llgcWnCw2DE5CCB7OxAWEu7CVaFOwrEMQdSw.XSDWoqybaziJUzHS', 1, 'M', '25-9-1986', 30, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'toPtKMs2o1QEZcS9E0gBABcBf7OzRisLRcUMndd6CAJ1vvhDZmklucTetSz0', '2017-01-28 21:54:47', '2017-01-29 02:11:39'),
(141, 'Đào', ' Quỳnh Anh', 'Đào Quỳnh Anh', NULL, NULL, 'quynhanh248@gmail.com', '', '$2y$10$EK9q7lBysC0FALbyaVEotuPrr17SJzNQYv7.sZZXtO4KaD.0lOcRO', 1, 'F', '24-8-1993', 23, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'qVcJpFB0armaZYG9BHcK0VlT3uBCmix402uNS55PLSJElWr236JyFT1PXK6h', '2017-01-29 02:24:53', '2017-01-29 02:31:33'),
(142, 'Bùi', ' Đình Nhật', 'Bùi Đình Nhật', NULL, NULL, 'nhatbui@gmail.com', '', '$2y$10$JJZfP24qKqbGp6bg.bQzfOI1TprWM42JAX/4P4a.HfpacjMSOjpYS', 1, 'M', '24-7-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'f77v6A8tG9KFDaI0H21Y6VGlIkCHOn6WLxjzAJteQO4zXH0dxGE6ZhSM8tVn', '2017-01-29 02:33:28', '2017-01-29 03:48:16'),
(143, 'Đinh', ' Anh Tuấn', 'Đinh Anh Tuấn', NULL, NULL, 'tuanda1211@gmail.com', '', '$2y$10$mdWrvsygr34vLWNAQkFa8ORXFY/YaH6NBvo4JEmZFgr1Kv5GEzrY.', 1, 'M', '12-11-1990', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, '528DhBzBZpUGDmtR2bfSr03y96shFkJ53Y279I7QFCFehb6qyoZ9f0KfykpP', '2017-01-29 03:52:00', '2017-01-29 04:07:36'),
(144, 'Vũ', ' Thanh Tùng', 'Vũ Thanh Tùng', NULL, NULL, 'tungvu@gmail.com', '', '$2y$10$w.DjcAYozl0aB1vb8u360usdTeSry2EZ/upgcAxa54Xnpr6MzC9fC', 1, 'M', '15-4-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'yykqLzLuiHvTl5OrbuIeTlsqVBAGv8wDW8yvkUFSahhOoHdB2ucVT1Q8d9rX', '2017-01-29 04:08:57', '2017-01-29 04:16:01'),
(145, 'Lê', ' Hồng Phúc', 'Lê Hồng Phúc', NULL, NULL, 'kimseryl@gmail.com', '', '$2y$10$sMceVfzegLn09eoeUe1V9uuV4jzdnoQx11wpSJHQntNJBPr3YVsTu', 1, 'F', '26-1-1991', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, '26Y9SdxPOeXBBl4eeCpQCjuzDPRzoYzuh0eFZews7jEV7AsJpkH4tm705k6v', '2017-01-29 06:21:01', '2017-01-29 06:29:14'),
(146, 'Mai', ' Thị Phương Thảo', 'Mai Thị Phương Thảo', NULL, NULL, 'maithao.ba@gmail.com', '', '$2y$10$b2l7FEF9awKBsVmyWvp1ZeKi2HGh3U2TbjKQdmckBYorgKYNIbf4S', 1, 'F', '3-7-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'ZJgJoE329i31HXRNjH6w5Q9NTTEFKoBbleQKyzzWga5vYWENcMk9s2Lm9Fur', '2017-01-29 06:30:20', '2017-01-29 06:33:34'),
(147, 'Vũ', ' Thị Minh Oanh', 'Vũ Thị Minh Oanh', NULL, NULL, 'oanh.vtm@vietinbank.vn', '', '$2y$10$wNdPgvN9TH.eu5.Cnw7wGenrDOXyb7vIaAr20uYD5/ZA4BWaSb4Du', 1, 'F', '31-8-1990', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'DjA9HY0MUJnrzBOcDbWpN2T5jr1y7kEZIw0Ylyfwj1fuYYByYPidMTRu5QDR', '2017-01-29 06:34:46', '2017-01-29 06:43:57'),
(148, 'Coen', ' Bakker', 'Coen Bakker', NULL, NULL, 'coenbakker@gmail.com', '', '$2y$10$TkBUKYCSPFKMuTqi38UicuhR7gQjteJCzXw50nJ1PqSbNwe0a1jH2', 1, 'M', '14-4-1988', 29, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'CqnfDyf20N9CpvPBmyzr6qAaPy0kKMiIgOgAK5vVggQbR6Rvd1cRAoRNaDkM', '2017-01-29 06:44:21', '2017-01-29 07:12:04'),
(149, 'Vũ', ' Đức Thuận', 'Vũ Đức Thuận', NULL, NULL, 'vuthuan90@gmail.com', '', '$2y$10$pb7vWvr6/AgyGzlw.v6ua.hyLF6fZ/.3fo3HrjXVmy.VFDAGMB5Xm', 1, 'M', '1-2-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, '15qraYmDsWXnoV4BzTsM6L75zShK7iGNExd4BARBt8zBap6Q1ucaTzLFIXz4', '2017-01-29 07:13:29', '2017-01-29 07:23:27'),
(150, 'Bùi', ' Phương Thảo', 'Bùi Phương Thảo', NULL, NULL, 'elena.phuong@gmail.com', '', '$2y$10$kapB1QZ9iOM.unZmSVqnm.veYwaTaJczFm1NjUaRS3QaVRsjNyueW', 1, 'F', '21-12-1990', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'rlANxi4SrkxlBswEJlyIwCymVQQ926mkjEL6BP7U5u50X79uKgNnK1Nc8unf', '2017-01-29 07:26:25', '2017-01-29 07:31:19'),
(151, 'Vũ', ' Hương Giang', 'Vũ Hương Giang', NULL, NULL, 'giangvh1@vietinbank.vn', '', '$2y$10$QCoDxzzAGqBt9EA/QFbzI.JjaJabGcI8bdGwCLlZ7yb9yPqszWaxu', 1, 'F', '28-8-1990', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'dvutyk5Hz1yUN1i4nhszc8xZeEaG3JspR0VOAMBCeNhSalvrikCWbboSmtZS', '2017-01-29 07:34:38', '2017-01-29 07:43:41'),
(152, 'David', ' Gillham', 'David Gillham', NULL, NULL, 'davidgillham@gmail.com', '', '$2y$10$NymvrKTbN9zpwjiLZJf3FeYAyyHW1DZmwgnslf24aiQcLllzOYB9W', 1, 'M', '24-2-1991', 26, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'bkQAUsUnapmQDmI6rIB0A6f8rsjDb4QtS0nhyetJnjIR6Je256Yt9yeMMpRj', '2017-01-29 07:44:27', '2017-01-29 07:53:54'),
(153, 'Lauren', ' Hawkins', 'Lauren Hawkins', NULL, NULL, 'lawrenhawkins@gmail.com', '', '$2y$10$KnedoqoRBWv2lKkn10PEN.ZY8KiRysszVFiaOkdvyz7N.IZK6/B7m', 1, 'F', '16-10-1995', 21, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'Rqrml12sf7wJApHew73DIjxgoZLXA2Y5a0iOOqVDrL4ZbZSMTW3FpCMXXYOi', '2017-01-29 07:57:43', '2017-01-29 08:15:11'),
(154, 'Đào', ' Cẩm Thủy', 'Đào Cẩm Thủy', NULL, NULL, 'thuydc@vietinbank.vn', '', '$2y$10$LePaWVdxhlsVJ1JjM.76QeDhSy2yEeHUG5Tj6qTP5CvRIrkPGiV/O', 1, 'F', '16-6-1987', 30, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'BTi8dxXPhSyGne7JO89kJvCD735gZ0Ghvag50MLdZQ5zF23teexAxgI1tAtw', '2017-01-29 08:16:57', '2017-01-29 08:51:30'),
(155, 'Jinda', ' Thepphalid', 'Jinda Thepphalid', NULL, NULL, 'jindathepphalid@gmail.com', '', '$2y$10$l9uUSvVLLNgDO95AC6ZrN.zh40z0B.5xWMTnuV2S7P9WO2Wpe.36q', 1, 'F', '9-8-1993', 23, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'nd5gzc37hUQq1XqjztQNYQ3BSPEWkZbwR3XkcMePun5unFR4e4oaTB9hE36H', '2017-01-29 08:55:17', '2017-01-29 09:01:12'),
(156, 'Phạm', ' Hà Trang', 'Phạm Hà Trang', NULL, NULL, 'ph.trang@vietinbank.vn', '', '$2y$10$4M/XLH3ui8kmir.TcyBOAuH4vTkwqKFuHyZ9LjDR4PiHVzU8yiVE.', 1, 'F', '25-5-1989', 28, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, '5ZpkL0xvO2pAl5zn9K9Xp6a4QZWIZt1EGaYOHlcJQ6NOUIDuybirpfxcjMTn', '2017-01-29 09:02:18', '2017-01-29 09:09:01'),
(157, 'Phạm', ' Hồng Tiến', 'Phạm Hồng Tiến', NULL, NULL, 'tienhp11690@gmail.com', '', '$2y$10$2Cn0nUBTECuWM7qZb9tFfu2RHtCAdtSbosk787100BbTGab7xb7sC', 1, 'M', '11-6-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'HipPj7WE76rY1pqW8u0Pc5OTl74PLE4xUH28AHrZdMKBhETUSF4L0lUakgLT', '2017-01-29 09:10:07', '2017-02-07 21:18:20'),
(158, 'Nguyễn', ' Hồng Tân', 'Nguyễn Hồng Tân', NULL, NULL, 'tannguyen@gmail.com', '', '$2y$10$IhK7QvHA.2jv7z4/Na/6bOr9azdVsXqZ35ugEwg.w5KiynV4gam3C', 1, 'M', '22-7-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'Bte5ZIBRCt6eU1MzVDTr1cuCdOhcPcFCmBGGKaxYMSREKHLWZvNNVzMc17xc', '2017-01-29 19:23:37', '2017-01-29 19:41:56'),
(159, 'Bùi', ' Xuân Trường', 'Bùi Xuân Trường', NULL, NULL, 'truongbx@gmail.com', '', '$2y$10$xzA9.Gl3YnBfB33bOIjykuxi22nTRwvhTtJ5QtsA42xUsBBg./G5m', 1, 'M', '1-1-1972', 45, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'Yi5GDwNjNTcxeVSrIaDfJJRPjEC8ra2U2aykfMBrowvVNWnh965hyyFD4ATe', '2017-01-29 19:45:18', '2017-01-29 20:10:05'),
(160, 'Đặng', ' Vương Anh', 'Đặng Vương Anh', NULL, NULL, 'dangvuonganh@gmail.com', '', '$2y$10$FWdlepHmver4k70fr287CekHIrcViRcKaAT.xp5e2HJM82dWLI81e', 1, 'M', '1-1-1988', 29, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'rznPGbqXrPsJbBNRjmSVYcvZlbANE5R1TQAbfyMdA77I4fzwOLyCFR9FMVds', '2017-01-29 20:13:18', '2017-01-29 20:20:09'),
(161, 'Ngô', ' Duy', 'Ngô Duy', NULL, NULL, 'duyngo@gmail.com', '', '$2y$10$U27L6pw.UaMHo1CsFxQyX.5KPajOrnajCoHEHWE7ri4lXgIeQeS2O', 1, 'M', '1-1-1989', 28, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'X456FM53bxh1YT16UAPPIDheHboUew3WEDPiMoTI209C6iaH2C9msvnkp8MR', '2017-01-29 20:21:38', '2017-01-29 20:33:29'),
(162, 'Nguyễn', ' Lê Vân Quỳnh', 'Nguyễn Lê Vân Quỳnh', NULL, NULL, 'quynhnlv@gmail.com', '', '$2y$10$sG/CRuGbQ1cpLO3u8j0OTuHhOkeZgm.Dh7QakxCUumm0m8l0DHdFW', 1, 'F', '1-1-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'lwsnFp3Kf0MGQNrVSg1h1VznIHhxpVRuATXITTTzAB52IuQzBg6LzJY6bTFg', '2017-01-29 20:35:24', '2017-01-29 20:44:03'),
(163, 'Phạm', ' Hồng Hạnh', 'Phạm Hồng Hạnh', NULL, NULL, 'bugpham@gmail.com', '', '$2y$10$50cRjQ73l3ufDYgydSsDq.Hg/b7nM3v8KxbbiUiwXCkzkdtYMS3ly', 1, 'F', '1-1-1990', 27, '', '', '', '', '', '', '', '', '', '', 0, 'Y', 5, 'dLfm9XjSRpN7LDQHhrtDgJtyxPsTcqQTsRNXB1QzA34cnip5yH9tYzaRpjyV', '2017-01-29 20:45:38', '2017-01-29 20:50:15');

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `type` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1 = address1, 2 = address2 ',
  `is_default` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' COMMENT '''Y''=>''Yes'', ''N''=>''No''',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`id`, `user_id`, `address`, `type`, `is_default`, `created_at`, `updated_at`) VALUES
(311, 101, 'Houston, TX, United States', 1, 'Y', '2017-01-26 08:52:57', '2017-01-26 08:52:57'),
(312, 102, 'Hà Nội, Việt Nam', 1, 'Y', '2017-01-26 09:07:41', '2017-01-26 09:07:41'),
(313, 102, 'Utica, Tiểu bang New York, Hoa Kỳ', 1, 'N', '2017-01-26 09:07:56', '2017-01-26 09:07:56'),
(314, 102, 'Hải Phòng, Việt Nam', 1, 'N', '2017-01-26 09:08:06', '2017-01-26 09:08:06'),
(315, 103, 'Beppu, Ōita, Nhật Bản', 1, 'Y', '2017-01-26 09:23:32', '2017-01-26 09:23:32'),
(316, 103, 'Hà Nội, Việt Nam', 1, 'N', '2017-01-26 09:23:42', '2017-01-26 09:23:42'),
(317, 103, 'Hải Phòng, Việt Nam', 1, 'N', '2017-01-26 09:23:51', '2017-01-26 09:23:51'),
(318, 103, 'Miyazaki, Nhật Bản', 2, 'N', '2017-01-26 09:24:22', '2017-01-26 09:24:22'),
(319, 103, 'Fukuoka, Nhật Bản', 2, 'N', '2017-01-26 09:24:32', '2017-01-26 09:24:32'),
(320, 104, 'Hà Nội, Việt Nam', 1, 'Y', '2017-01-26 09:34:25', '2017-01-26 09:34:25'),
(321, 104, 'Hải Phòng, Việt Nam', 1, 'N', '2017-01-26 09:34:34', '2017-01-26 09:34:34'),
(322, 105, 'Hà Nội, Việt Nam', 1, 'Y', '2017-01-26 09:45:30', '2017-01-26 09:45:30'),
(323, 105, 'Hải Phòng, Việt Nam', 1, 'N', '2017-01-26 09:45:42', '2017-01-26 09:45:42'),
(324, 106, 'Hà Nội, Việt Nam', 1, 'Y', '2017-01-26 09:56:02', '2017-01-26 09:56:02'),
(325, 106, 'Monterey, California, Hoa Kỳ', 1, 'N', '2017-01-26 09:56:42', '2017-01-26 09:56:42'),
(326, 106, 'Taiwan, Đài Loan', 1, 'N', '2017-01-26 09:56:56', '2017-01-26 09:56:56'),
(327, 107, 'Hà Nội, Việt Nam', 1, 'Y', '2017-01-27 07:42:42', '2017-01-27 07:42:42'),
(328, 107, 'Barcelona, Tây Ban Nha', 2, 'N', '2017-01-27 07:44:11', '2017-01-27 07:44:11'),
(329, 107, 'Bucharest, Ru-ma-ni', 2, 'N', '2017-01-27 07:50:24', '2017-01-27 07:50:24'),
(330, 107, 'Rome, Roma, Ý', 2, 'N', '2017-01-27 07:50:36', '2017-01-27 07:50:36'),
(331, 107, 'Manila, Vùng đô thị Manila, Phi-líp-pin', 2, 'N', '2017-01-27 07:50:55', '2017-01-27 07:50:55'),
(332, 107, 'Nice, Pháp', 2, 'N', '2017-01-27 07:51:15', '2017-01-27 07:51:15'),
(333, 107, 'Istanbul, Thổ Nhĩ Kỳ', 2, 'N', '2017-01-27 07:51:50', '2017-01-27 07:51:50'),
(334, 107, 'Venice, Ý', 2, 'N', '2017-01-27 07:52:05', '2017-01-27 07:52:05'),
(335, 107, 'Vienna, Áo', 2, 'N', '2017-01-27 07:52:20', '2017-01-27 07:52:20'),
(336, 107, 'Sibiu, Ru-ma-ni', 2, 'N', '2017-01-27 07:52:35', '2017-01-27 07:52:35'),
(338, 107, 'Brussel, Bỉ', 2, 'N', '2017-01-27 07:53:45', '2017-01-27 07:53:45'),
(339, 107, 'Bali, In-đô-nê-xi-a', 2, 'N', '2017-01-27 07:54:10', '2017-01-27 07:54:10'),
(340, 107, 'Boracay, Malay, Tây Visayas, Phi-líp-pin', 2, 'N', '2017-01-27 07:54:23', '2017-01-27 07:54:23'),
(341, 107, 'Penang Ma-lai-xi-a', 2, 'N', '2017-01-27 07:54:34', '2017-01-27 07:54:34'),
(342, 108, 'Singapore', 1, 'Y', '2017-01-27 08:31:15', '2017-01-27 08:31:15'),
(343, 108, 'Hà Nội, Việt Nam', 1, 'N', '2017-01-27 08:32:28', '2017-01-27 08:32:28'),
(344, 108, 'Interlaken, Thuỵ Sĩ', 2, 'N', '2017-01-27 08:32:57', '2017-01-27 08:32:57'),
(345, 108, 'Alexandria, Ai Cập', 2, 'N', '2017-01-27 08:33:18', '2017-01-27 08:33:18'),
(346, 108, 'Cairo, Ai Cập', 2, 'N', '2017-01-27 08:33:26', '2017-01-27 08:33:26'),
(347, 108, 'Colombo, Western Province, Xri Lan-ca', 2, 'N', '2017-01-27 08:33:43', '2017-01-27 08:33:43'),
(348, 108, 'Genève, Thuỵ Sĩ', 2, 'N', '2017-01-27 08:33:54', '2017-01-27 08:33:54'),
(349, 108, 'Macao, Ma Cao', 2, 'N', '2017-01-27 08:34:05', '2017-01-27 08:34:05'),
(350, 108, 'Laax, Thuỵ Sĩ', 2, 'N', '2017-01-27 08:34:17', '2017-01-27 08:34:17'),
(351, 108, 'Dachau, Đức', 2, 'N', '2017-01-27 08:34:36', '2017-01-27 08:34:36'),
(352, 108, 'München, Đức', 2, 'N', '2017-01-27 08:34:47', '2017-01-27 08:34:47'),
(353, 108, 'Malé, Man-đi-vơ', 2, 'N', '2017-01-27 08:34:56', '2017-01-27 08:34:56'),
(354, 108, 'Lagoi, Sebong Lagoi, Kabupaten Bintan, Riau Islands, In-đô-nê-xi-a', 2, 'N', '2017-01-27 08:35:26', '2017-01-27 08:35:26'),
(355, 108, 'Jerusalem, Israel', 2, 'N', '2017-01-27 08:35:39', '2017-01-27 08:35:39'),
(356, 108, 'Ban Khao Lak Lam Kaen Phang Nga Thái Lan', 2, 'N', '2017-01-27 08:35:59', '2017-01-27 08:35:59'),
(357, 108, 'Rigi Kulm, Arth, Thuỵ Sĩ', 2, 'N', '2017-01-27 08:36:14', '2017-01-27 08:36:14'),
(358, 108, 'Port Said Governorate, Ai Cập', 2, 'N', '2017-01-27 08:36:30', '2017-01-27 08:36:30'),
(359, 109, 'Hà Nội, Việt Nam', 1, 'Y', '2017-01-27 08:43:41', '2017-01-27 08:43:41'),
(360, 109, 'Gainesville, Florida, Hoa Kỳ', 1, 'N', '2017-01-27 08:44:25', '2017-01-27 08:44:25'),
(361, 109, 'Helsinki, Phần Lan', 1, 'N', '2017-01-27 08:44:35', '2017-01-27 08:44:35'),
(362, 109, 'Mikkeli, Phần Lan', 2, 'N', '2017-01-27 08:44:56', '2017-01-27 08:44:56'),
(363, 109, 'Espoo, Phần Lan', 2, 'N', '2017-01-27 08:45:10', '2017-01-27 08:45:10'),
(364, 109, 'Philadelphia, Pennsylvania, Hoa Kỳ', 2, 'N', '2017-01-27 08:45:30', '2017-01-27 08:45:30'),
(365, 110, 'Yokohama, Kanagawa, Nhật Bản', 1, 'Y', '2017-01-27 08:56:07', '2017-01-27 08:56:07'),
(366, 110, 'Hà Nội, Việt Nam', 1, 'N', '2017-01-27 08:56:20', '2017-01-27 08:56:20'),
(367, 110, 'Shibuya, Tokyo, Nhật Bản', 2, 'N', '2017-01-27 08:56:55', '2017-01-27 08:56:55'),
(368, 110, 'Ōsaka, Nhật Bản', 2, 'N', '2017-01-27 08:57:29', '2017-01-27 08:57:29'),
(369, 110, 'Narita, Chiba, Nhật Bản', 2, 'N', '2017-01-27 08:57:42', '2017-01-27 08:57:42'),
(370, 110, 'Kyōto, Nhật Bản', 2, 'N', '2017-01-27 08:57:53', '2017-01-27 08:57:53'),
(371, 110, 'Venice, Ý', 2, 'N', '2017-01-27 08:58:15', '2017-01-27 08:58:15'),
(372, 110, 'Honolulu, Hawaii, Hoa Kỳ', 2, 'N', '2017-01-27 08:58:32', '2017-01-27 08:58:32'),
(373, 110, 'Fuchū, Tokyo, Nhật Bản', 2, 'N', '2017-01-27 08:58:44', '2017-01-27 08:58:44'),
(374, 110, 'Florence, Firenze, Ý', 2, 'N', '2017-01-27 08:59:10', '2017-01-27 08:59:10'),
(375, 110, 'Gifu, Nhật Bản', 2, 'N', '2017-01-27 08:59:23', '2017-01-27 08:59:23'),
(376, 110, 'Sofia, Bun-ga-ri', 2, 'N', '2017-01-27 08:59:34', '2017-01-27 08:59:34'),
(377, 110, 'Nice, Pháp', 2, 'N', '2017-01-27 08:59:46', '2017-01-27 08:59:46'),
(378, 110, 'Karlovo, Bun-ga-ri', 2, 'N', '2017-01-27 08:59:59', '2017-01-27 08:59:59'),
(379, 110, 'Troyan, Bun-ga-ri', 2, 'N', '2017-01-27 09:00:10', '2017-01-27 09:00:10'),
(380, 110, 'Nara, Nhật Bản', 2, 'N', '2017-01-27 09:00:20', '2017-01-27 09:00:20'),
(381, 110, 'Roma, Ý', 2, 'N', '2017-01-27 09:01:21', '2017-01-27 09:01:21'),
(382, 110, 'Paris, Pháp', 2, 'N', '2017-01-27 09:01:37', '2017-01-27 09:01:37'),
(383, 110, 'Berlin, Đức', 2, 'N', '2017-01-27 09:01:58', '2017-01-27 09:01:58'),
(384, 110, 'Salzburg, Áo', 2, 'N', '2017-01-27 09:02:19', '2017-01-27 09:02:19'),
(385, 110, 'Sozopol, Bun-ga-ri', 2, 'N', '2017-01-27 09:02:44', '2017-01-27 09:02:44'),
(386, 110, 'Athens, Kentrikos Tomeas Athinon, Hy Lạp', 2, 'N', '2017-01-27 09:03:05', '2017-01-27 09:03:05'),
(387, 110, 'Tehran, I-ran', 2, 'N', '2017-01-27 09:03:24', '2017-01-27 09:03:24'),
(388, 110, 'Esfahan, I-ran', 2, 'N', '2017-01-27 09:03:36', '2017-01-27 09:03:36'),
(389, 110, 'Kashan, Isfahan, I-ran', 2, 'N', '2017-01-27 09:03:48', '2017-01-27 09:03:48'),
(391, 110, 'Yangon Region, Mi-an-ma (Miến Điện)', 2, 'N', '2017-01-27 09:04:19', '2017-01-27 09:04:19'),
(392, 110, 'Khaosan Road Bangkok Thái Lan', 2, 'N', '2017-01-27 09:04:39', '2017-01-27 09:04:39'),
(393, 110, 'Penang Ma-lai-xi-a', 2, 'N', '2017-01-27 09:04:50', '2017-01-27 09:04:50'),
(394, 110, 'Taipei City, Đài Loan', 2, 'N', '2017-01-27 09:05:03', '2017-01-27 09:05:03'),
(395, 110, 'Nanjing, Giang Tô, Trung Quốc', 2, 'N', '2017-01-27 09:05:34', '2017-01-27 09:05:34'),
(396, 110, 'Thượng Hải, Trung Quốc', 2, 'N', '2017-01-27 09:05:49', '2017-01-27 09:05:49'),
(397, 110, 'Dalian, Liêu Ninh, Trung Quốc', 2, 'N', '2017-01-27 09:06:11', '2017-01-27 09:06:11'),
(398, 111, 'New York, Hoa Kỳ', 1, 'Y', '2017-01-27 20:03:56', '2017-01-27 20:03:56'),
(399, 111, 'Hà Nội, Việt Nam', 1, 'N', '2017-01-27 20:04:06', '2017-01-27 20:04:06'),
(400, 111, 'Manila, Vùng đô thị Manila, Phi-líp-pin', 2, 'N', '2017-01-27 20:04:50', '2017-01-27 20:04:50'),
(401, 111, 'Boracay, Malay, Tây Visayas, Phi-líp-pin', 2, 'N', '2017-01-27 20:05:15', '2017-01-27 20:05:15'),
(402, 111, 'Cebu, Trung tâm Visayas, Phi-líp-pin', 2, 'N', '2017-01-27 20:05:26', '2017-01-27 20:05:26'),
(403, 111, 'Kota Kinabalu Sabah Ma-lai-xi-a', 2, 'N', '2017-01-27 20:06:03', '2017-01-27 20:06:03'),
(404, 111, 'Brunei', 2, 'N', '2017-01-27 20:06:30', '2017-01-27 20:06:30'),
(405, 111, 'Langkawi Kedah Ma-lai-xi-a', 2, 'N', '2017-01-27 20:06:46', '2017-01-27 20:06:46'),
(406, 111, 'Penang Ma-lai-xi-a', 2, 'N', '2017-01-27 20:06:55', '2017-01-27 20:06:55'),
(407, 111, 'Bali, In-đô-nê-xi-a', 2, 'N', '2017-01-27 20:07:11', '2017-01-27 20:07:11'),
(408, 112, 'Hà Nội, Việt Nam', 1, 'Y', '2017-01-27 20:19:11', '2017-01-27 20:19:11'),
(409, 112, 'Hạ Long, Quảng Ninh, Việt Nam', 1, 'N', '2017-01-27 20:19:20', '2017-01-27 20:19:20'),
(410, 112, 'Vientiane, Vientiane Prefecture, Lào', 2, 'N', '2017-01-27 20:20:27', '2017-01-27 20:20:27'),
(411, 112, 'Angkor Wat, Xiêm Riệp, Cam-pu-chia', 2, 'N', '2017-01-27 20:21:05', '2017-01-27 20:21:05'),
(412, 112, 'Nong Khae Saraburi Thái Lan', 2, 'N', '2017-01-27 20:21:32', '2017-01-27 20:21:32'),
(413, 112, 'Pattaya Chon Buri Thái Lan', 2, 'N', '2017-01-27 20:21:45', '2017-01-27 20:21:45'),
(414, 112, 'Phnôm Pênh, Cam-pu-chia', 2, 'N', '2017-01-27 20:22:01', '2017-01-27 20:22:01'),
(415, 112, 'Bali, In-đô-nê-xi-a', 2, 'N', '2017-01-27 20:24:35', '2017-01-27 20:24:35'),
(416, 113, 'Hà Nội, Việt Nam', 1, 'Y', '2017-01-27 20:33:42', '2017-01-27 20:33:42'),
(417, 113, 'Dubai - Các tiểu Vương quốc Ả rập Thống nhất', 2, 'N', '2017-01-27 20:34:30', '2017-01-27 20:34:30'),
(418, 113, 'Amsterdam, Hà Lan', 2, 'N', '2017-01-27 20:34:57', '2017-01-27 20:34:57'),
(419, 113, 'Mont-Saint-Michel, Pháp', 2, 'N', '2017-01-27 20:35:24', '2017-01-27 20:35:24'),
(421, 113, 'Paris, Pháp', 1, 'N', '2017-01-27 20:35:48', '2017-01-27 20:35:48'),
(422, 113, 'Mykonos, Hy Lạp', 2, 'N', '2017-01-27 20:36:09', '2017-01-27 20:36:09'),
(423, 113, 'Jeju, Hàn Quốc', 2, 'N', '2017-01-27 20:36:40', '2017-01-27 20:36:40'),
(424, 113, 'Mount Fuji, Fujinomiya, Shizuoka, Nhật Bản', 2, 'N', '2017-01-27 20:37:24', '2017-01-27 20:37:24'),
(425, 113, 'Tokyo, Nhật Bản', 2, 'N', '2017-01-27 20:37:43', '2017-01-27 20:37:43'),
(426, 114, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-27 20:58:34', '2017-01-27 20:58:34'),
(427, 114, 'Portsmouth, United Kingdom', 1, 'N', '2017-01-27 20:58:50', '2017-01-27 20:58:50'),
(428, 114, 'Athens, Athina, Kentrikos Tomeas Athinon, Greece', 2, 'N', '2017-01-27 20:59:26', '2017-01-27 20:59:26'),
(429, 114, 'Crawley, United Kingdom', 2, 'N', '2017-01-27 20:59:59', '2017-01-27 20:59:59'),
(430, 114, 'Brighton, United Kingdom', 2, 'N', '2017-01-27 21:00:13', '2017-01-27 21:00:13'),
(431, 114, 'Winchester, United Kingdom', 2, 'N', '2017-01-27 21:00:33', '2017-01-27 21:00:33'),
(432, 114, 'Bath, United Kingdom', 2, 'N', '2017-01-27 21:00:42', '2017-01-27 21:00:42'),
(433, 114, 'Paris, France', 2, 'N', '2017-01-27 21:00:59', '2017-01-27 21:00:59'),
(434, 115, 'Ho Chi Minh, Vietnam', 1, 'Y', '2017-01-27 21:54:56', '2017-01-27 21:54:56'),
(435, 115, 'Pleiku, Gia Lai Province, Vietnam', 1, 'N', '2017-01-27 21:55:40', '2017-01-27 21:55:40'),
(436, 115, 'Taipei City, Taiwan', 2, 'N', '2017-01-27 21:55:57', '2017-01-27 21:55:57'),
(437, 115, 'Hong Kong', 2, 'N', '2017-01-27 21:56:12', '2017-01-27 21:56:12'),
(438, 115, 'Singapore', 2, 'N', '2017-01-27 21:57:02', '2017-01-27 21:57:02'),
(439, 115, 'Bangkok Thailand', 2, 'N', '2017-01-27 21:57:25', '2017-01-27 21:57:25'),
(440, 115, 'Shanghai, China', 2, 'N', '2017-01-27 21:57:43', '2017-01-27 21:57:43'),
(441, 115, 'Busan, South Korea', 2, 'N', '2017-01-27 21:58:05', '2017-01-27 21:58:05'),
(442, 115, 'Kyoto, Kyoto Prefecture, Japan', 2, 'N', '2017-01-27 21:58:27', '2017-01-27 21:58:27'),
(443, 115, 'Tokyo, Japan', 2, 'N', '2017-01-27 21:58:39', '2017-01-27 21:58:39'),
(444, 115, 'Victoria, Australia', 1, 'N', '2017-01-27 21:59:05', '2017-01-27 21:59:05'),
(445, 116, 'Ho Chi Minh, Vietnam', 1, 'Y', '2017-01-27 22:10:10', '2017-01-27 22:10:10'),
(446, 116, 'Hà Nội, Vietnam', 1, 'N', '2017-01-27 22:10:24', '2017-01-27 22:10:24'),
(447, 116, 'Singapore', 2, 'N', '2017-01-27 22:10:56', '2017-01-27 22:10:56'),
(448, 116, 'Seoul, South Korea', 2, 'N', '2017-01-27 22:11:10', '2017-01-27 22:11:10'),
(449, 116, 'Boracay, Malay, Western Visayas, Philippines', 2, 'N', '2017-01-27 22:11:24', '2017-01-27 22:11:24'),
(450, 116, 'Bangkok Thailand', 2, 'N', '2017-01-27 22:12:24', '2017-01-27 22:12:24'),
(451, 117, 'Trento, Italy', 1, 'Y', '2017-01-27 22:17:18', '2017-01-27 22:17:18'),
(452, 117, 'Hà Nội, Vietnam', 1, 'N', '2017-01-27 22:17:27', '2017-01-27 22:17:27'),
(454, 117, 'Tiên Lãng, Haiphong, Vietnam', 1, 'N', '2017-01-27 22:21:11', '2017-01-27 22:21:11'),
(455, 118, 'Helsinki, Finland', 1, 'Y', '2017-01-27 22:24:54', '2017-01-27 22:24:54'),
(456, 118, 'Hà Nam Province, Vietnam', 1, 'N', '2017-01-27 22:25:05', '2017-01-27 22:25:05'),
(457, 118, 'Vaasa, Finland', 1, 'N', '2017-01-27 22:25:17', '2017-01-27 22:25:17'),
(458, 118, 'Vienna, Austria', 2, 'N', '2017-01-27 22:31:18', '2017-01-27 22:31:18'),
(459, 118, 'St Petersburg, Saint Petersburg, Russia', 2, 'N', '2017-01-27 22:32:12', '2017-01-27 22:32:12'),
(460, 118, 'Sopot, Poland', 2, 'N', '2017-01-27 22:32:36', '2017-01-27 22:32:36'),
(461, 119, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-28 02:56:05', '2017-01-28 02:56:05'),
(462, 119, 'London, United Kingdom', 1, 'N', '2017-01-28 02:56:22', '2017-01-28 02:56:22'),
(463, 119, 'Hải Phòng, Vietnam', 1, 'N', '2017-01-28 02:56:50', '2017-01-28 02:56:50'),
(464, 120, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-28 03:03:08', '2017-01-28 03:03:08'),
(465, 120, 'London, United Kingdom', 1, 'N', '2017-01-28 03:03:57', '2017-01-28 03:03:57'),
(466, 121, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-28 03:13:35', '2017-01-28 03:13:35'),
(467, 121, 'Berlin, Germany', 1, 'N', '2017-01-28 03:13:48', '2017-01-28 03:13:48'),
(468, 121, 'Bangkok Thailand', 2, 'N', '2017-01-28 03:14:21', '2017-01-28 03:14:21'),
(469, 121, 'Melbourne, Victoria, Australia', 2, 'N', '2017-01-28 03:15:06', '2017-01-28 03:15:06'),
(470, 121, 'Docklands, Victoria, Australia', 2, 'N', '2017-01-28 03:15:23', '2017-01-28 03:15:23'),
(471, 121, 'Brighton, Victoria, Australia', 2, 'N', '2017-01-28 03:15:35', '2017-01-28 03:15:35'),
(472, 121, 'Mount Ruapehu, Manawatu-Wanganui, New Zealand', 2, 'N', '2017-01-28 03:16:28', '2017-01-28 03:16:28'),
(473, 121, 'Manawatu-Wanganui, New Zealand', 2, 'N', '2017-01-28 03:16:49', '2017-01-28 03:16:49'),
(475, 121, 'Taupo, Waikato, New Zealand', 2, 'N', '2017-01-28 03:17:35', '2017-01-28 03:17:35'),
(476, 121, 'Muriwai, Auckland, New Zealand', 2, 'N', '2017-01-28 03:18:04', '2017-01-28 03:18:04'),
(477, 121, 'Palmerston North, Manawatu-Wanganui, New Zealand', 2, 'N', '2017-01-28 03:18:24', '2017-01-28 03:18:24'),
(478, 121, 'Wellington, New Zealand', 2, 'N', '2017-01-28 03:18:38', '2017-01-28 03:18:38'),
(479, 121, 'Auckland, New Zealand', 1, 'N', '2017-01-28 03:18:50', '2017-01-28 03:18:50'),
(480, 121, 'Christchurch, Canterbury, New Zealand', 2, 'N', '2017-01-28 03:19:06', '2017-01-28 03:19:06'),
(481, 121, 'Tekapo, Canterbury, New Zealand', 2, 'N', '2017-01-28 03:19:20', '2017-01-28 03:19:20'),
(482, 121, 'Queenstown, Otago, New Zealand', 2, 'N', '2017-01-28 03:19:35', '2017-01-28 03:19:35'),
(483, 121, 'Dossenheim, Germany', 2, 'N', '2017-01-28 03:20:21', '2017-01-28 03:20:21'),
(484, 121, 'Paris, France', 2, 'N', '2017-01-28 03:20:33', '2017-01-28 03:20:33'),
(485, 121, 'Luxembourg', 2, 'N', '2017-01-28 03:20:44', '2017-01-28 03:20:44'),
(486, 121, 'Bonn, Germany', 2, 'N', '2017-01-28 03:20:56', '2017-01-28 03:20:56'),
(487, 121, 'Münster, Germany', 2, 'N', '2017-01-28 03:21:12', '2017-01-28 03:21:12'),
(488, 121, 'Amsterdam, Netherlands', 2, 'N', '2017-01-28 03:21:35', '2017-01-28 03:21:35'),
(489, 122, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-28 03:35:08', '2017-01-28 03:35:08'),
(490, 122, 'Hải Phòng, Vietnam', 1, 'N', '2017-01-28 03:35:18', '2017-01-28 03:35:18'),
(491, 123, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-28 03:40:32', '2017-01-28 03:40:32'),
(492, 123, 'Glasgow, United Kingdom', 1, 'N', '2017-01-28 03:41:02', '2017-01-28 03:41:02'),
(493, 123, 'Edinburgh, United Kingdom', 2, 'N', '2017-01-28 03:41:39', '2017-01-28 03:41:39'),
(494, 123, 'Ambleside, United Kingdom', 2, 'N', '2017-01-28 06:32:45', '2017-01-28 06:32:45'),
(495, 123, 'York, United Kingdom', 2, 'N', '2017-01-28 06:33:04', '2017-01-28 06:33:04'),
(496, 123, 'Leeds, United Kingdom', 2, 'N', '2017-01-28 06:34:31', '2017-01-28 06:34:31'),
(497, 123, 'Manchester, United Kingdom', 2, 'N', '2017-01-28 06:34:45', '2017-01-28 06:34:45'),
(498, 123, 'Highland, Scotland, United Kingdom', 2, 'N', '2017-01-28 06:35:12', '2017-01-28 06:35:12'),
(499, 123, 'Liverpool, United Kingdom', 2, 'N', '2017-01-28 06:35:23', '2017-01-28 06:35:23'),
(500, 123, 'Nottingham, United Kingdom', 2, 'N', '2017-01-28 06:35:37', '2017-01-28 06:35:37'),
(501, 123, 'Birmingham, United Kingdom', 2, 'N', '2017-01-28 06:35:49', '2017-01-28 06:35:49'),
(502, 123, 'Cambridge, United Kingdom', 2, 'N', '2017-01-28 06:36:00', '2017-01-28 06:36:00'),
(503, 123, 'London, United Kingdom', 2, 'N', '2017-01-28 06:36:19', '2017-01-28 06:36:19'),
(504, 123, 'Brighton, United Kingdom', 2, 'N', '2017-01-28 06:36:30', '2017-01-28 06:36:30'),
(505, 123, 'Bath, United Kingdom', 2, 'N', '2017-01-28 06:36:42', '2017-01-28 06:36:42'),
(506, 123, 'Belfast, United Kingdom', 2, 'N', '2017-01-28 06:37:06', '2017-01-28 06:37:06'),
(507, 123, 'Scotland, United Kingdom', 2, 'N', '2017-01-28 06:37:29', '2017-01-28 06:37:29'),
(508, 123, 'Paris, France', 2, 'N', '2017-01-28 06:37:59', '2017-01-28 06:37:59'),
(509, 123, 'Barcelona, Spain', 2, 'N', '2017-01-28 06:38:19', '2017-01-28 06:38:19'),
(510, 123, 'Rome, Metropolitan City of Rome, Italy', 2, 'N', '2017-01-28 06:38:45', '2017-01-28 06:38:45'),
(511, 123, 'Santorini, Thira, Greece', 2, 'N', '2017-01-28 06:39:03', '2017-01-28 06:39:03'),
(513, 123, 'Long Beach, CA, United States', 2, 'N', '2017-01-28 06:40:45', '2017-01-28 06:40:45'),
(514, 123, 'Tokyo, Japan', 2, 'N', '2017-01-28 06:41:09', '2017-01-28 06:41:09'),
(515, 123, 'Kuala Lumpur Federal Territory of Kuala Lumpur Malaysia', 2, 'N', '2017-01-28 06:41:40', '2017-01-28 06:41:40'),
(516, 123, 'Singapore', 2, 'N', '2017-01-28 06:41:51', '2017-01-28 06:41:51'),
(517, 124, 'Union City, New Jersey, United States', 1, 'Y', '2017-01-28 07:01:00', '2017-01-28 07:01:00'),
(518, 124, 'Hải Dương, Hai Duong, Vietnam', 1, 'N', '2017-01-28 07:01:16', '2017-01-28 07:01:16'),
(519, 124, 'New York, United States', 2, 'N', '2017-01-28 07:01:43', '2017-01-28 07:01:43'),
(520, 124, 'Princeton, NJ, United States', 2, 'N', '2017-01-28 07:01:57', '2017-01-28 07:01:57'),
(521, 124, 'Virginia, United States', 2, 'N', '2017-01-28 07:02:30', '2017-01-28 07:02:30'),
(522, 124, 'Washington, United States', 2, 'N', '2017-01-28 07:02:40', '2017-01-28 07:02:40'),
(523, 124, 'Hong Kong', 2, 'N', '2017-01-28 07:03:09', '2017-01-28 07:03:09'),
(524, 125, 'Ho Chi Minh, Vietnam', 1, 'Y', '2017-01-28 08:12:42', '2017-01-28 08:12:42'),
(525, 125, 'Hà Nội, Vietnam', 1, 'N', '2017-01-28 08:12:51', '2017-01-28 08:12:51'),
(526, 125, 'Hải Phòng, Vietnam', 1, 'N', '2017-01-28 08:12:59', '2017-01-28 08:12:59'),
(527, 125, 'Bangkok Thailand', 2, 'N', '2017-01-28 08:13:10', '2017-01-28 08:13:10'),
(528, 125, 'Hong Kong', 2, 'N', '2017-01-28 08:13:19', '2017-01-28 08:13:19'),
(529, 125, 'Singapore', 2, 'N', '2017-01-28 08:13:31', '2017-01-28 08:13:31'),
(530, 125, 'London, United Kingdom', 2, 'N', '2017-01-28 08:13:56', '2017-01-28 08:13:56'),
(531, 126, 'Hà Nội, Vietnam', 1, 'N', '2017-01-28 08:34:08', '2017-01-28 08:34:38'),
(532, 126, 'Ho Chi Minh, Vietnam', 1, 'Y', '2017-01-28 08:34:16', '2017-01-28 08:34:38'),
(533, 127, 'Singapore', 1, 'N', '2017-01-28 08:39:47', '2017-01-28 08:40:49'),
(534, 127, 'Canberra, Australian Capital Territory, Australia', 1, 'N', '2017-01-28 08:39:59', '2017-01-28 08:40:49'),
(535, 127, 'Hà Nội, Vietnam', 1, 'N', '2017-01-28 08:40:07', '2017-01-28 08:40:49'),
(536, 127, 'Hải Phòng, Vietnam', 1, 'N', '2017-01-28 08:40:16', '2017-01-28 08:40:49'),
(537, 127, 'Ho Chi Minh, Vietnam', 1, 'Y', '2017-01-28 08:40:41', '2017-01-28 08:40:49'),
(538, 127, 'Kuala Lumpur Federal Territory of Kuala Lumpur Malaysia', 2, 'N', '2017-01-28 08:41:24', '2017-01-28 08:41:24'),
(539, 127, 'Hong Kong', 2, 'N', '2017-01-28 08:41:42', '2017-01-28 08:41:42'),
(540, 127, 'Taipei City, Taiwan', 2, 'N', '2017-01-28 08:41:54', '2017-01-28 08:41:54'),
(541, 127, 'Sydney, New South Wales, Australia', 2, 'N', '2017-01-28 08:42:13', '2017-01-28 08:42:13'),
(542, 127, 'Victoria, Australia', 2, 'N', '2017-01-28 08:42:25', '2017-01-28 08:42:25'),
(543, 128, 'Singapore', 1, 'Y', '2017-01-28 08:48:21', '2017-01-28 08:48:21'),
(544, 128, 'Hanoi, Vietnam', 1, 'N', '2017-01-28 08:48:31', '2017-01-28 08:48:31'),
(545, 129, 'Bac Ninh Province, Vietnam', 1, 'Y', '2017-01-28 08:56:54', '2017-01-28 08:56:54'),
(546, 129, 'Hà Nội, Vietnam', 1, 'N', '2017-01-28 08:57:32', '2017-01-28 08:57:32'),
(547, 129, 'Toulouse, France', 1, 'N', '2017-01-28 08:57:42', '2017-01-28 08:57:42'),
(549, 129, 'Paris, France', 2, 'N', '2017-01-28 08:58:49', '2017-01-28 08:58:49'),
(550, 129, 'Barcelona, Spain', 2, 'N', '2017-01-28 08:59:02', '2017-01-28 08:59:02'),
(551, 129, 'Carcassonne, France', 2, 'N', '2017-01-28 08:59:22', '2017-01-28 08:59:22'),
(552, 129, 'Bordeaux, France', 2, 'N', '2017-01-28 08:59:35', '2017-01-28 08:59:35'),
(553, 130, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-28 09:06:14', '2017-01-28 09:06:14'),
(554, 130, 'Paris, France', 1, 'N', '2017-01-28 09:06:24', '2017-01-28 09:06:24'),
(555, 131, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-28 09:17:56', '2017-01-28 09:17:56'),
(556, 131, 'Buffalo, NY, United States', 1, 'N', '2017-01-28 09:18:10', '2017-01-28 09:18:10'),
(557, 132, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-28 09:28:00', '2017-01-28 09:28:00'),
(558, 132, 'Melbourne, Victoria, Australia', 1, 'N', '2017-01-28 09:28:15', '2017-01-28 09:28:15'),
(559, 132, 'Moscow, Russia', 1, 'N', '2017-01-28 09:28:25', '2017-01-28 09:28:25'),
(560, 133, 'Sydney, New South Wales, Australia', 1, 'Y', '2017-01-28 20:00:26', '2017-01-28 20:00:26'),
(561, 133, 'Hà Nội, Vietnam', 1, 'N', '2017-01-28 20:00:37', '2017-01-28 20:00:37'),
(562, 133, 'Ninh Bình Province, Vietnam', 1, 'N', '2017-01-28 20:00:49', '2017-01-28 20:00:49'),
(563, 133, 'Kensington, New South Wales, Australia', 2, 'N', '2017-01-28 20:01:43', '2017-01-28 20:01:43'),
(564, 134, 'Paris, France', 1, 'Y', '2017-01-28 20:10:36', '2017-01-28 20:10:36'),
(565, 134, 'Hà Nội, Vietnam', 1, 'N', '2017-01-28 20:10:45', '2017-01-28 20:10:45'),
(566, 134, 'Palermo, Italy', 2, 'N', '2017-01-28 20:11:25', '2017-01-28 20:11:25'),
(567, 134, 'Catania, Italy', 2, 'N', '2017-01-28 20:11:39', '2017-01-28 20:11:39'),
(568, 134, 'Cosenza, Italy', 2, 'N', '2017-01-28 20:11:53', '2017-01-28 20:11:53'),
(569, 134, 'Rome, Italy', 2, 'N', '2017-01-28 20:12:03', '2017-01-28 20:12:03'),
(570, 134, 'Venice, Italy', 2, 'N', '2017-01-28 20:12:14', '2017-01-28 20:12:14'),
(571, 134, 'Monaco', 2, 'N', '2017-01-28 20:13:07', '2017-01-28 20:13:07'),
(572, 134, 'Vienna, Austria', 2, 'N', '2017-01-28 20:13:21', '2017-01-28 20:13:21'),
(573, 134, 'Eguisheim, France', 2, 'N', '2017-01-28 20:13:36', '2017-01-28 20:13:36'),
(574, 134, 'Strasbourg, France', 2, 'N', '2017-01-28 20:13:48', '2017-01-28 20:13:48'),
(575, 134, 'Brussels, Belgium', 2, 'N', '2017-01-28 20:14:02', '2017-01-28 20:14:02'),
(576, 135, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-28 20:23:53', '2017-01-28 20:23:53'),
(577, 135, 'Leicester, United Kingdom', 1, 'N', '2017-01-28 20:24:13', '2017-01-28 20:24:13'),
(578, 136, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-28 20:31:42', '2017-01-28 20:31:42'),
(579, 136, 'Thai Binh, Vietnam', 1, 'N', '2017-01-28 20:31:51', '2017-01-28 20:31:51'),
(580, 136, 'Rome, Italy', 2, 'N', '2017-01-28 20:32:27', '2017-01-28 20:32:27'),
(581, 136, 'Barcelona, Spain', 2, 'N', '2017-01-28 20:32:39', '2017-01-28 20:32:39'),
(582, 136, 'Marseille, France', 2, 'N', '2017-01-28 20:32:54', '2017-01-28 20:32:54'),
(583, 136, 'Budapest, Hungary', 2, 'N', '2017-01-28 20:33:03', '2017-01-28 20:33:03'),
(584, 136, 'Ansfelden, Austria', 2, 'N', '2017-01-28 20:33:17', '2017-01-28 20:33:17'),
(585, 136, 'Paris, France', 2, 'N', '2017-01-28 20:33:27', '2017-01-28 20:33:27'),
(586, 136, 'Nottingham, United Kingdom', 2, 'N', '2017-01-28 20:33:59', '2017-01-28 20:33:59'),
(587, 136, 'Sheffield, United Kingdom', 2, 'N', '2017-01-28 20:34:11', '2017-01-28 20:34:11'),
(588, 136, 'Manchester, United Kingdom', 2, 'N', '2017-01-28 20:34:21', '2017-01-28 20:34:21'),
(589, 136, 'Bournemouth, United Kingdom', 2, 'N', '2017-01-28 20:35:39', '2017-01-28 20:35:39'),
(591, 136, 'London, United Kingdom', 2, 'N', '2017-01-28 20:40:08', '2017-01-28 20:40:08'),
(592, 136, 'Leicester, United Kingdom', 1, 'N', '2017-01-28 20:40:25', '2017-01-28 20:40:25'),
(593, 137, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-28 20:59:55', '2017-01-28 20:59:55'),
(594, 137, 'Hải Phòng, Vietnam', 1, 'N', '2017-01-28 21:00:06', '2017-01-28 21:00:06'),
(595, 138, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-28 21:11:48', '2017-01-28 21:11:48'),
(596, 138, 'Budapest, Hungary', 1, 'N', '2017-01-28 21:12:03', '2017-01-28 21:12:03'),
(597, 138, 'El Nido, MIMAROPA, Philippines', 2, 'N', '2017-01-28 21:12:53', '2017-01-28 21:12:53'),
(598, 138, 'Pattaya City Chon Buri Thailand', 2, 'N', '2017-01-28 21:43:02', '2017-01-28 21:43:02'),
(599, 139, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-28 21:48:09', '2017-01-28 21:48:09'),
(600, 139, 'Hải Phòng, Vietnam', 1, 'N', '2017-01-28 21:48:19', '2017-01-28 21:48:19'),
(601, 139, 'Seoul, South Korea', 2, 'N', '2017-01-28 21:48:48', '2017-01-28 21:48:48'),
(602, 140, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-28 21:55:49', '2017-01-28 21:55:49'),
(603, 140, 'Phu Tho Province, Vietnam', 1, 'N', '2017-01-28 22:00:35', '2017-01-28 22:00:35'),
(604, 141, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 02:26:08', '2017-01-29 02:26:08'),
(605, 141, 'Bangkok Thailand', 2, 'N', '2017-01-29 02:26:44', '2017-01-29 02:26:44'),
(607, 141, 'Tehran, Tehran Province, Iran', 2, 'N', '2017-01-29 02:27:38', '2017-01-29 02:27:38'),
(608, 142, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 02:34:17', '2017-01-29 02:34:17'),
(609, 143, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 03:52:31', '2017-01-29 03:52:31'),
(610, 143, 'Hải Phòng, Vietnam', 1, 'N', '2017-01-29 03:52:44', '2017-01-29 03:52:44'),
(611, 143, 'Long Beach, CA, United States', 2, 'N', '2017-01-29 03:53:34', '2017-01-29 03:53:34'),
(612, 143, 'Auckland, New Zealand', 1, 'N', '2017-01-29 03:54:09', '2017-01-29 03:54:09'),
(613, 143, 'Hikuai, Waikato, New Zealand', 2, 'N', '2017-01-29 03:54:31', '2017-01-29 03:54:31'),
(614, 143, 'Taupo, Waikato, New Zealand', 2, 'N', '2017-01-29 03:54:48', '2017-01-29 03:54:48'),
(615, 143, 'Tongariro National Park, Waikato, New Zealand', 2, 'N', '2017-01-29 03:55:04', '2017-01-29 03:55:04'),
(616, 143, 'Wellington, New Zealand', 2, 'N', '2017-01-29 03:55:17', '2017-01-29 03:55:17'),
(617, 143, 'Kaitaia, Northland, New Zealand', 2, 'N', '2017-01-29 03:55:31', '2017-01-29 03:55:31'),
(618, 143, 'Queenstown, Otago, New Zealand', 2, 'N', '2017-01-29 03:56:00', '2017-01-29 03:56:00'),
(619, 143, 'Dunedin, Otago, New Zealand', 2, 'N', '2017-01-29 03:56:12', '2017-01-29 03:56:12'),
(620, 144, 'Tokyo, Japan', 1, 'N', '2017-01-29 04:09:30', '2017-01-29 04:11:30'),
(622, 144, 'Ho Chi Minh, Vietnam', 1, 'Y', '2017-01-29 04:09:51', '2017-01-29 04:11:30'),
(623, 144, 'Hải Phòng, Vietnam', 1, 'N', '2017-01-29 04:14:41', '2017-01-29 04:14:41'),
(624, 145, 'Quezon City, NCR, Philippines', 1, 'Y', '2017-01-29 06:21:51', '2017-01-29 06:21:51'),
(625, 145, 'Hà Nội, Vietnam', 1, 'N', '2017-01-29 06:22:01', '2017-01-29 06:22:01'),
(626, 145, 'Tagaytay, Calabarzon, Philippines', 2, 'N', '2017-01-29 06:22:35', '2017-01-29 06:22:35'),
(627, 145, 'Manila, NCR, Philippines', 2, 'N', '2017-01-29 06:22:50', '2017-01-29 06:22:50'),
(628, 145, 'Naga City, Naga, Bicol, Philippines', 2, 'N', '2017-01-29 06:24:10', '2017-01-29 06:24:10'),
(629, 145, 'Singapore', 2, 'N', '2017-01-29 06:24:28', '2017-01-29 06:24:28'),
(630, 146, 'Phu Tho Province, Vietnam', 1, 'N', '2017-01-29 06:31:05', '2017-01-29 06:31:20'),
(631, 146, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 06:31:14', '2017-01-29 06:31:20'),
(632, 147, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 06:35:58', '2017-01-29 06:35:58'),
(633, 147, 'Hải Dương, Hai Duong, Vietnam', 1, 'N', '2017-01-29 06:36:14', '2017-01-29 06:36:14'),
(634, 147, 'Bangkok Thailand', 2, 'N', '2017-01-29 06:36:55', '2017-01-29 06:36:55'),
(635, 147, 'Singapore', 2, 'N', '2017-01-29 06:37:07', '2017-01-29 06:37:07'),
(636, 147, 'Seoul, South Korea', 2, 'N', '2017-01-29 06:37:18', '2017-01-29 06:37:18'),
(637, 148, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 06:45:29', '2017-01-29 06:45:29'),
(638, 148, 'Amsterdam, Netherlands', 1, 'N', '2017-01-29 06:45:40', '2017-01-29 06:45:40'),
(639, 149, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 07:14:11', '2017-01-29 07:14:11'),
(640, 149, 'Hải Phòng, Vietnam', 1, 'N', '2017-01-29 07:14:21', '2017-01-29 07:14:21'),
(641, 149, 'Bangkok Thailand', 2, 'N', '2017-01-29 07:14:31', '2017-01-29 07:14:31'),
(642, 150, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 07:27:01', '2017-01-29 07:27:01'),
(643, 150, 'Hải Phòng, Vietnam', 1, 'N', '2017-01-29 07:27:12', '2017-01-29 07:27:12'),
(644, 151, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 07:35:22', '2017-01-29 07:35:22'),
(645, 151, 'Thái Bình, Thai Binh, Vietnam', 1, 'N', '2017-01-29 07:35:35', '2017-01-29 07:35:35'),
(646, 151, 'Singapore', 2, 'N', '2017-01-29 07:36:15', '2017-01-29 07:36:15'),
(647, 151, 'Mount Fuji, Fujinomiya, Shizuoka Prefecture, Japan', 2, 'N', '2017-01-29 07:36:39', '2017-01-29 07:36:39'),
(648, 151, 'Tokyo, Japan', 2, 'N', '2017-01-29 07:36:52', '2017-01-29 07:36:52'),
(649, 151, 'Nami Island, Chuncheon-si, Gangwon-do, South Korea', 2, 'N', '2017-01-29 07:37:22', '2017-01-29 07:37:22'),
(650, 152, 'Plymouth, United Kingdom', 1, 'Y', '2017-01-29 07:45:13', '2017-01-29 07:45:13'),
(651, 152, 'Torquay, United Kingdom', 2, 'N', '2017-01-29 07:46:37', '2017-01-29 07:46:37'),
(652, 152, 'Paignton, United Kingdom', 2, 'N', '2017-01-29 07:46:50', '2017-01-29 07:46:50'),
(653, 152, 'Liverpool, United Kingdom', 2, 'N', '2017-01-29 07:47:37', '2017-01-29 07:47:37'),
(654, 152, 'Bangkok Thailand', 2, 'N', '2017-01-29 07:47:58', '2017-01-29 07:47:58'),
(655, 152, 'Na Muang Ratchaburi Thailand', 2, 'N', '2017-01-29 07:48:26', '2017-01-29 07:48:26'),
(656, 152, 'Phuket Thailand', 2, 'N', '2017-01-29 07:48:41', '2017-01-29 07:48:41'),
(657, 152, 'Chiang Mai Thailand', 2, 'N', '2017-01-29 07:49:23', '2017-01-29 07:49:23'),
(658, 152, 'Sihanoukville, Cambodia', 2, 'N', '2017-01-29 07:49:41', '2017-01-29 07:49:41'),
(659, 152, 'Hà Nội, Vietnam', 2, 'N', '2017-01-29 07:50:11', '2017-01-29 07:50:11'),
(660, 152, 'Sa Pa, Lao Cai, Vietnam', 2, 'N', '2017-01-29 07:50:21', '2017-01-29 07:50:21'),
(661, 152, 'Da Nang, Vietnam', 2, 'N', '2017-01-29 07:50:31', '2017-01-29 07:50:31'),
(662, 152, 'Nha Trang, Khanh Hoa Province, Vietnam', 2, 'N', '2017-01-29 07:50:40', '2017-01-29 07:50:40'),
(663, 152, 'Ho Chi Minh, Vietnam', 2, 'N', '2017-01-29 07:50:51', '2017-01-29 07:50:51'),
(664, 153, 'London, United Kingdom', 1, 'Y', '2017-01-29 08:09:04', '2017-01-29 08:09:04'),
(665, 153, 'Hanoi, Vietnam', 2, 'N', '2017-01-29 08:09:26', '2017-01-29 08:09:26'),
(666, 153, 'Da Nang, Vietnam', 2, 'N', '2017-01-29 08:09:35', '2017-01-29 08:09:35'),
(667, 153, 'Nha Trang, Khanh Hoa Province, Vietnam', 2, 'N', '2017-01-29 08:09:45', '2017-01-29 08:09:45'),
(668, 153, 'Ho Chi Minh, Vietnam', 2, 'N', '2017-01-29 08:09:55', '2017-01-29 08:09:55'),
(669, 153, 'Bangkok Thailand', 2, 'N', '2017-01-29 08:10:10', '2017-01-29 08:10:10'),
(670, 153, 'Sa Pa, Lao Cai, Vietnam', 2, 'N', '2017-01-29 08:10:21', '2017-01-29 08:10:21'),
(671, 153, 'Phuket Thailand', 2, 'N', '2017-01-29 08:10:55', '2017-01-29 08:10:55'),
(672, 153, 'Chiang Mai Thailand', 2, 'N', '2017-01-29 08:11:44', '2017-01-29 08:11:44'),
(673, 153, 'Sihanoukville, Cambodia', 2, 'N', '2017-01-29 08:11:55', '2017-01-29 08:11:55'),
(674, 154, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 08:19:25', '2017-01-29 08:19:25'),
(675, 154, 'Quảng Ninh Province, Vietnam', 1, 'N', '2017-01-29 08:19:36', '2017-01-29 08:19:36'),
(676, 154, 'Bali, Indonesia', 2, 'N', '2017-01-29 08:41:04', '2017-01-29 08:41:04'),
(678, 154, 'Angkor Wat, Krong Siem Reap, Siem Reap Province, Cambodia', 2, 'N', '2017-01-29 08:42:07', '2017-01-29 08:42:07'),
(679, 154, 'Bangkok Thailand', 2, 'N', '2017-01-29 08:42:25', '2017-01-29 08:42:25'),
(680, 154, 'Pattaya City Chon Buri Thailand', 2, 'N', '2017-01-29 08:42:34', '2017-01-29 08:42:34'),
(681, 154, 'Phnom Penh, Cambodia', 2, 'N', '2017-01-29 08:42:46', '2017-01-29 08:42:46'),
(682, 155, 'Vientiane Province, Laos', 1, 'Y', '2017-01-29 08:55:59', '2017-01-29 08:55:59'),
(683, 155, 'Pakse, Champasak Province, Laos', 2, 'N', '2017-01-29 08:56:48', '2017-01-29 08:56:48'),
(684, 155, 'Bangkok Thailand', 2, 'N', '2017-01-29 08:57:06', '2017-01-29 08:57:06'),
(685, 155, 'Hua Hin Prachuap Khiri Khan Thailand', 2, 'N', '2017-01-29 08:57:16', '2017-01-29 08:57:16'),
(686, 155, 'Phuket Thailand', 2, 'N', '2017-01-29 08:57:31', '2017-01-29 08:57:31'),
(687, 155, 'Sydney, New South Wales, Australia', 2, 'N', '2017-01-29 08:57:54', '2017-01-29 08:57:54'),
(688, 155, 'Tokyo, Japan', 2, 'N', '2017-01-29 08:58:05', '2017-01-29 08:58:05'),
(689, 156, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 09:03:03', '2017-01-29 09:03:03'),
(690, 156, 'Missouri, United States', 1, 'N', '2017-01-29 09:04:00', '2017-01-29 09:04:00'),
(691, 156, 'Columbia, MD, United States', 2, 'N', '2017-01-29 09:04:25', '2017-01-29 09:04:25'),
(692, 156, 'Dubai - United Arab Emirates', 2, 'N', '2017-01-29 09:04:38', '2017-01-29 09:04:38'),
(693, 156, 'Abu Dhabi - United Arab Emirates', 2, 'N', '2017-01-29 09:04:47', '2017-01-29 09:04:47'),
(694, 156, 'Singapore', 2, 'N', '2017-01-29 09:04:58', '2017-01-29 09:04:58'),
(695, 157, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 09:10:36', '2017-01-29 09:10:36'),
(696, 157, 'Hải Phòng, Vietnam', 1, 'N', '2017-01-29 09:10:45', '2017-01-29 09:10:45'),
(697, 157, 'Manila, NCR, Philippines', 2, 'N', '2017-01-29 09:10:54', '2017-01-29 09:10:54'),
(698, 157, 'Boracay, Malay, Western Visayas, Philippines', 2, 'N', '2017-01-29 09:11:05', '2017-01-29 09:11:05'),
(699, 157, 'Angeles City, Central Luzon, Philippines', 2, 'N', '2017-01-29 09:11:15', '2017-01-29 09:11:15'),
(700, 157, 'Singapore', 2, 'N', '2017-01-29 09:11:42', '2017-01-29 09:11:42'),
(701, 157, 'Pakse, Champasak Province, Laos', 2, 'N', '2017-01-29 09:11:57', '2017-01-29 09:11:57'),
(702, 157, 'Dubai - United Arab Emirates', 2, 'N', '2017-01-29 09:12:10', '2017-01-29 09:12:10'),
(703, 157, 'Bangkok Thailand', 2, 'N', '2017-01-29 09:12:19', '2017-01-29 09:12:19'),
(704, 157, 'Pattaya City Chon Buri Thailand', 2, 'N', '2017-01-29 09:12:32', '2017-01-29 09:12:32'),
(705, 158, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 19:24:41', '2017-01-29 19:24:41'),
(706, 158, 'Birmingham, United Kingdom', 1, 'N', '2017-01-29 19:24:54', '2017-01-29 19:24:54'),
(707, 158, 'Phnom Penh, Cambodia', 2, 'N', '2017-01-29 19:25:38', '2017-01-29 19:25:38'),
(708, 158, 'Koh Rong, Krong Preah Sihanouk, Sihanoukville, Cambodia', 2, 'N', '2017-01-29 19:25:57', '2017-01-29 19:25:57'),
(709, 158, 'Siem Reap, Siem Reap Province, Cambodia', 2, 'N', '2017-01-29 19:26:47', '2017-01-29 19:26:47'),
(710, 158, 'Singapore', 2, 'N', '2017-01-29 19:27:02', '2017-01-29 19:27:02'),
(711, 158, 'Melaka Malaysia', 2, 'N', '2017-01-29 19:27:30', '2017-01-29 19:27:30'),
(712, 158, 'Penang Malaysia', 2, 'N', '2017-01-29 19:27:38', '2017-01-29 19:27:38'),
(713, 158, 'Kuala Lumpur Federal Territory of Kuala Lumpur Malaysia', 2, 'N', '2017-01-29 19:27:48', '2017-01-29 19:27:48'),
(714, 158, 'Yangon, Yangon Region, Myanmar (Burma)', 2, 'N', '2017-01-29 19:28:01', '2017-01-29 19:28:01'),
(715, 158, 'Bagan, Mandalay Region, Myanmar (Burma)', 2, 'N', '2017-01-29 19:28:23', '2017-01-29 19:28:23'),
(716, 158, 'Inle Lake, Taunggyi, Shan, Myanmar (Burma)', 2, 'N', '2017-01-29 19:28:44', '2017-01-29 19:28:44'),
(717, 158, 'Geneva, Switzerland', 2, 'N', '2017-01-29 19:30:05', '2017-01-29 19:30:05'),
(718, 158, 'Lucerne, Switzerland', 2, 'N', '2017-01-29 19:30:28', '2017-01-29 19:30:28'),
(719, 158, 'Frankfurt, Germany', 2, 'N', '2017-01-29 19:32:02', '2017-01-29 19:32:02'),
(720, 158, 'Luxembourg', 2, 'N', '2017-01-29 19:32:16', '2017-01-29 19:32:16'),
(721, 158, 'Paris, France', 2, 'N', '2017-01-29 19:32:29', '2017-01-29 19:32:29'),
(722, 158, 'Brussels, Belgium', 2, 'N', '2017-01-29 19:32:43', '2017-01-29 19:32:43'),
(723, 158, 'Amsterdam, Netherlands', 2, 'N', '2017-01-29 19:32:53', '2017-01-29 19:32:53'),
(724, 158, 'Cambridge, United Kingdom', 2, 'N', '2017-01-29 19:33:08', '2017-01-29 19:33:08'),
(725, 158, 'London, United Kingdom', 2, 'N', '2017-01-29 19:33:17', '2017-01-29 19:33:17'),
(726, 158, 'Manchester, United Kingdom', 2, 'N', '2017-01-29 19:33:32', '2017-01-29 19:33:32'),
(727, 158, 'Edinburgh, United Kingdom', 2, 'N', '2017-01-29 19:33:44', '2017-01-29 19:33:44'),
(728, 158, 'Scotland, United Kingdom', 2, 'N', '2017-01-29 19:33:55', '2017-01-29 19:33:55'),
(729, 159, 'Hà Nội, Vietnam', 1, 'N', '2017-01-29 19:46:45', '2017-01-29 20:07:54'),
(730, 159, 'Thái Nguyên, Thai Nguyen, Vietnam', 1, 'N', '2017-01-29 19:46:59', '2017-01-29 20:07:54'),
(732, 159, 'Prague, Czechia', 1, 'N', '2017-01-29 20:06:48', '2017-01-29 20:07:54'),
(733, 159, 'Ho Chi Minh, Vietnam', 1, 'Y', '2017-01-29 20:07:47', '2017-01-29 20:07:54'),
(734, 160, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 20:13:50', '2017-01-29 20:13:50'),
(735, 161, 'New York, United States', 1, 'Y', '2017-01-29 20:22:31', '2017-01-29 20:22:31'),
(736, 161, 'Hà Nội, Vietnam', 1, 'N', '2017-01-29 20:22:50', '2017-01-29 20:22:50'),
(737, 161, 'Washington, United States', 2, 'N', '2017-01-29 20:24:02', '2017-01-29 20:24:02'),
(741, 162, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 20:41:52', '2017-01-29 20:42:48'),
(742, 162, 'Hải Phòng, Vietnam', 1, 'N', '2017-01-29 20:42:38', '2017-01-29 20:42:47'),
(743, 163, 'Hà Nội, Vietnam', 1, 'Y', '2017-01-29 20:46:34', '2017-01-29 20:46:34');

-- --------------------------------------------------------

--
-- Table structure for table `work_experiences`
--

CREATE TABLE `work_experiences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` int(10) UNSIGNED NOT NULL,
  `from_month` int(10) UNSIGNED NOT NULL,
  `from_year` int(10) UNSIGNED NOT NULL,
  `to_month` int(10) UNSIGNED NOT NULL,
  `to_year` int(10) UNSIGNED NOT NULL,
  `status` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y' COMMENT 'Y = Active, N = Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `work_experiences`
--

INSERT INTO `work_experiences` (`id`, `user_id`, `title`, `company`, `department`, `city`, `country`, `from_month`, `from_year`, `to_month`, `to_year`, `status`, `created_at`, `updated_at`) VALUES
(343, 102, 'Web Developer', 'East Agile', '', 'Hà Nội, Việt Nam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-26 09:10:43', '2017-01-26 09:10:43'),
(344, 103, 'Purchasing Agent', 'LG Electronics', '', 'Hải Phòng, Việt Nam', 0, 4, 2014, 6, 2014, 'Y', '2017-01-26 09:25:52', '2017-01-26 09:26:08'),
(345, 103, 'Marketing', ' America Academy', '', 'Hải Phòng, Việt Nam', 0, 5, 2013, 12, 2013, 'Y', '2017-01-26 09:27:13', '2017-01-26 09:27:13'),
(346, 104, 'Auditor', 'KPMG', '', 'Hà Nội, Việt Nam', 0, 0, 2013, 0, 2017, 'Y', '2017-01-26 09:38:42', '2017-01-26 09:38:42'),
(347, 104, 'ERP Consultant', 'Global FIS', '', 'Hồ Chí Minh, Việt Nam', 0, 0, 2012, 0, 2013, 'Y', '2017-01-26 09:39:39', '2017-01-26 09:39:39'),
(348, 105, 'Business Development Management Trainee', 'Blitz IT Consultants Pte Ltd', '', 'Hà Nội, Việt Nam', 0, 0, 2015, 0, 2017, 'Y', '2017-01-26 09:47:07', '2017-01-26 09:47:07'),
(349, 105, 'Wap Development Team', 'iNET Corporation', '', 'Hà Nội, Việt Nam', 0, 0, 2013, 0, 2015, 'Y', '2017-01-26 09:47:58', '2017-01-26 09:47:58'),
(350, 106, 'Blogger', 'Vegan Kitty Cat', '', 'Hà Nội, Việt Nam', 0, 0, 2016, 0, 2017, 'Y', '2017-01-26 09:58:06', '2017-01-26 09:58:06'),
(351, 106, 'Facilitator', 'Knowmads Hanoi', '', 'Hà Nội, Việt Nam', 0, 0, 2016, 0, 2017, 'Y', '2017-01-26 09:58:41', '2017-01-26 09:58:41'),
(352, 107, 'Event Sales Executive', 'JW Marriott Hanoi', '', 'Hà Nội, Việt Nam', 0, 0, 2013, 0, 2015, 'Y', '2017-01-27 07:55:59', '2017-01-27 07:55:59'),
(353, 107, 'Culi chạy việc', 'Delisa - Fresh Salad Bar', '', 'Hà Nội, Việt Nam', 0, 9, 2016, 0, 2017, 'Y', '2017-01-27 07:56:47', '2017-01-27 07:57:03'),
(354, 108, 'Market Underwriter', 'Swiss Re', '', 'Singapore', 0, 9, 2014, 0, 2017, 'Y', '2017-01-27 08:37:41', '2017-01-27 08:37:41'),
(355, 108, 'Account Strategist', 'Google', '', 'Singapore', 0, 0, 2012, 0, 2014, 'Y', '2017-01-27 08:38:44', '2017-01-27 08:38:44'),
(356, 109, 'Acting Head of Fashion', 'Adayroi - Vingroup', '', 'Hà Nội, Việt Nam', 0, 9, 2016, 0, 2017, 'Y', '2017-01-27 08:46:34', '2017-01-27 08:46:34'),
(357, 109, 'Business Development Manager', 'Adayroi - Vingroup', '', 'Hà Nội, Việt Nam', 0, 4, 2016, 8, 2016, 'Y', '2017-01-27 08:47:14', '2017-01-27 08:47:14'),
(358, 109, 'Executive Assistant to CEO', 'VinEcom - VinGroup', '', 'Hà Nội, Việt Nam', 0, 4, 2015, 3, 2016, 'Y', '2017-01-27 08:47:52', '2017-01-27 08:47:52'),
(359, 109, 'Key account planning', 'Công ty TNHH Quốc tế Unilever Việt Nam', '', 'Hồ Chí Minh, Việt Nam', 0, 1, 2014, 3, 2015, 'Y', '2017-01-27 08:48:32', '2017-01-27 08:48:32'),
(360, 109, 'Management Trainee', 'Công ty TNHH Quốc tế Unilever Việt Nam', '', 'Hồ Chí Minh, Việt Nam', 0, 0, 2013, 2, 2015, 'Y', '2017-01-27 08:49:22', '2017-01-27 08:49:22'),
(361, 110, 'Packaging Solution Consultant', 'IBM', '', 'Tokyo, Nhật Bản', 0, 4, 2015, 0, 2017, 'Y', '2017-01-27 09:08:39', '2017-01-27 09:08:39'),
(362, 111, 'Auditor', 'Ernst & Young', '', 'Hà Nội, Việt Nam', 0, 0, 2012, 0, 2016, 'Y', '2017-01-27 20:11:20', '2017-01-27 20:11:20'),
(363, 111, 'General Director Assistant', 'Netco', '', 'Hà Nội, Việt Nam', 0, 0, 2016, 0, 2016, 'Y', '2017-01-27 20:12:06', '2017-01-27 20:12:06'),
(364, 112, 'Marketing', 'Habubank', '', 'Hà Nội, Việt Nam', 0, 0, 2009, 0, 2011, 'Y', '2017-01-27 20:25:40', '2017-01-27 20:25:40'),
(365, 112, 'Marketing', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Khối bán lẻ', 'Hà Nội, Việt Nam', 0, 0, 2011, 0, 2016, 'Y', '2017-01-27 20:26:29', '2017-01-27 20:26:29'),
(366, 112, 'Marketing', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng PTSP&Marketing - Khối Khách hàng doanh nghiệp', 'Hà Nội, Việt Nam', 0, 0, 2016, 0, 2017, 'Y', '2017-01-27 20:27:20', '2017-01-27 20:27:20'),
(367, 113, 'Marketing & Event Planner', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng PTSP&Marketing - Khối Khách hàng doanh nghiệp', 'Hà Nội, Việt Nam', 0, 0, 2015, 0, 2017, 'Y', '2017-01-27 20:39:55', '2017-01-27 20:39:55'),
(368, 113, 'Marketing', 'Viettel', '', 'Hà Nội, Việt Nam', 0, 0, 2014, 0, 2015, 'Y', '2017-01-27 20:41:07', '2017-01-27 20:41:07'),
(369, 114, 'Unknown', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng Khách hàng FDI & Nguồn vốn quốc tế', 'Hà Nội, Vietnam', 0, 0, 2016, 0, 2017, 'Y', '2017-01-27 21:02:32', '2017-01-27 21:02:32'),
(370, 115, 'Nhân viên hỗ trợ bán lẻ', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Văn phòng đại diện miền Nam', 'Ho Chi Minh, Vietnam', 0, 0, 2012, 0, 2017, 'Y', '2017-01-27 22:06:43', '2017-01-27 22:06:43'),
(371, 116, 'Management Trainee - Supply Chain', 'Công ty TNHH Quốc tế Unilever Việt Nam', '', 'Ho Chi Minh, Vietnam', 0, 0, 2012, 0, 2017, 'Y', '2017-01-27 22:14:09', '2017-01-27 22:14:09'),
(372, 118, 'Auditor', 'Deloitte', '', 'Hà Nội, Vietnam', 0, 8, 2012, 4, 2013, 'Y', '2017-01-27 22:27:54', '2017-01-27 22:27:54'),
(373, 119, 'Cán bộ quan hệ khách hàng', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng Khách hàng doanh nghiệp lớn', 'Hà Nội, Vietnam', 0, 0, 2013, 0, 2017, 'Y', '2017-01-28 02:58:49', '2017-01-28 02:58:49'),
(374, 120, 'Coordinator', 'Rolls-Royce Motor Cars ', '', 'Hà Nội, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-28 03:05:11', '2017-01-28 03:05:11'),
(375, 120, 'Project Executive', 'DHN Holdings', '', 'Hà Nội, Vietnam', 0, 3, 2014, 7, 2014, 'Y', '2017-01-28 03:06:13', '2017-01-28 03:06:13'),
(376, 120, 'Vietnamese Interpreter', 'Medic Interpreter', '', 'Hà Nội, Vietnam', 0, 7, 2013, 1, 2014, 'Y', '2017-01-28 03:07:10', '2017-01-28 03:07:10'),
(377, 120, 'Website Management and Director Assistant', 'Dai Luc Jsc', '', 'Hà Nội, Vietnam', 0, 4, 2011, 7, 2011, 'Y', '2017-01-28 03:07:59', '2017-01-28 03:07:59'),
(378, 121, 'Giảng viên ', 'Đại học Kinh tế quốc dân', 'Khoa bảo hiểm', 'Hà Nội, Vietnam', 0, 0, 2012, 0, 2017, 'Y', '2017-01-28 03:23:53', '2017-01-28 03:23:53'),
(379, 122, 'Trade Finance', 'Ngân hàng Standard Chartered Việt Nam', '', 'Hà Nội, Vietnam', 0, 0, 2012, 0, 2016, 'Y', '2017-01-28 03:35:58', '2017-01-28 03:35:58'),
(380, 123, 'Giảng viên', 'Đại học Kinh tế quốc dân', 'Khoa Tài chính - Ngân hàng', 'Hà Nội, Vietnam', 0, 0, 2012, 0, 2017, 'Y', '2017-01-28 06:42:50', '2017-01-28 06:42:50'),
(381, 123, 'Vietnamese - English Interpreter', 'VoiceOver Interpreting', '', 'Glasgow, United Kingdom', 0, 0, 2012, 0, 2013, 'Y', '2017-01-28 06:43:41', '2017-01-28 06:43:41'),
(382, 123, 'Trainee', 'Aon Risk Solutions', 'Health and Benefits Department', 'Hà Nội, Vietnam', 0, 0, 2011, 0, 2012, 'Y', '2017-01-28 06:44:54', '2017-01-28 06:44:54'),
(383, 123, 'Trainee Manager', 'Students In Free Enterprise (SIFE)', '', 'Hà Nội, Vietnam', 0, 0, 2009, 0, 2011, 'Y', '2017-01-28 06:46:01', '2017-01-28 06:46:01'),
(384, 124, 'Associate, Advisory', 'KPMG US', '', 'New York, United States', 0, 0, 2014, 0, 2017, 'Y', '2017-01-28 07:04:20', '2017-01-28 07:04:20'),
(385, 124, 'Risk Consultant', 'Office of State Comptroller', '', 'Hartford, CT, United States', 0, 8, 2013, 12, 2013, 'Y', '2017-01-28 07:05:14', '2017-01-28 07:05:14'),
(386, 124, 'Tutor', 'UConn School of Business', '', 'Stamford, CT, United States', 0, 12, 2012, 5, 2013, 'Y', '2017-01-28 07:06:09', '2017-01-28 07:07:28'),
(387, 124, 'Underwriter', 'Ngân hàng Thương mại cổ phần Ngoại thương Việt Nam (VIETCOMBANK)', '', 'Hà Nội, Vietnam', 0, 1, 2012, 5, 2012, 'Y', '2017-01-28 07:07:01', '2017-01-28 07:07:01'),
(388, 124, 'Accountant', 'Manulife Financial', '', 'Hà Nội, Vietnam', 0, 11, 2010, 11, 2011, 'Y', '2017-01-28 07:08:08', '2017-01-28 07:08:08'),
(389, 125, 'Head of Brand Marketing ', 'Lazada Group', '', 'Ho Chi Minh, Vietnam', 0, 9, 2016, 0, 2017, 'Y', '2017-01-28 08:15:32', '2017-01-28 08:15:32'),
(390, 125, 'Campaign Manager', 'Lazada Group', '', 'Ho Chi Minh, Vietnam', 0, 2, 2016, 8, 2016, 'Y', '2017-01-28 08:16:03', '2017-01-28 08:16:03'),
(391, 125, 'Business Development Manager', 'VinEcom - VinGroup', '', 'Ho Chi Minh City, Ho Chi Minh, Vietnam', 0, 6, 2015, 11, 2015, 'Y', '2017-01-28 08:16:35', '2017-01-28 08:16:35'),
(392, 128, 'Experience Senior Executive, Lease Management', 'JLL', '', 'Singapore', 0, 9, 2013, 0, 2017, 'Y', '2017-01-28 08:49:44', '2017-01-28 08:49:44'),
(393, 128, 'Vice President', 'Vietnamese Students\' Association in NUS (VNCNUS)', '', 'Singapore', 0, 9, 2010, 9, 2011, 'Y', '2017-01-28 08:50:53', '2017-01-28 08:50:53'),
(394, 128, 'Event Executive', 'NUS Entrepreneurship Society', '', 'Singapore', 0, 8, 2010, 6, 2011, 'Y', '2017-01-28 08:51:40', '2017-01-28 08:51:40'),
(395, 129, 'Unknown', 'Continental Automotive', '', 'Hà Nội, Vietnam', 0, 4, 2015, 0, 2017, 'Y', '2017-01-28 09:00:55', '2017-01-28 09:00:55'),
(396, 130, 'Ngân hàng điện tử', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng PTSP&Marketing - Khối KHDN', 'Hà Nội, Vietnam', 0, 0, 2013, 0, 2017, 'Y', '2017-01-28 09:09:04', '2017-01-28 09:10:10'),
(397, 131, 'Chuyên viên lãi suất', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng Quản lý cân đối vốn & Kế hoạch tài chính', 'Hà Nội, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-28 09:19:31', '2017-01-28 09:19:31'),
(398, 132, 'Chuyên viên sản phẩm tín dụng', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng PTSP&Marketing - Khối KHDN', 'Hà Nội, Vietnam', 0, 0, 2012, 0, 2017, 'Y', '2017-01-28 09:29:21', '2017-01-28 09:29:21'),
(399, 132, 'Chuyên viên tín dụng', 'Ngân hàng Kỹ Thương Việt Nam', '', 'Hà Nội, Vietnam', 0, 0, 2010, 0, 2012, 'Y', '2017-01-28 09:30:42', '2017-01-28 09:30:42'),
(400, 132, 'Quản lý', 'Công ty TNHH T&A HC', '', 'Hà Nội, Vietnam', 0, 0, 2008, 0, 2010, 'Y', '2017-01-28 09:31:26', '2017-01-28 09:31:26'),
(401, 132, 'Accountant', 'Radum Pty Ltd', '', 'Northlink Place, Virginia, Queensland, Australia', 0, 0, 2006, 0, 2007, 'Y', '2017-01-28 09:33:39', '2017-01-28 09:33:39'),
(402, 133, 'Junior Accountant', 'F I P Electrical', '', 'Sydney, New South Wales, Australia', 0, 3, 2016, 0, 2017, 'Y', '2017-01-28 20:03:16', '2017-01-28 20:03:16'),
(403, 133, 'Floor Manager', 'Khing Thai Restaurant', '', 'Sydney, New South Wales, Australia', 0, 3, 2015, 2, 2016, 'Y', '2017-01-28 20:04:06', '2017-01-28 20:04:06'),
(404, 134, 'Unilever Campus Ambassador', 'Công ty TNHH Quốc tế Unilever Việt Nam', '', 'Hà Nội, Vietnam', 0, 3, 2013, 4, 2014, 'Y', '2017-01-28 20:15:13', '2017-01-28 20:15:13'),
(405, 135, 'Chuyên viên phân tích tài chính', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng Kế hoạch & Quản lý thông tin khách hàng', 'Hà Nội, Vietnam', 0, 4, 2015, 0, 2017, 'Y', '2017-01-28 20:26:20', '2017-01-28 20:26:20'),
(406, 135, 'Chuyên viên phân tích tài chính', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng Quản lý cân đối vốn & Kế hoạch tài chính', 'Hà Nội, Vietnam', 0, 5, 2011, 4, 2015, 'Y', '2017-01-28 20:27:08', '2017-01-28 20:27:08'),
(407, 136, 'Chuyên viên lãi suất', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng Quản lý cân đối vốn & Kế hoạch tài chính', 'Hà Nội, Vietnam', 0, 0, 2013, 0, 2017, 'Y', '2017-01-28 20:37:44', '2017-01-28 20:37:44'),
(408, 137, 'Learning & Development Manager ', 'TH Group - Food Chain', '', 'Hà Nội, Vietnam', 0, 2, 2016, 0, 2017, 'Y', '2017-01-28 21:01:59', '2017-01-28 21:01:59'),
(409, 137, 'Learning & Development Executive', 'TH Group - Food Chain', '', 'Hà Nội, Vietnam', 0, 7, 2015, 1, 2016, 'Y', '2017-01-28 21:02:43', '2017-01-28 21:02:43'),
(410, 137, 'Management Trainee', 'TH Group - Food Chain', '', 'Ho Chi Minh, Vietnam', 0, 2, 2014, 6, 2015, 'Y', '2017-01-28 21:03:31', '2017-01-28 21:03:31'),
(411, 137, 'Assistant to Chairwoman and Project Assistant', 'TH Group - TH Herbals Joint Stock Company', '', 'Ho Chi Minh, Vietnam', 0, 10, 2013, 2, 2014, 'Y', '2017-01-28 21:04:10', '2017-01-28 21:04:10'),
(412, 137, 'ERP Consultant and Implementor', 'FPT Information System', '', 'Hà Nội & TP. Hồ Chí Minh ', 0, 6, 2012, 5, 2013, 'Y', '2017-01-28 21:05:24', '2017-01-28 21:05:24'),
(413, 137, 'Intern Assistant to COO', 'FPT Corporation', '', 'Hà Nội, Vietnam', 0, 2, 2012, 5, 2012, 'Y', '2017-01-28 21:06:18', '2017-01-28 21:06:18'),
(414, 138, 'Deputy Chief Executive Officer', 'Hestia', '', 'Hà Nội', 0, 9, 2016, 0, 2017, 'Y', '2017-01-28 21:39:33', '2017-01-28 21:39:33'),
(415, 138, 'Chief Investment Officer', 'Passion Invesment', '', 'Hà Nội, Vietnam', 0, 11, 2015, 0, 2017, 'Y', '2017-01-28 21:40:09', '2017-01-28 21:40:09'),
(416, 138, 'Equity Analyst', 'Bao Viet Fund', '', 'Hà Nội, Vietnam', 0, 3, 2014, 11, 2015, 'Y', '2017-01-28 21:41:02', '2017-01-28 21:41:02'),
(417, 139, 'Social Media Manager', 'Appota Corp', '', 'Hà Nội, Vietnam', 0, 7, 2014, 0, 2017, 'Y', '2017-01-28 21:49:34', '2017-01-28 21:49:34'),
(418, 139, 'Media Manager', 'Appota Corp', '', 'Hà Nội, Vietnam', 0, 12, 2013, 7, 2014, 'Y', '2017-01-28 21:50:18', '2017-01-28 21:50:18'),
(419, 139, 'Marketing Executives', 'Appota Corp', '', 'Hà Nội, Vietnam', 0, 8, 2012, 3, 2014, 'Y', '2017-01-28 21:50:55', '2017-01-28 21:50:55'),
(420, 140, 'Master Product Manager', 'VNPT VinaPhone', '', 'Hà Nội, Vietnam', 0, 6, 2016, 0, 2017, 'Y', '2017-01-28 21:56:49', '2017-01-28 21:56:49'),
(421, 140, 'Marketing Executive', 'VDC / VNPT', '', 'Hà Nội, Vietnam', 0, 10, 2008, 6, 2015, 'Y', '2017-01-28 21:57:37', '2017-01-28 21:57:37'),
(422, 141, 'Business Development Manager', 'VNPT Technology', '', 'Hà Nội, Vietnam', 0, 3, 2016, 0, 2017, 'Y', '2017-01-29 02:28:31', '2017-01-29 02:28:31'),
(423, 141, 'Creative Director', 'Move it Project', 'Head of Event Development', 'Hà Nội, Vietnam', 0, 10, 2012, 5, 2013, 'Y', '2017-01-29 02:29:31', '2017-01-29 02:29:31'),
(424, 142, 'Associate - Transaction Services', 'PwC Vietnam', '', 'Hà Nội, Vietnam', 0, 8, 2012, 12, 2013, 'Y', '2017-01-29 02:35:45', '2017-01-29 02:35:45'),
(425, 142, 'IT', 'TOPICA Group', '', 'Hà Nội, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-29 02:36:16', '2017-01-29 02:36:16'),
(426, 142, 'Vice President - R&D', 'SIFE NEU', '', 'Hà Nội, Vietnam', 0, 10, 2009, 3, 2011, 'Y', '2017-01-29 02:37:13', '2017-01-29 02:37:13'),
(427, 143, 'Giảng viên', 'Đại học kinh tế quốc dân', '', 'Hà Nội, Vietnam', 0, 8, 2012, 0, 2017, 'Y', '2017-01-29 03:57:55', '2017-01-29 03:58:43'),
(428, 143, 'Giảng viên', 'Viện đào tạo quốc tế - Đại học kinh tế quốc dân', '', 'Hà Nội, Vietnam', 0, 8, 2015, 0, 2017, 'Y', '2017-01-29 04:00:21', '2017-01-29 04:00:21'),
(429, 143, 'International Partnership Manager', 'Property & Management Company (PMC)', '', 'Hà Nội, Vietnam', 0, 12, 2015, 10, 2016, 'Y', '2017-01-29 04:01:04', '2017-01-29 04:01:04'),
(430, 143, 'Project Finance Consultant (Contractor)', 'Viet Nam Investment and Development Consultant Company Limited (IAC Vietnam)', '', 'Hà Nội, Vietnam', 0, 9, 2015, 8, 2016, 'Y', '2017-01-29 04:01:53', '2017-01-29 04:01:53'),
(431, 143, 'Assistant to COO (Chief Operating Officer)', 'VNP Group', '', 'Hà Nội, Vietnam', 0, 8, 2015, 12, 2015, 'Y', '2017-01-29 04:02:49', '2017-01-29 04:02:49'),
(432, 143, 'President', 'SIFE NEU', '', 'Hà Nội, Vietnam', 0, 2, 2010, 1, 2012, 'Y', '2017-01-29 04:03:44', '2017-01-29 04:03:44'),
(433, 144, 'Consultant', 'Link and Motivation Inc', '', 'Tokyo, Japan', 0, 0, 2013, 1, 2014, 'Y', '2017-01-29 04:11:08', '2017-01-29 04:11:08'),
(434, 145, 'Research Specialist', 'Industrial Information Resources', '', 'Quezon City, NCR, Philippines', 0, 0, 2016, 0, 2017, 'Y', '2017-01-29 06:26:16', '2017-01-29 06:26:16'),
(435, 147, 'Phó phòng bán lẻ', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'PGD Tràng An', 'Hà Nội, Vietnam', 0, 0, 2016, 0, 2017, 'Y', '2017-01-29 06:38:14', '2017-01-29 06:38:14'),
(436, 147, 'Chuyên viên chính sách', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Trung tâm thẻ', 'Hà Nội, Vietnam', 0, 0, 2012, 0, 2016, 'Y', '2017-01-29 06:38:46', '2017-01-29 06:38:46'),
(437, 148, 'Educational Entrepreneur', 'Self-Employment', '', 'Hà Nội, Vietnam', 0, 9, 2016, 0, 2017, 'Y', '2017-01-29 06:47:46', '2017-01-29 06:47:46'),
(438, 148, 'Personal Coach & Teacher', 'Self-Employment', '', 'Amsterdam, Netherlands', 0, 0, 2008, 0, 2017, 'Y', '2017-01-29 06:48:31', '2017-01-29 06:48:31'),
(439, 148, 'Intern', 'Thomas en Charles', '', 'Amsterdam, Netherlands', 0, 3, 2016, 9, 2016, 'Y', '2017-01-29 06:49:16', '2017-01-29 06:49:16'),
(440, 149, 'Purchasing Executive', 'Samsung Electronics', '', 'Bac Ninh Province, Vietnam', 0, 0, 2013, 0, 2017, 'Y', '2017-01-29 07:21:50', '2017-01-29 07:21:50'),
(441, 150, 'Administrator', 'iFACT', '', 'Hà Nội, Vietnam', 0, 0, 2013, 0, 2017, 'Y', '2017-01-29 07:28:25', '2017-01-29 07:28:25'),
(442, 151, 'Chuyên viên tín dụng', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng Khách hàng FDI & Nguồn vốn quốc tế', 'Hà Nội, Vietnam', 0, 1, 2015, 0, 2017, 'Y', '2017-01-29 07:39:39', '2017-01-29 07:39:39'),
(443, 151, 'Chuyên viên tín dụng', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng Khách hàng doanh nghiệp Lớn - Chi nhánh Ba Đình', 'Hà Nội, Vietnam', 0, 9, 2012, 1, 2015, 'Y', '2017-01-29 07:40:58', '2017-01-29 07:40:58'),
(444, 154, 'Chuyên viên marketing', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng PTSP&Marketing - Khối KHDN', 'Hà Nội, Vietnam', 0, 0, 2016, 0, 2017, 'Y', '2017-01-29 08:45:45', '2017-01-29 08:45:45'),
(445, 154, 'Chuyên viên marketing', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng Khách hàng ưu tiên - Khối bán lẻ', 'Hà Nội, Vietnam', 0, 6, 2011, 1, 2016, 'Y', '2017-01-29 08:46:42', '2017-01-29 08:46:42'),
(446, 154, 'Marketing', 'Habubank', '', 'Hà Nội, Vietnam', 0, 9, 2009, 5, 2011, 'Y', '2017-01-29 08:47:37', '2017-01-29 08:47:37'),
(447, 155, 'Cán bộ tín dụng bán lẻ', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng bán lẻ', 'Vientiane, Vientiane Prefecture, Laos', 0, 0, 2015, 0, 2017, 'Y', '2017-01-29 08:59:55', '2017-01-29 08:59:55'),
(448, 156, 'Cán bộ tín dụng', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng KHDN FDI - Chi nhánh Đống Đa', 'Hà Nội, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-29 09:06:47', '2017-01-29 09:06:47'),
(449, 157, 'Chuyên viên marketing', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng PTSP&Marketing - Khối KHDN', 'Hà Nội, Vietnam', 0, 10, 2014, 0, 2017, 'Y', '2017-01-29 09:13:11', '2017-01-29 09:13:11'),
(450, 157, 'Chuyên viên marketing', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng Dịch vụ ngân hàng điện tử', 'Hà Nội, Vietnam', 0, 10, 2013, 9, 2014, 'Y', '2017-01-29 09:13:56', '2017-01-29 09:13:56'),
(451, 157, 'Giám sát bán hàng', 'Công ty TNHH Quốc tế Unilever Việt Nam', '', 'Bac Ninh Province, Vietnam', 0, 12, 2012, 6, 2013, 'Y', '2017-01-29 09:14:40', '2017-01-29 09:14:40'),
(452, 158, 'Tỷ giá ngoại tệ', 'Ngân hàng nhà nước Việt Nam', '', 'Hà Nội, Vietnam', 0, 0, 2014, 0, 2017, 'Y', '2017-01-29 19:36:32', '2017-01-29 19:36:32'),
(453, 159, 'Experience Chief Technology Officer', 'LifeStyle Project Management (Timo)', '', 'Ho Chi Minh, Vietnam', 0, 3, 2015, 0, 2017, 'Y', '2017-01-29 19:50:03', '2017-01-29 19:50:03'),
(454, 159, 'Head of Alternative Channels (E-Banking)', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng Dịch vụ ngân hàng điện tử', 'Hà Nội, Vietnam', 0, 5, 2014, 3, 2015, 'Y', '2017-01-29 19:51:16', '2017-01-29 19:53:33'),
(455, 159, 'Deputy Head for E-Banking Services', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', 'Phòng Dịch vụ ngân hàng điện tử', 'Hà Nội, Vietnam', 0, 6, 2009, 5, 2014, 'Y', '2017-01-29 19:54:26', '2017-01-29 19:54:26'),
(456, 159, 'Head of Modernization Project Implementation Unit', 'Ngân hàng Thương mại Cổ phần Công thương Việt Nam (Vietinbank)', '', 'Hà Nội, Vietnam', 0, 4, 2010, 4, 2011, 'Y', '2017-01-29 19:55:16', '2017-01-29 19:55:16'),
(457, 159, 'Programme Manager, VN Board Member', 'Harvey Nash Software', '', 'Ho Chi Minh, Vietnam', 0, 9, 2004, 6, 2009, 'Y', '2017-01-29 19:56:39', '2017-01-29 19:56:39'),
(458, 159, 'Technical Director', 'VNET Group', '', 'Ho Chi Minh, Vietnam', 0, 2, 2004, 9, 2004, 'Y', '2017-01-29 19:57:55', '2017-01-29 19:57:55'),
(459, 159, 'Technical Director', 'Thai Nam Technology', '', 'Hà Nội, Vietnam', 0, 10, 2001, 2, 2004, 'Y', '2017-01-29 19:59:13', '2017-01-29 19:59:13'),
(460, 159, 'Project Leader', 'CSC Vietnam (formely PSV)', '', 'Hà Nội, Vietnam', 0, 3, 2001, 8, 2001, 'Y', '2017-01-29 20:00:00', '2017-01-29 20:00:00'),
(461, 159, 'Project Manager', 'Cygni Software', '', 'Prague, Czechia', 0, 9, 1995, 12, 2000, 'Y', '2017-01-29 20:02:59', '2017-01-29 20:02:59'),
(462, 160, 'Macro Economic Analyst', 'National Financial Supervisory Commission', '', 'Hà Nội, Vietnam', 0, 5, 2011, 0, 2017, 'Y', '2017-01-29 20:15:10', '2017-01-29 20:15:10'),
(463, 160, 'Business Management Officer', 'Ngân hàng Kỹ Thương Việt Nam (Techcombank)', '', 'Hà Nội, Vietnam', 0, 10, 2010, 4, 2011, 'Y', '2017-01-29 20:16:16', '2017-01-29 20:16:16'),
(464, 161, 'Experienced Associate (Financial Markets Group)', 'PwC', '', 'New York, United States', 0, 2, 2015, 0, 2017, 'Y', '2017-01-29 20:25:26', '2017-01-29 20:25:26'),
(465, 161, 'Director', 'Vietnam Investor Network', '', 'New York, United States', 0, 6, 2016, 0, 2017, 'Y', '2017-01-29 20:26:30', '2017-01-29 20:26:30'),
(466, 161, 'Investment Banking Intern', 'Focus Investment Banking', '', 'Washington, United States', 0, 9, 2014, 12, 2014, 'Y', '2017-01-29 20:27:26', '2017-01-29 20:27:39'),
(467, 161, 'Private Equity Intern', 'Royal Ascot Partners, LLC', '', 'Baltimore, MD, United States', 0, 3, 2014, 12, 2014, 'Y', '2017-01-29 20:28:41', '2017-01-29 20:28:41'),
(468, 161, 'Private Equity Summer Analyst', 'Antson Capital Partners, LLC', '', 'Baltimore, MD, United States', 0, 6, 2014, 9, 2014, 'Y', '2017-01-29 20:29:27', '2017-01-29 20:29:27'),
(469, 162, 'Senior Associate, Assurance Services', 'KPMG', '', 'Hà Nội, Vietnam', 0, 9, 2016, 0, 2017, 'Y', '2017-01-29 20:37:58', '2017-01-29 20:37:58'),
(470, 162, 'Associate, Assurance Services and Financial Advisory Services', 'KPMG', '', 'Hà Nội, Vietnam', 0, 9, 2014, 8, 2016, 'Y', '2017-01-29 20:38:43', '2017-01-29 20:38:43'),
(471, 162, 'Research Assistant in Corporate Social Responsibility (CSR) Research', 'Associate, Assurance Services and Financial Advisory Services', '', 'Hà Nội, Vietnam', 0, 7, 2010, 8, 2011, 'Y', '2017-01-29 20:39:30', '2017-01-29 20:39:30'),
(472, 163, 'Marketing Senior', 'Hino Motors Vietnam', '', 'Hà Nội, Vietnam', 0, 1, 2015, 0, 2017, 'Y', '2017-01-29 20:47:54', '2017-01-29 20:47:54'),
(473, 163, 'Project Manager/Project Manager Assistant', 'TAJ Media Vietnam', '', 'Hà Nội, Vietnam', 0, 6, 2012, 11, 2014, 'Y', '2017-01-29 20:48:40', '2017-01-29 20:48:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `all_tropics`
--
ALTER TABLE `all_tropics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ask_questions`
--
ALTER TABLE `ask_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ask_questions_user_id_index` (`user_id`),
  ADD KEY `ask_questions_post_type_index` (`post_type`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_ifnos`
--
ALTER TABLE `contact_ifnos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contact_ifnos_user_id_index` (`user_id`);

--
-- Indexes for table `contact_messages`
--
ALTER TABLE `contact_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD UNIQUE KEY `idCountry` (`idCountry`);

--
-- Indexes for table `degrees`
--
ALTER TABLE `degrees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `educations`
--
ALTER TABLE `educations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `educations_user_id_index` (`user_id`),
  ADD KEY `educations_country_index` (`country`),
  ADD KEY `educations_from_month_index` (`from_month`),
  ADD KEY `educations_from_year_index` (`from_year`),
  ADD KEY `educations_to_month_index` (`to_month`),
  ADD KEY `educations_to_year_index` (`to_year`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorite_tropics`
--
ALTER TABLE `favorite_tropics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `favorite_tropics_tropic_id_index` (`tropic_id`),
  ADD KEY `favorite_tropics_user_id_index` (`user_id`);

--
-- Indexes for table `filter_order_sets`
--
ALTER TABLE `filter_order_sets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `filter_order_sets_user_id_index` (`user_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `languages_locale_index` (`locale`);

--
-- Indexes for table `notification_settings`
--
ALTER TABLE `notification_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_settings_user_id_index` (`user_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_translations`
--
ALTER TABLE `page_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `page_translations_page_id_locale_unique` (`page_id`,`locale`),
  ADD KEY `page_translations_locale_index` (`locale`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `privacy_settings`
--
ALTER TABLE `privacy_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `privacy_settings_user_id_index` (`user_id`),
  ADD KEY `privacy_settings_max_request_index` (`max_request`);

--
-- Indexes for table `school_majors`
--
ALTER TABLE `school_majors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timezones`
--
ALTER TABLE `timezones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tropics`
--
ALTER TABLE `tropics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tropics_user_id_index` (`user_id`),
  ADD KEY `tropics_type_index` (`type`);

--
-- Indexes for table `tropic_groups`
--
ALTER TABLE `tropic_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `universities`
--
ALTER TABLE `universities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_user_name_unique` (`user_name`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_address_user_id_index` (`user_id`),
  ADD KEY `user_address_type_index` (`type`);

--
-- Indexes for table `work_experiences`
--
ALTER TABLE `work_experiences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `work_experiences_user_id_index` (`user_id`),
  ADD KEY `work_experiences_country_index` (`country`),
  ADD KEY `work_experiences_from_month_index` (`from_month`),
  ADD KEY `work_experiences_from_year_index` (`from_year`),
  ADD KEY `work_experiences_to_month_index` (`to_month`),
  ADD KEY `work_experiences_to_year_index` (`to_year`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `all_tropics`
--
ALTER TABLE `all_tropics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `ask_questions`
--
ALTER TABLE `ask_questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=354;
--
-- AUTO_INCREMENT for table `contact_ifnos`
--
ALTER TABLE `contact_ifnos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `contact_messages`
--
ALTER TABLE `contact_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `degrees`
--
ALTER TABLE `degrees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `educations`
--
ALTER TABLE `educations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;
--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `favorite_tropics`
--
ALTER TABLE `favorite_tropics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `filter_order_sets`
--
ALTER TABLE `filter_order_sets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `notification_settings`
--
ALTER TABLE `notification_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `page_translations`
--
ALTER TABLE `page_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `privacy_settings`
--
ALTER TABLE `privacy_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `school_majors`
--
ALTER TABLE `school_majors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=406;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `timezones`
--
ALTER TABLE `timezones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=549;
--
-- AUTO_INCREMENT for table `tropics`
--
ALTER TABLE `tropics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=588;
--
-- AUTO_INCREMENT for table `tropic_groups`
--
ALTER TABLE `tropic_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `universities`
--
ALTER TABLE `universities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;
--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=744;
--
-- AUTO_INCREMENT for table `work_experiences`
--
ALTER TABLE `work_experiences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=474;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ask_questions`
--
ALTER TABLE `ask_questions`
  ADD CONSTRAINT `ask_questions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `contact_ifnos`
--
ALTER TABLE `contact_ifnos`
  ADD CONSTRAINT `contact_ifnos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `educations`
--
ALTER TABLE `educations`
  ADD CONSTRAINT `educations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `favorite_tropics`
--
ALTER TABLE `favorite_tropics`
  ADD CONSTRAINT `favorite_tropics_tropic_id_foreign` FOREIGN KEY (`tropic_id`) REFERENCES `all_tropics` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `favorite_tropics_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `filter_order_sets`
--
ALTER TABLE `filter_order_sets`
  ADD CONSTRAINT `filter_order_sets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notification_settings`
--
ALTER TABLE `notification_settings`
  ADD CONSTRAINT `notification_settings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `page_translations`
--
ALTER TABLE `page_translations`
  ADD CONSTRAINT `page_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `privacy_settings`
--
ALTER TABLE `privacy_settings`
  ADD CONSTRAINT `privacy_settings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tropics`
--
ALTER TABLE `tropics`
  ADD CONSTRAINT `tropics_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_address`
--
ALTER TABLE `user_address`
  ADD CONSTRAINT `user_address_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `work_experiences`
--
ALTER TABLE `work_experiences`
  ADD CONSTRAINT `work_experiences_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
