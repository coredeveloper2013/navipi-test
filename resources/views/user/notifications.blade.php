@extends('layouts.app')

@section('pageTitle', 'Welcome to ')

@section('content')
<section class="mainbody detailspageMiddle clear">
@include('include.left_pan')

<!--middle open-->
<div class="middlecol">
	
	<div class="contentmiddle">
		<div class="middlerow ansPage">
			<div class="ansQuesArea">
				<div class="contactNext">
					<div class="contactHead">
						<div class="titlebox clear" style="margin:0;">
							<h2>Notifications</h2>
						</div>
					</div>
					<?php
					$notify_text=[
						'Q'=>'has sent you a question.',
						'A'=>'has answered your question.',
						'U'=>'has upvoted your answer.',
						'C'=>'has commented in your question/article.',
						'R'=>'has reminded you a question.'];
					?>
					<ul class="notificationListing">
						@if(!empty($notifications))
							@foreach($notifications as $val)

								<?php $profImag = asset('assets/frontend/images/profile.jpg');
								if(!empty($val->from_usr))
								{
									if($val->from_usr->profile_image !='' && file_exists('assets/upload/profile_image/'.$val->from_usr->profile_image)==1)
									{
										$profImag =asset('assets/upload/profile_image/'.$val->from_usr->profile_image);
									}
								}
								?>
								<li>
									<a href="{{url('user/question-details/'.$val->question_id)}}">
										@if($val->type !='Q' || ($val->type =='Q' && $val->question->send_as_anonymous == 'N'))
										<span class="image" style="background:url({!! $profImag !!});">
											<!-- <img src="{!! $profImag !!}" alt="">-->
										</span>
										@endif
										<div class="textBody">
											<div class="msgTxt">
												@if($val->type !='Q' || ($val->type =='Q' && $val->question->send_as_anonymous == 'N'))
													<span class="nmTl"> {!!(!empty($val->from_usr))?$val->from_usr->nickname:''!!}</span>
												@else
													<span class="nmTl"> Anonymous User</span>
												@endif

													{!!$notify_text[$val->type]!!}
											</div>
											<span class="tm">{!!time_elapsed_string(strtotime($val->created_at))!!}</span>
										</div>
										<div class="spacer"></div>
									</a>
								</li>
							@endforeach
						@endif
						{{--<li class="clear">
								<a href=""><img src="{!! asset('assets/frontend') !!}/images/userthumb.png" alt=""></a>
								<div class="notText">
									<p><strong><a href="">John Smith</a></strong> voted this article</p>
									<em>"Lorem Ipsum is simply dummy text"</em>
									<span>4 mins ago</span>
								</div>
						</li>--}}
					</ul>
					<div class="smallAll">
						<a href="#" class="">view All</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--middle close-->
        
@include('include.right_pan')
</section>
@endsection
@section('customScript')
<script src="{!!asset('assets/frontend')!!}/ckeditor/ckeditor.js"></script>
<script> 
$(document).ready(function() {
	CKEDITOR.replaceClass = 'texteditor';
	
	$('.newcomment').click(function(){
		$(this).parent('li').parent('ul').parent('.bottombox').next('.anscomment').slideToggle(200);
	});
	
	//
	$('.SeeAll').click(function(){
		$(this).prev('.addDesc').slideToggle(200);
		$(this).toggleClass('active');
		$(this).parent('.des-text').parent('.middleleft').parent('.answertop').toggleClass('fullbox');
	});
	
	//
	$('.cmntbtn').click(function(){
		$(this).parent('li').parent('ul').parent('.cmdhead').parent('.cmdbox').children('.openComment').slideToggle(200);
	});
		
	//
	$('.givAns').click(function(){
		$('.ansbot').children('.yourans').slideToggle(200);
	});
	$('.ansFull').click(function(){
		$(this).parent('.allAns').prev('.ansbody').toggleClass('allShow');
		$(this).toggleClass('active');
	});
	//
	//$('.ansPage').prepend('<div class="manageheight" style="height: 0px;"></div>');
	$('.vwans').click(function(){
		$(this).prev('.answertop').slideToggle(200);
		$(this).toggleClass('active');
		$('.smlScreen').slideToggle(100);
		$('.fixedans .questionHead').slideToggle(100);
		$('.fixedans .usertop').slideToggle(100);
	});
	
	//
	$(document).on('click','.replayForm',function() {
		if(!$(this).parent('span').parent('.blogmeta').next('.smallreplay').is(":visible")){
			$('.smallreplay').slideUp(200);
			$(this).parent('span').parent('.blogmeta').next('.smallreplay').slideDown(200);
		}else{
			$(this).parent('span').parent('.blogmeta').next('.smallreplay').slideUp(200);
		}
	});
	$('.closeBox').click(function() {
		$(this).parent('.smallreplay ').slideUp(200);
	});
	$('#ansFrm').click(function(e) {
		e.preventDefault();
		var textbox_data = CKEDITOR.instances['anscontent'].getData();
		
		if(textbox_data.replace(/(<([^>]+)>)/ig,"") != "")
		{
			$(this).submit();
		}
	});
	
	$(document).on( "submit","form.commentfrm", function( event ) {
	
		event.preventDefault();
		
		var formdata =  $( this ).serializeArray();
		var fobj = $(this);

		$.ajax({
			type:"post",
			url: "{!! url('user/comment-process') !!}" ,
			headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
			data: formdata,			
			dataType: "json",			
			success:function(res) {
				console.log(res);
				if(res.comment)
				{
					var cur_comnt = res.comment.comment;
					cur_comnt = cur_comnt.replace(/(?:\r\n|\r|\n)/g, '<br />');
					var uimg ="<?php
                        $profImag = asset('assets/frontend/images/profile.jpg');
                        if(Auth::check() && Auth::user()->profile_image !='' && file_exists('assets/upload/profile_image/'.Auth::user()->profile_image)==1)
                        {
                            $profImag =asset('assets/upload/profile_image/'.Auth::user()->profile_image);
                        }
                        echo  $profImag;
                        ?>";
					var chtml = '<li>'
								+'<figure><img src="'+uimg+'" alt=""></figure>'
								+'<div class="blogcommnt">'
									+'<h4 class="blgTtl"><a href="'+BASE_URL+'profile/'+res.comment.user_id+'">'+res.nickname+'</a></h4>'
									+'<div class="blogTxts"><p>'+cur_comnt+'</p></div>'
									+'<div class="blogmeta">'
										+'<span>0 <a href=""><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <span>Like</span></a></span>'
										+'<span>0 <a href="javascript:void(0);" class="replayForm"><i class="fa fa-reply" aria-hidden="true"></i> <span>Reply</span></a></span>'
										+'<span>'+res.created_at+'</span>'
									+'</div>'
									+'<div class="smallCommnt smallreplay clear">'
										+'<figure><img src="{!!asset('assets/frontend')!!}/images/userthumb.png" alt=""></figure>'
										+'<div class="blogcommnt">'
											+'<form class="commentfrm" method="POST">'
												+'<textarea name="comment" placeholder="Write a comment..."></textarea>'
												+'<input name="answer_id" type="hidden" value="'+res.comment.answer_id+'"/>'
												+'<input name="parent_id" type="hidden" value="'+res.comment.id+'"/>'
												+'<button type="submit" value="submit"><i class="fa fa-reply-all" aria-hidden="true"></i><span>Reply</span></button>'
											+'</form>'
										+'</div>'
									+'</div><div class="commntList" style="display:block;"><ul></ul></div>'
								+'</div>'
							+'</li>';
					fobj.parent().parent().next('.commntList').children('ul').prepend(chtml);		
					fobj.find("textarea").val("");
				}
			}
		});
		
	});
	
	$(document).on('click','.upvote a',function(){
		var obj = $(this);
		var ans_id = obj.children('input').val();
		upvoteDownvote(obj,ans_id,'Y');
	});
	$(document).on('click','.downvote a',function(){
		var obj = $(this);
		var ans_id = obj.children('input').val();
		upvoteDownvote(obj,ans_id,'N');
	});
	
	function upvoteDownvote(obj,ans_id,status)
	{
		$.ajax({
			type:"post",
			url: "{!! url('user/upvote-downvote') !!}" ,
			headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
			data: {'ans_id':ans_id,'status':status},			
			dataType: "json",			
			success:function(res) {
				/* console.log(res); */
				if(res)
				{			
					obj.parent().parent().children('li.upvote').children('a').children('span').html(res.upvotes);
					obj.parent().parent().children('li.downvote').children('a').children('span').html(res.downvotes);
				}
					
			}
		});
	}
	$(document).on('click','.cmtLike',function (){
		var cur_obj = $(this);
		var cmnt_id = cur_obj.attr('cmid');
		$.ajax({
			type:"post",
			url: "{!! url('user/like-comment') !!}" ,
			headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
			data: {'cmnt_id':cmnt_id},			
			dataType: "json",			
			success:function(res) {
				/* console.log(res); */
				if(res)
				{		
					cur_obj.parent('span').find('font').text(res);
				}
					
			}
		});
	});
	
	$('.rqstbtn').click(function(){
        $('#frm_type').val('r');
		$('.success_message').hide();
		$('#tglist').html('');
		$('#qsnFrm').find("input[type=text], select, textarea").val("");
		var qobj = $(this);
		var qid = qobj.children('input').val();
		//alert(qid);
		$.ajax({
			type:"post",
			url: "{!! url('user/get-question') !!}" ,
			headers: {'X-CSRF-TOKEN': "{!! csrf_token() !!}"},
			data: {'qid':qid},			
			dataType: "json",			
			success:function(res) {
				//console.log(res);
				if(res)
				{				
					/* $.fancybox.open([
						{
							//type: 'iframe',
							href : '#tospopup',   
						}
					], {
						padding : 0
					});	 */	
						
					//$('#askbtn').fancybox().trigger('click');	
					$('#asktlt').text('Request More');		
					$('#sec1,#sec2,#sec3,#sec4,#sec5,#sec6,#sec7').find("input[type=text], select, textarea").prop('disabled', true);			
					$('#sec1,#sec2,#sec3,#sec4,#sec5,#sec6,#sec7').css('display','none');
					$('#secEmail').find("input[type=text], select, textarea").prop('disabled', false);
					$('#secEmail').show();
					$('#inpids').val('');		
					$('#emails').val('');
					
					$('#qsn_id').val(res.question.id);
				} 
				
			}
		});
	});

	$('.rqstbtn').fancybox({
		padding:0,
		afterShow: function(){
			$(".viewansbox").mCustomScrollbar();
		},
		afterClose: function() {
		}
	});
	
});

$(window).load(function(){
	tqhControl();
});
$(window).resize(function(){
	tqhControl();
});
function tqhControl(){
	var tqh = $('.ansQues').innerHeight();	
	$(".ansQuesArea").css({"min-height":tqh});
}
$(document).scroll(function() {
	var xx = $('.fixedans .ansQues').outerHeight();
	var th = $('#mainheader').height() + $('.ansQues').innerHeight() + 80;
	var tqa = (th - 160);
	if($(this).scrollTop() > tqa){
		$('.answertop').removeClass('fullbox');
		$('.addDesc').removeAttr('style');
	}
	if($(this).scrollTop() > th){
		$(".ansPage").addClass("fixedans"); 
		$(".manageheight").addClass('noheight');
	} 
	else{
		$(".ansPage").removeClass("fixedans");
		$(".manageheight").removeClass('noheight');
		$('.answertop').removeAttr('style');
		$('.vwans').removeAttr('style');
		$('.question-tags').removeAttr('style');
		$('.vwans').removeClass('active');
		$('.fixedans .anstopuser .question-tags').removeAttr('style');
		$('.SeeAll').removeClass('active');
		//$('.answertop').removeClass('fullbox');
		//$('.addDesc').removeAttr('style');
		$('.questionHead').removeAttr('style');
		$('.usertop').removeAttr('style');
	}
	$('.manageheight').height(xx);
});

$('body').on('click','.edtAns',function() {
	var cobj = $(this);
	//$('.ans_sec').slideUp(200);
	//$('.ansSec').slideDown(200);
	var ans_sec = cobj.parent().parent().parent().parent().parent().children('.ans_sec');
	var ansSec = cobj.parent().parent().parent().parent().parent().children('.ansSec');
	ans_sec.slideToggle(200);
	ansSec.slideToggle(200);
	
});

$(window).load(function(){
	$('body').on('click','.blogTxts .mr',function() {
		$(this).parent().children("p").css({"max-height":"none"});
		$(this).addClass("ls");
		$(this).html("less");
	});
	$('body').on('click','.blogTxts .mr.ls',function() {
		$(this).parent().children("p").css({"max-height":"222px"});
		$(this).removeClass("ls");
		$(this).html("more...");
	});
	
});


</script>
@endsection