<?php
use App\Helpers\CustomHelper;
?>
@if(count($staticmessages) > 0)
<ul>
    @foreach($staticmessages as $st_msg)
    <li>
        <label>
            <input type="radio" name="message" value="{!! $st_msg->name !!}">
            <span class="label">{!! $st_msg->name !!}</span>
        </label>
    </li>
    @endforeach
</ul>
@else
    {!! CustomHelper::siteStaticText(113) !!}...
@endif
