<?php
/* use App\Helpers\CustomHelper; */
$loguser = Auth::User();
?>
@if(isset($chat_messsage_data['all_msg_list']) && count($chat_messsage_data['all_msg_list']))

<ul id="boxscroll2">
    @foreach($chat_messsage_data['all_msg_list'] as $msg)
        
        
            <!--<li class="msgDateShow">
                <span>{!! date('l,F-d-Y',strtotime($msg->created_at)) !!}</span>
            </li>-->
            
      
        @if($msg->from_user_id == $chat_messsage_data['from_user_id'])
            <li class="list meChat">
                 <span class="name" style="display:block;"><a href="{!! url('profile/'.$msg->fromUser->id) !!}" target="_blank">{!! $msg->fromUser->first_name !!} {!! $msg->fromUser->last_name !!}</a></span>
                <span class="time">{!! time_elapsed_string(strtotime($msg->created_at)) !!} ago</span>
				@if($msg->type == 'T')
					<p>{!! $msg->message !!}</p>
				@else
					@if(in_array($msg->file_ext,array('jpg','JPG','JPEG','png','PNG','bmp','BMP','gif','GIF')))
						<p><a href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" class="chat_pic_msg"><img style="width:120px;" src="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}"/></a></p>
					@else
						<p><a href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" download >{!! $msg->message !!}</a></p>
					@endif
				@endif
            </li>
        @else
            <li class="list userChat">
		
                <span class="name" style="display:block;"><a href="{!! url('profile/'.$msg->fromUser->id) !!}" target="_blank">{!! $msg->fromUser->first_name !!} {!! $msg->fromUser->last_name !!}</a></span>								
                <span class="time">{!! time_elapsed_string(strtotime($msg->created_at)) !!} ago</span>
                @if($msg->type == 'T')
					<p>{!! $msg->message !!}</p>
				@else
					@if(in_array($msg->file_ext,array('jpg','JPG','JPEG','png','PNG','bmp','BMP','gif','GIF')))
						<p><a href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" class="chat_pic_msg"><img style="width:120px;" src="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}"/></a></p>
					@else
						<p><a href="{!! asset('assets/upload/chat_files/'.$msg->file_name) !!}" download >{!! $msg->message !!}</a></p>
					@endif
				@endif                                               
            </li>
        @endif
    @endforeach
    
        <li class="msgDateShow">
            <span>{!! date('l,F-d-Y',time()) !!}</span>
        </li>
    
</ul>
@else
<ul id="boxscroll2">
    <li class="msgDateShow">
        <span>{!! date('l,F-d-Y',time()) !!}</span>
    </li>
</ul>
@endif    