<li>
    <a href="javascript:void(0);" onclick="startChat('{!! $to_user_det->id !!}');">
        <span class="image">
            @if($to_user_det->prifile_picture != '' && file_exists('assets/upload/profile_pictures/'.$to_user_det->prifile_picture))
                <img src="{!! asset('assets/upload/profile_pictures/'.$to_user_det->prifile_picture) !!}" alt="{!! $to_user_det->username !!}"/>
            @else
                <img src="{!! asset('assets/common/images/default_avatar.jpg') !!}" alt="{!! $to_user_det->username !!}"/>
            @endif
        </span>
        <span class="name">{!! $to_user_det->username !!}</span>
        <span class="status {!! $is_online == 1?'online':'' !!}"></span>
    </a>
</li>