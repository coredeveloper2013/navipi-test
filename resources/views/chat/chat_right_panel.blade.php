<?php
$loguser = Auth::User();
if($loguser):
$shw_arr=[];
?>
<div id="tab1" class="allUserTabtab_view online_userchat_right_panel">
	<span class="ttl"><i class="fa fa-comments" aria-hidden="true"></i> Messenger</span>
    <ul class="allFriendsList">
    @if(count($chat_messsage_data['chat_users']) > 0)
        @foreach($chat_messsage_data['chat_users'] as $cht_usr)
            @if($cht_usr->from_user_id == $loguser->id && !in_array($cht_usr->to_user_id,$shw_arr))
					@php($shw_arr[]=$cht_usr->to_user_id)
				<li>
					<a href="javascript:void(0);" onclick="startChatPop('{!! $cht_usr->toUser->id !!}', 0);">
						<span class="image">
							@php
								$notifyImag = asset('assets/frontend/images/profile.jpg');

                                if($cht_usr->toUser->profile_image !='' && file_exists('assets/upload/profile_image/'.$cht_usr->toUser->profile_image)==1)
        {
            $notifyImag = asset('assets/upload/profile_image/'.$cht_usr->toUser->profile_image);
        }
							@endphp

								<img src="{!! $notifyImag !!}" alt="{!! $cht_usr->toUser->first_name !!} {!! $cht_usr->toUser->last_name !!}"/>

						</span>
						<span class="name">{!! $cht_usr->toUser->first_name !!} {!! $cht_usr->toUser->last_name !!}</span>
						<span id="sts_{{$cht_usr->toUser->id}}" class="status online"></span>
					</a>
				</li>
            @elseif($cht_usr->to_user_id == $loguser->id  && !in_array($cht_usr->from_user_id,$shw_arr))
					@php($shw_arr[]=$cht_usr->from_user_id)
				<li>
					<a href="javascript:void(0);" onclick="startChatPop('{!! $cht_usr->fromUser->id !!}', 0);">
						<span class="image">
							@php
								$notifyImag = asset('assets/frontend/images/profile.jpg');

                                if($cht_usr->fromUser->profile_image !='' && file_exists('assets/upload/profile_image/'.$cht_usr->fromUser->profile_image)==1)
        {
            $notifyImag = asset('assets/upload/profile_image/'.$cht_usr->fromUser->profile_image);
        }
							@endphp

								<img src="{!! $notifyImag !!}" alt="{!! $cht_usr->fromUser->first_name !!} {!! $cht_usr->fromUser->last_name !!}"/>
						</span>
						<span class="name">{!! $cht_usr->fromUser->first_name !!} {!! $cht_usr->fromUser->last_name !!}</span>
						<span id="sts_{{$cht_usr->fromUser->id}}" class="status online"></span>
					</a>
				</li>

            @endif
        @endforeach
    @endif
    </ul>
</div>
<?php
endif;
?>