<?php
//use App\Helpers\CustomHelper;
//dd($chat_messsage_data['chat_users']->toArray());
$loguser = Auth::User();
$shw_arr=[];
?>
<div id="chtMmbrlst" class="chatMmbrList mCustomScrollbar">
@if(count($chat_messsage_data['chat_users']) > 0)
    <ul>
    @foreach($chat_messsage_data['chat_users'] as $cht_usr)


        @if($cht_usr->from_user_id == $loguser->id && !in_array($cht_usr->to_user_id,$shw_arr))

                @php($shw_arr[]=$cht_usr->to_user_id)

            <li {!! session('touser_id') == $cht_usr->toUser->id?'class="selectli"':'' !!} id="user_list_{!! $cht_usr->toUser->id !!}">
                <a href="javascript:void(0);" onclick="startChatMessage('{!! $cht_usr->toUser->id !!}');">
                    <span class="user">
                         @php
                             $notifyImag = asset('assets/frontend/images/profile.jpg');

                             if($cht_usr->toUser->profile_image !='' && file_exists('assets/upload/profile_image/'.$cht_usr->toUser->profile_image)==1)
     {
         $notifyImag = asset('assets/upload/profile_image/'.$cht_usr->toUser->profile_image);
     }
                         @endphp

                            <img src="{!! $notifyImag !!}" alt="{!! $cht_usr->toUser->first_name !!} {!! $cht_usr->toUser->last_name !!}"/>

                    </span>
                    <span class="nameTm">
                        <span class="name">
                            <span class="status {!! (count($cht_usr->toUser->userLogin) > 0 && $cht_usr->toUser->userLogin->login_status == 1)?'online':'offline' !!}"></span>
                            {!! $cht_usr->toUser->first_name !!} {!! $cht_usr->toUser->last_name !!}
    						<span class="count" style="display:none;"></span>
                        </span>
						<span class="msgUTxt">
							<i class="fa fa-envelope-o"></i> {!!last_received_msg($loguser->id,$cht_usr->to_user_id)!!}
						</span>
                        @if(count($cht_usr->toUserMessage))
                        <p>{!! $cht_usr->toUserMessage->message !!}</p>
                        @endif
                    </span>
                    @if($cht_usr)
                        <span class="lastOnline">{!! date('D,M-d',strtotime($cht_usr->created_at)) !!}</span>
                    @endif
                </a>
            </li>
        @elseif($cht_usr->to_user_id == $loguser->id && !in_array($cht_usr->from_user_id,$shw_arr))

                        @php($shw_arr[]=$cht_usr->from_user_id)

            <li {!! session('touser_id') == $cht_usr->fromUser->id?'class="selectli"':'' !!} id="user_list_{!! $cht_usr->fromUser->id !!}">
                <a href="javascript:void(0);" onclick="startChatMessage('{!! $cht_usr->fromUser->id !!}');">
                    <span class="user">
                        @php
                            $notifyImag = asset('assets/frontend/images/profile.jpg');

                            if($cht_usr->fromUser->profile_image !='' && file_exists('assets/upload/profile_image/'.$cht_usr->fromUser->profile_image)==1)
    {
        $notifyImag = asset('assets/upload/profile_image/'.$cht_usr->fromUser->profile_image);
    }
                        @endphp

                            <img src="{!! $notifyImag !!}" alt="{!! $cht_usr->fromUser->first_name !!} {!! $cht_usr->fromUser->last_name !!}"/>

                    </span>
                    <span class="nameTm">
                        <span class="name">
                            <span class="status {!! (count($cht_usr->fromUser->userLogin) > 0 && $cht_usr->fromUser->userLogin->login_status == 1)?'online':'offline' !!}"></span>
                            {!! $cht_usr->fromUser->first_name !!} {!! $cht_usr->fromUser->last_name !!}
    						<span class="count" style="display:none;"></span>
                        </span>
						<span class="msgUTxt">
							<i class="fa fa-envelope-o"></i> {!!last_received_msg($loguser->id,$cht_usr->from_user_id)!!}
						</span>
                        @if(count($cht_usr->toUserMessage))
                        <p>{!! $cht_usr->toUserMessage->message !!}</p>
                        @endif
                    </span>
                    @if($cht_usr)
                        <span class="lastOnline">{!! date('D,M-d',strtotime($cht_usr->created_at)) !!}</span>
                    @endif
                </a>
            </li>
         @endif
    @endforeach
    </ul>
@else
<ul><li> No Record Found! </li></ul>
@endif
</div>