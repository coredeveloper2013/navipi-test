<?php
/**
 * Page resource built upon CoC
 * You can leave this as it is
 * or feel free to remove these configuration and customize
 * @author Tuhin | <tuhin@technoexponent.com>
 */
$resource = 'email_template';
$resource_pl = str_plural($resource);
$page_title = ucfirst(str_replace('_', ' ', $resource));
$page_title_pl = ucfirst(str_replace('_', ' ', $resource_pl));
// Localization data...
$locale = app()->getLocale();

// Resolute the data...
$data = ${$resource_pl};
//dd($data);

?>

@extends('admin.layouts.app')

@section('pageTitle', 'Edit ' . $page_title)

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {!! $page_title !!}
            <small>Edit {!! $data->firstname !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{!! url("admin/$resource") !!}"><i class="fa  fa-user"></i> {!! $page_title_pl !!}</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit {{ $page_title }}</h3>
                    </div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <p>{!! $error !!}</p>
                            @endforeach
                        </div>
                    @endif
                    @if(session('success'))
                    <div class="alert alert-success">
                        {!! session('success') !!}
                    </div>
                    @endif
					
					<select size="10" style="font-size:11px;width:20%">
						<option>{USER_NAME} User Name</option>											
						<option>{ACTIVATION_LINK} Activation Link</option>	
						<option>{SITE_URL} Site URL</option>	
						<option>{CONTACT_MAIL} Site URL</option>
					</select>
                    <form method="POST" action="{!! admin_url('email-template/' . ${$resource_pl}->id) !!}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PATCH"> {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
								<label for="email_title" class="col-sm-2 control-label">Title </label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="email_title" name="email_title" placeholder="title" value="{!! $data->email_title !!}" />
								</div>
							</div>
							<div class="form-group">
								<label for="description" class="col-sm-2 control-label">Description </label>
								<div class="col-sm-6">
									<textarea class="form-control ckeditor" id="description" name="description" placeholder="description" >{!! $data->description !!}</textarea>
								</div>
							</div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <a class="btn btn-default" href="{!! url('admin/email-template') !!}">
                                Back</a>
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </div><!-- /.box-footer -->
					</form>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection

@section('customScript')
<script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>
@endsection