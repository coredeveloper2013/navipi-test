<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class PassQuestion extends Model 
{
    protected $table = "pass_questions";
    protected $fillable = [
        'question_id', 'user_id', 'status'
    ];
	
	public function question_pass()
    {
        return $this->hasOne('App\User','id','question_id');
    }
	public function user_pass()
    {
        return $this->hasMany('App\User','id','user_id')->where('status','Y');
    }
}
