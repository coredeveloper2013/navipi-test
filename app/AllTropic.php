<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class AllTropic extends Model 
{
	 protected $fillable = [
        'title','group_id','image'
    ];
	
	public function group()
    {
        return $this->hasOne('App\TropicGroup','id','group_id');
    }
	public function favorite_tropic()
    {
        return $this->hasMany('App\FavoriteTropic','tropic_id','id');
    }
	public function tropic_questions()
    {
        return $this->hasMany('App\TropicQuestion','tropic_id','id')->orderBy('id','DESC');
    }
	
}
