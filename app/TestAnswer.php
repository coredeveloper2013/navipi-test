<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class TestAnswer extends Model 
{
	protected $fillable = [
        'test_eavl_id','question_id','answer_id','answer','is_correct'
    ];
	
	public function test_eval()
    {
        return $this->belongsTo('App\TestEvaluation');
    }
}
