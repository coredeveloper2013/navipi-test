<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class BlogComment extends Model 
{
    protected $table = "blog_comments";
    protected $fillable = [
        'user_id', 'blog_id', 'parent_id', 'comment'
    ];
	
	public  function parentComment(){
        return $this->belongsTo('App\BlogComment', 'parent_id');
    }
	public function replyComments()
    {
        return $this->hasMany('App\BlogComment','parent_id','id')->orderBy('id','DESC');
    }
	public  function comment_user(){
        return $this->belongsTo('App\User', 'user_id','id');
    }
	public  function comment_blog(){
        return $this->belongsTo('App\Blog', 'blog_id','id');
    }
}
