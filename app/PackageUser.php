<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class PackageUser extends Model 
{
	protected $table = "package_user";
	protected $fillable = [
        'user_id','package_id','display_order','paid','valid','add_by_admin',
    ];
	
}
