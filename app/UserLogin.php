<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class UserLogin extends Model
{
	
	protected $fillable = [
        'user_id','lastlogintime','ipaddress'
    ];
		
	/*public function question()
    {
        return $this->belongsTo('App\AskQuestion');
	}*/

}
