<?php

namespace App\Http\Controllers;


use App\Page;
use App\Friend;
use App\Chatmessage;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller {

    public $chat_messsage_data = array();

    public function __construct()
    {
        $unread_msg = [];
        $chat_users = [];
        $frnd_users = [];
        $chat_usersdta = [];
        $loguser = Auth::User();
        if(Auth::check())
        {
            $chat_usersdta = Friend::where(['to_user_id' => Auth::user()->id, 'status'=>'Y'])->orWhere(['from_user_id' => Auth::user()->id, 'status'=>'Y'])->orderBy('created_at', 'desc')->get();

        }
        if(count($chat_usersdta) > 0)
        {
            for($i = 0;$i < count($chat_usersdta); $i++)
            {
                if ($loguser->id == $chat_usersdta[$i]->from_user_id)   $frnd_users[]=$chat_usersdta[$i]->to_user_id;
                else if ($loguser->id == $chat_usersdta[$i]->to_user_id)   $frnd_users[]=$chat_usersdta[$i]->from_user_id;
            }
        }
        $frnd_users = array_unique($frnd_users);
        //
        $chat_users = Chatmessage::whereIn('from_user_id', $frnd_users)->orWhereIn('to_user_id', $frnd_users)->orderBy('created_at', 'desc')->get();
        //dd($chat_users);

        if(Auth::check())
        {
            $unread_msg = Chatmessage::where([
                ['to_user_id', '=', Auth::user()->id],
                ['to_user_view_status', '=', 'N']
            ])
                ->orderBy('created_at', 'desc')
                ->get();
        }

        //dd($chat_users->toArray());
        $this->chat_messsage_data = [
            'chat_users' => $chat_users,
            'unread_msg' => $unread_msg
        ];
    }

    // Add methods to add, edit, delete and show pages
    // create method to create new pages
    // submit the form to this method
    /*  public function create()
     {
         $inputs = Input::all();
         //dd($inputs);
         $page = Page::create($inputs);
     } */

    // Show a page by slug
    public function show($slug = 'home')
    {

        //$page = page::whereSlug($slug)->first();


        $page = \App\PageTranslation::where('locale',app()->getLocale())->where('slug',$slug)->first();

        $user = Auth::user();
        //dd($page);

        if(empty($page))
        {

            return redirect()->to('');

        }
        $data = [
            'user' => $user,
            'page' => $page,
            'chat_messsage_data' => $this->chat_messsage_data
        ];
        return view('page.index',$data);

    }



}