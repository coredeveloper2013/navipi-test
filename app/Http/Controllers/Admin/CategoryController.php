<?php



namespace App\Http\Controllers\Admin;


use Image;
use App\Category;

use App\Language;

use Illuminate\Http\Request;



use App\Http\Requests;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;



class CategoryController extends Controller

{

    protected $languages;

    protected $fallback_language;



    /**

     * CmsPageController constructor.

     */

    public function __construct()

    {

        $this->languages = app('languages');

        $this->fallback_language = $this->languages->where('fallback_locale', 'Y')->first();

    }



    /**

     * Display a listing of the resource.

     *

     * @param string $locale

     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View

     */

    public function index($locale = '')
    {

        /* if($locale && in_array($locale, config('translatable.locales'))) {

            app()->setLocale($locale);

        } */

        $current_language = Language::current();


        $per_page = config('constants.ADMIN_PER_PAGE');
        $categories = Category::orderBy('id', 'desc')->paginate($per_page);

        //$cms = Page::translatedIn(app()->getLocale())->orderBy('id', 'desc')->paginate($per_page);

        return view('admin.categories.list', compact('categories','current_language'));

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
		$parent_categories = Category::where('parent_id',0)->get();
        return view('admin.categories.add',compact('parent_categories'));

    }

	
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */
    public function store(Request $request)

    {		
        $this->validate($request, [

                'title' => 'required|max:255',
            ]
        );
		$inp_data=$request->all();
		//dd($inp_data);
		$title = trim($inp_data['title']);
		$time = time();
		if($request->hasFile('icon')){
            $path   = public_path().'/assets/upload/category_icon/';
            $image  = $request->file('icon');
            $save_name = $time.str_random(10).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->save($path . $save_name, 100);
            $data['icon'] = $save_name;
        }
		$data['title']= $title;
        Category::create($data);

        return redirect()->back()->with('success', 'Category successfully added.');

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $categories = Category::find($id);
		
		//dd($packages->toArray());

        return view('admin.categories.edit', compact('categories'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)
    {

        $this->validate($request, [

                'title' => 'required|max:255',

            ]

        );
		$inp_data=$request->all();
		$data['title']= $inp_data['title'];
		
		$category = Category::find($id);
		$time = time();
		if($request->hasFile('icon')){
            $old_image = 'assets/upload/category_icon/'.$category->icon;
            \File::delete($old_image);

            $path   = public_path().'/assets/upload/category_icon/';
            $image  = $request->file('icon');
            $save_name = $time.str_random(10).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->save($path . $save_name, 100);
            $category->icon = $save_name;
        }
		//dd($data);
        $category->fill($data)->save();
        return redirect()->back()->with('success', 'Update was successfully done.');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)
    {

       $category =  Category::find($id);
        $old_image = 'assets/upload/category_icon/'.$category->icon;
		if(file_exists($old_image)){
            \File::delete($old_image);
		}
       $category =  Category::destroy($id);
        return redirect()->back()->with('success', 'Tropic successfully removed!');

    }



    /* public function deleteTranslation($locale = '', $id)

    {

        if($locale && in_array($locale, config('translatable.locales'))) {

            if($locale == '' || !in_array($locale, config('translatable.locales'))) {

                return redirect()->back()->with('error', 'Sorry! Unable to process your request.');

            }

            app()->setLocale($locale);

            $cms = Page::find($id)->getTranslation($locale);

            $cms->delete();

            $current_language = Language::current();

            return redirect()->back()->with('success', 'Page for ' . $current_language->name . ' successfully removed!');

        }

        return redirect()->back()->with('error', 'Sorry! Unable to process your request.');

    } */

}

