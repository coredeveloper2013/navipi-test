<?php

namespace App\Http\Controllers\Admin;

use Image;
use App\Setting;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Display the dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    /**
     * General site settings.
     */
    public function settings()
    {
        $settings = Setting::find(1);
        $admin_user = Auth::guard('admin')->user();
        return view('admin.setting', compact('settings', 'admin_user'));
    }

    public function updateSettings(Request $request)
    {
        $this->validate($request, [
            'admin_email' => 'required|email|max:255',
            'site_title' => 'required|max:255',
            'contact_email' => 'required|email',
            'contact_name' => 'required|max:255',
           // 'contact_phone' => 'required',
            'site_logo' => 'mimes:jpg,jpeg,png,bmp|max:10000',
        ],
        [
            'site_logo.mimes' => 'Please upload site logo of type jpg,jpeg,png,bmp',
            'site_logo.max' => 'Maximum of 10 MB is allowed for site logo'
        ]);
        $input = $request->all();
        $admin_user = Auth::guard('admin')->user();
        $admin_user->email = $input['admin_email'];
        if($input['admin_pass'] != '')
        {
            $admin_user->password = bcrypt($input['admin_pass']);
        }
        $admin_user->save();

        $time = time();
        $settings = Setting::find(1);
        /* Logo Image upload */
        if($request->hasFile('site_logo')){
            $old_image = 'assets/upload/site_logo/'.$settings->site_logo;
            \File::delete($old_image);

            $path   = public_path().'/assets/upload/site_logo/';
            $image  = $request->file('site_logo');
            $save_name = $time.str_random(10).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->save($path . $save_name, 100);
            $settings->site_logo = $save_name;
        }
        if($request->hasFile('site_logo2')){
            $old_image = 'assets/upload/site_logo/'.$settings->site_logo2;
            \File::delete($old_image);

            $path   = public_path().'/assets/upload/site_logo/';
            $image  = $request->file('site_logo2');
            $save_name = $time.str_random(10).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->save($path . $save_name, 100);
            $settings->site_logo2 = $save_name;
        }
        if($request->hasFile('site_home_bg')){
            $old_image = 'assets/upload/site_logo/'.$settings->site_home_bg;
            \File::delete($old_image);

            $path   = public_path().'/assets/upload/site_logo/';
            $image  = $request->file('site_home_bg');
            $save_name2 = $time.str_random(10).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->save($path . $save_name2, 100);
            $settings->site_home_bg = $save_name2;
        }

        $settings->admin_name = $admin_user->name;
        $settings->admin_email = $input['admin_email'];
        $settings->site_slogan = $input['site_slogan'];
        $settings->site_title = $input['site_title'];

        $settings->contact_info = $input['contact_info'];
        $settings->contact_email = $input['contact_email'];
        $settings->contact_name = $input['contact_name'];
        $settings->contact_phone = $input['contact_phone'];		
        $settings->seo_title = $input['seo_title'];
        $settings->seo_keywords = $input['seo_keywords'];
        $settings->seo_description = $input['seo_description'];
        $settings->contact_address = $input['contact_address'];
        $settings->lat = $input['lat'];
        $settings->lng = $input['lng'];
        //$settings->paypalemailaddress = $input['Paypal_Email'];
        $settings->site_fb_link = $input['facebook_link'];
        $settings->site_twitter_link = $input['twitter_link'];
        $settings->site_gplus_link = $input['gplus_link'];
        $settings->site_linkedin_link = $input['linkedin_link'];
        $settings->site_rss_link = $input['rss_link'];
        $settings->save();
        return redirect()->back()->with('success', 'Settings updated successfully.');
    }
}
