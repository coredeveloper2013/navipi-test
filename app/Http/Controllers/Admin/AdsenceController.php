<?php

namespace App\Http\Controllers\Admin;



use App\Adsence;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdsenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per_page = config('constants.ADMIN_PER_PAGE');
        $adsences = Adsence::orderBy('id', 'desc')->paginate($per_page);
        return view('admin.adsence.list', compact('adsences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$car_makers = PopularCitie::get(['id', 'name']);
        //$car_models = CarModel::get(['id', 'name']);
        return view('admin.adsence.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		//dd($request->all());
        $this->validate($request, [
            'adsense_code'  => 'required',
        ]);

        Adsence::create($request->all());
        return redirect()->back()->with('success', 'Adsence successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $adsence = Adsence::find($id);
		
        $data = [
            'adsences' => $adsence,
        ];
        return view('admin.adsence.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'adsense_code'  => 'required',
        ]);
        $adsense = Adsence::find($id);
        $adsense->fill($request->all());
        $adsense->save();
		
        return redirect()->back()->with('success', 'Update was successfully done.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Adsence::destroy($id);
        return redirect()->back()->with('success', 'City successfully removed!');
    }
	
	
}
