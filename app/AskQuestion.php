<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use DB;

class AskQuestion extends Model 
{
    protected $table = "ask_questions";
    protected $fillable = [
        'user_id', 'title', 'to', 'content', 'post_type', 'tags', 'question_attached', 'send_as_anonymous',  'is_pinned', 'pinned_time', 
    ];
		
	public function answers()
    {
        return $this->hasMany('App\Answer','question_id','id')->orderBy('id','DESC');
    }
	
	public function questionUser()
    {
        return $this->belongsTo('App\User','user_id','id');
	}
	public function question_forward()
    {
        return $this->belongsTo('App\QuestionForward','question_id','id');
	}
	
	public function all_forwards()
    {
        return $this->hasMany('App\QuestionForward','question_id','id');
	}
	
	public function question_tropics()
    {
        return $this->hasMany('App\TropicQuestion','question_id','id');
    }
	public function qsn_views()
	{
		return $this->hasMany('App\QuestionView','question_id','id');
	}
}
